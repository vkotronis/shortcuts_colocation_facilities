import sys
import os
if sys.version_info >= (3, 0):
    import xmlrpc.client as xmlrpclib
else:
    import xmlrpclib
import argparse
import ujson as json

PLC_API = 'https://www.planet-lab.org/PLCAPI/'

parser = argparse.ArgumentParser(
    description="retrieve site id and coordinates of planetlab nodes via the PLC API")
parser.add_argument('-i', '--input', dest='input_file', type=str,
                    help='file with list of planetlab nodes, one per line', required=True)
parser.add_argument('-c', '--cache_file', dest='cache_file', type=str, help='cache file', required=True)
parser.add_argument('-o', '--output_file', dest='output_file', type=str,
                    help='dict output (node name --> site_id, coordinates) in json format', required=True)
args = parser.parse_args()

if not os.path.isfile(args.input_file):
    print("No input file {}, please make sure that the file exists".format(
        args.input_file))
    sys.exit(1)

# read all nodes
plnodes_info = {}
with open(args.input_file, 'r') as f:
    plnodes_list = json.load(f)

for plnode_name in plnodes_list:
    plnodes_info[plnode_name] = {}

# check if cache can save us the call to PL API
if not os.path.exists(args.cache_file):
    with open(args.cache_file, 'w') as f:
        json.dump({}, f)

plnodes_cached = {}
with open(args.cache_file, 'r') as f:
    plnodes_cached = json.load(f)

nodes_retrieved = 0
for node in plnodes_info:
    if node in plnodes_cached:
        plnodes_info[node] = plnodes_cached[node]
        nodes_retrieved += 1

if nodes_retrieved == len(plnodes_list):
    # write everything to file and exit
    with open(args.output_file, 'w') as f:
        json.dump(plnodes_info, f, indent=2)
    sys.exit(0)

# call to PL API needed
api_server = xmlrpclib.ServerProxy(PLC_API, allow_none=True)
auth = {}
auth['AuthMethod'] = 'anonymous'
auth['Role'] = 'user'

'''
authorized = api_server.AuthCheck(auth)
if authorized:
    print 'PLC authorization successfull!'
else:
    print 'PLC authorization failed! Aborting...'
    sys.exit(1)
'''

# retrieve the site info and coordinates per node using the PL API
for node in plnodes_info:
    plnodes_cached = {}
    with open(args.cache_file, 'r') as f:
        plnodes_cached = json.load(f)

    if node in plnodes_cached:
        plnodes_info[node] = plnodes_cached[node]
    else:
        returnFields = ['site_id']
        site_info = api_server.GetNodes(auth, {'hostname': node}, returnFields)

        if len(site_info) > 0:
            site_id = site_info[0]['site_id']
            plnodes_info[node]['site_id'] = site_id

            returnFields = ['longitude', 'latitude']
            site_coords = api_server.GetSites(
                auth, {'site_id': site_id}, returnFields)
            if len(site_coords) > 0:
                plnodes_info[node]['longitude'] = site_coords[0]['longitude']
                plnodes_info[node]['latitude'] = site_coords[0]['latitude']
                plnodes_cached[node] = plnodes_info[node]

                with open(args.cache_file, 'w') as f:
                    json.dump(plnodes_cached, f, indent=2)

# write everything to file
with open(args.output_file, 'w') as f:
    json.dump(plnodes_info, f, indent=2)