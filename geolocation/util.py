"""
util.py - provides useful utility functions such as calculating the propagation delay between
two points, given the geographical coordinates of both.
"""
import sys
import os
import math
import ujson
import geopy
from geopy.distance import vincenty
from geopy.distance import great_circle
import difflib
import time
import csv

if sys.version_info >= (3, 0):
    from urllib.request import urlopen
else:
    from urllib import urlopen

def city_strings_match(city_str_1, city_str_2):
    """

    Args:
        city_str_1: string 1 with city name
        city_str_2: string 2 with city name

    Returns:
        whether the city names match with each other
    """

    if city_str_1.lower() in city_str_2.lower() or city_str_2.lower() in city_str_1.lower():
        return True
    elif difflib.SequenceMatcher(None, city_str_1.lower(), city_str_2.lower()).ratio() > 0.8:
        return True

    return False

def city_to_coords(city_str, location_cache=None):
    """
    Args:
        city_str: string with city name, including the country code separated by |)
        e.g., "Zurich|CH"

    Returns:
        Pair of geographical coordinates floats (lat, lon)
    """
    city_cache_file = location_cache
    city_cache_dict = {}
    if not os.path.exists(city_cache_file):
        with open(city_cache_file, 'w') as f:
            ujson.dump(city_cache_dict, f)

    with open(city_cache_file, 'r') as f:
        city_cache_dict = ujson.load(f)

    if city_str not in city_cache_dict:
        normalized_city_str = city_str.replace('|', ',')
        location            = None

        try:
            google_geolocator = geopy.geocoders.GoogleV3()
            location          = google_geolocator.geocode(normalized_city_str)
        except:
            location = None

        time.sleep(1)

        if location is not None:
            city_cache_dict[city_str] = (location.latitude, location.longitude)
        else:
            return (None, None)

    with open(city_cache_file, 'w') as f:
        ujson.dump(city_cache_dict, f, indent=2)

    return city_cache_dict[city_str]

def get_json_data_object(request_url=None):
    """
    Args:
        request_url: string url to call

    Returns:
        utf-8 json decoded object data
    """
    response     = urlopen(request_url)
    str_response = response.read().decode('utf-8')
    obj          = ujson.loads(str_response)

    return obj['data']

def pdb_facility_id_to_coords(fac_id):
    """
    Args:
        fac_id: int facility id as appears in peeringDB database

    Returns:
        dictionary with facility coordinates (based on the hosting city)
    """
    coords = { 'latitude' : None, 'longitude': None }

    pdb_fac_api_url = "https://peeringdb.com/api/fac/" + str(fac_id)

    fac = get_json_data_object(pdb_fac_api_url)
    if fac is not None and len(fac) > 0:
        if 'city' in fac and 'country' in fac:
            fac_city            = "%s,%s" % (fac['city'], fac['country'])
            fac_city_coords     = city_to_coords(fac_city)
            coords['latitude']  = fac_city_coords[0]
            coords['longitude'] = fac_city_coords[1]

    return coords

def haversin(x):
    """
    Return Haversine function of x: (1-cos(x))/2.

    Args:
        x: The input value.

    Returns:
        haversin(x)
    """
    return (1-math.cos(x))/2

def haversin_great_circle_distance(p1,p2):
    """
    Return the great circle distance between two points on Earth.
    The points should be given as pairs of latitude and longitude.

    Args:
        p1: One of the endpoints. Format is a pair of floats (lat,lon).
        p2: One of the endpoints. Format is a pair of floats (lat,lon).

    Returns:
        The great circle distance between p1 and p2, in kilometers, as a
        float.
    """
    # Convert to radians.
    (x1,y1) = math.radians(p1[0]),math.radians(p1[1])
    (x2,y2) = math.radians(p2[0]),math.radians(p2[1])
    # Earth's radius (volumetric mean, in km)
    # from http://nssdc.gsfc.nasa.gov/planetary/factsheet/earthfact.html
    r = 6371.0
    # Formula from https://en.wikipedia.org/wiki/Haversine_formula
    val = haversin(x2-x1)+math.cos(x1)*math.cos(x2)*haversin(y2-y1)
    return r*math.acos(1-2*val)

def geopy_vincenty_distance(p1,p2):
    """
    Return the vincenty distance between two points on Earth.
    The points should be given as pairs of latitude and longitude.

    Args:
        p1: One of the endpoints. Format is a pair of floats (lat,lon).
        p2: One of the endpoints. Format is a pair of floats (lat,lon).

    Returns:
        The great vincenty distance between p1 and p2, in kilometers, as a
        float.
    """
    distance = None
    try:
        distance = vincenty(p1, p2).kilometers
    except ValueError as v:
        distance = geopy_great_circle_distance(p1, p2)

    return distance

def geopy_great_circle_distance(p1, p2):
    """
    Return the great circle distance between two points on Earth.
    The points should be given as pairs of latitude and longitude.

    Args:
        p1: One of the endpoints. Format is a pair of floats (lat,lon).
        p2: One of the endpoints. Format is a pair of floats (lat,lon).

    Returns:
        The great circle distance between p1 and p2, in kilometers, as a
        float.
    """

    return great_circle(p1, p2).kilometers

def propagation_delay(d,n=1.5):
    """
    Return the (direct) propagation delay over given distance.
    The distance should be given in km.
    The optical density may optionally be specified.
    The default of 1.5 is a reasonable assumption for an optic fibre.

    Args:
        d: Distance between the transmission endpoints, in kilometers.
        n: Optical density of the medium.

    Returns:
        The expected propagation delay, in milliseconds, as a float.
    """
    # Speed of light, in km/s.
    # From http://www.britannica.com/EBchecked/topic/559095/speed-of-light
    # Note that this is exact (the speed of light is defined to be this).
    c = 299792.458
    return 1000*n*d/c

def get_cc_to_cont(input_csv_file=None):
    cc_to_cont = {}

    with open(input_csv_file, 'r') as f:
        csv_reader = csv.reader(f, delimiter=',')
        start = False
        for row in csv_reader:

            # ignore the first cv line
            if not start:
                start = True
                continue

            # gather only valid mappings
            if row[1] != '--':
                cc_to_cont[row[0]] = row[1]

    return cc_to_cont
