import logging
import sys
import ujson
import geoip2.database, geoip2.errors
from maxminddb.errors import InvalidDatabaseError
import argparse
import requests


def query_ipapi(target_ips):
    locations = dict()
    
    for target_ip in target_ips:
        print(target_ip)
        url = 'https://ipapi.co/'+target_ip+'/json'
        result = requests.get(url).json()
        
        if 'city' in result and 'country' in result:
            locations[target_ip] = "%s|%s" % (result['city'], result['country'])
        
    return locations

def query_maxmind_batch(maxmind_reader, target_ips):
    """
    Returns the location for each IP in a set of IPs (city name and country 2-letter ISO code)
    based on Maxmind's Database
    :param target_ips: the set IP to geolocate
    :return: a string with the location of the IP
    """
    maxmind_locations    = dict()

    if maxmind_reader is not False:
        for target_ip in target_ips:
            try:
                response = maxmind_reader.city(target_ip)
                if response.country is not None:
                    maxmind_city = response.city.name
                    if maxmind_city is not None:
                        maxmind_city = maxmind_city.lower()
                    maxmind_country = response.country.iso_code

                    if str(maxmind_city) != "None" and str(maxmind_country) != "None":
                        maxmind_locations[target_ip] = "%s|%s" % (maxmind_city, maxmind_country)
            except geoip2.errors.AddressNotFoundError:
                print("IP %s was not found in Maxmind GeoIP DB." % target_ip)
                continue
            except ValueError:
                print("Skipping value '%s' which doesn't appear to be a valid IP address." % target_ip)
                continue

    return maxmind_locations

def main():
    parser = argparse.ArgumentParser(description="geolocate ips via maxmind")
    group = parser.add_mutually_exclusive_group(required=True)
    
    parser.add_argument('-i', '--ips_file', dest='ips_file', type=str,
                        help='txt file with ips to geolocate', required=True)
    group.add_argument('-m', '--maxm_db_file', dest='maxm_db_file', type=str,
                        help='maxmind db file')
    group.add_argument('-a', '--ipapi', action='store_true',
                        help='geolocation with ipapi')
    parser.add_argument('-o', '--out_file', dest='out_file', type=str,
                        help='file where the results are stored in json format', required=True)
    args = parser.parse_args()
    
    ip_locations = {}
    target_ips = []
    with open(args.ips_file, 'r') as f:
        for l in f.readlines():
            target_ips.append(l.strip())
    
    if args.maxm_db_file:
    
        maxmind_reader = False
        try:
            maxmind_reader = geoip2.database.Reader(args.maxm_db_file)
        except (IOError, InvalidDatabaseError) as e:
            print("Reading Maxmind DB filed failed with error: %s" % (str(e)))
            
        if maxmind_reader is not False:
            ip_locations = query_maxmind_batch(maxmind_reader, target_ips)
                
    elif args.ipapi:
        ip_locations = query_ipapi(target_ips)

    with open(args.out_file, 'w') as f:
        ujson.dump(ip_locations, f)

if __name__ == '__main__':
    main()
