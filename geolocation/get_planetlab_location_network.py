#!/usr/bin/env python3

import SubnetTree
import socket
import argparse, sys, ujson, geopy, requests, os
import pprint
pp = pprint.PrettyPrinter(indent=2)

total = {}

class geolocate_planetlab_nodes():
    def __init__(self):
        
        self.google_api_key = 'put your google api key here'
        self.google_geolocator = geopy.geocoders.GoogleV3(api_key = self.google_api_key)
        
        
    def import_data(self, filename):
        with open(filename, 'r') as file:
            return ujson.load(file)


    def export_data(self, data, filename):
        with open(filename, 'w') as file:
            ujson.dump(data, file, indent=2)


    def retrieve_geolocate_google(self, lat, lng):
        
        url='https://maps.googleapis.com/maps/api/geocode/json?latlng='+str(lat)+','+str(lng)+'&language=en&key='+self.google_api_key
        return requests.get(url).json()
        
    
    def retrieve_geolocate_bing(self,flag,lat,lng):
        url='http://dev.virtualearth.net/REST/v1/Locations/'+str(lat)+','+str(lng)+'?key='+self.bing_api_key
        return requests.get(url).json()
    
    
    def reverse_geolocate_google(self, path):
        global total
        files = [file for file in os.listdir(path)]
        
        for json_file in files:
            unique_countries = {}
            result = self.import_data(path+json_file)
            {"results":[],"status":"ZERO_RESULTS"}
            if 'results' in result and result['status']!='ZERO_RESULTS':
                for entry in result['results']:
                    if 'address_components' in entry:
                        for loc in entry['address_components']:
                            if 'types' in loc:
                                # For countries  
                                if 'country' in loc['types']:
                                    if 'short_name' in loc:
                                        unique_countries['short_name'] = loc['short_name']
                                    if 'long_name' in loc:
                                        unique_countries['long_name'] = loc['long_name']              
                                                                 
                node_id = json_file.split('.json')[0]
                total[node_id]={}
                
                if unique_countries: total[node_id] = unique_countries
                
        print('Geolocated:',len(total),'out of',len(files))    
        

    def map_ip_to_asn(self,path):
        global total
        tree=SubnetTree.SubnetTree()
        prfx2asn_mappings = self.import_data('../planetlab/data/routeviews.json')

        for prefix,asn in prfx2asn_mappings.items():
            tree[prefix] = asn
        
        current=1
        for node in total:
            
            try:
                total[node]['ip']  = socket.gethostbyname(node)
                total[node]['asn'] = tree[total[node]['ip']]
                print(current,node)
                current+=1
            except:
                pass
            
        self.export_data(total,path+'total.json')

    
def main():

    dir_cache = './data/planetlab/'
    g_r_p = geolocate_planetlab_nodes()
    
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-g','--google', action='store_true')
    group.add_argument('-rg','--reverse-google', action='store_true')
    options=parser.parse_args()
    
    planetlab_coordinates = g_r_p.import_data('planetlab_nodes_coordinates.json')
    plb_cords = {plb[0]:plb[1] for plb in planetlab_coordinates.items() if 'longitude' in plb[1] and 'latitude' in plb[1] }
   
    if options.google:
        to_geolocate = plb_cords.keys() - set(file.split('.json')[0] for file in os.listdir(dir_cache))
        
        current=1
        for node_id in to_geolocate:
            lat = plb_cords[node_id]['latitude']
            lon = plb_cords[node_id]['longitude']
        
            geo_data = g_r_p.retrieve_geolocate_google(lat,lon)
            g_r_p.export_data(geo_data,dir_cache+node_id+'.json')

            print(current,node_id,lon,lat)
            current+=1
            
    elif options.reverse_google:
        g_r_p.reverse_geolocate_google(dir_cache)
        g_r_p.map_ip_to_asn(dir_cache)
    

if __name__ == "__main__":
    main()
