#!/usr/bin/env python3

# example run:
# python3 get_colo_coordinates.py -i ../colo_tools/data/final_checked_colo_ips.json
# -f ../colo_tools/data/pdb_facs.json
# -l data/city_geoloc.json
# -o data/geolocated_colo_ips.json

import sys
import os
import argparse
import json as json
import util
from pprint import pprint as pp


parser = argparse.ArgumentParser(description="retrieve coordinates of colo-ips")
parser.add_argument('-i', '--to_geo_ip_json', dest='to_geo_ip_json', type=str, help='json file with ip dict to geolocate', required=True)
parser.add_argument('-f', '--pdb_facs_file', dest='pdb_facs_file', type=str, help='json file with PeeringDB facility info', required=True)
parser.add_argument('-l', '--location_cache_file', dest='location_cache_file', type=str, help='location cache file', required=True)
parser.add_argument('-o', '--output_file', dest='output_file', type=str, help='dict output (colo IP--> coordinates) in json format', required=True)
args = parser.parse_args()

print("Loading colo IPs to geolocate")
to_geo_ips = {}
with open(args.to_geo_ip_json, 'r') as f:
    to_geo_ips = json.load(f)

print("Loading PDB facilities")
pdb_fac_list = []
with open(args.pdb_facs_file, 'r') as f:
    pdb_fac_list = json.load(f)

pdb_facs = {}
for fac in pdb_fac_list:
    pdb_facs[fac['id']] = fac

print('Retrieving colo IP coordinates...')

ips_geoloc     = {}
location_cache = args.location_cache_file

for ip in to_geo_ips:
    ip_fac         = to_geo_ips[ip]['facilities_intersection'][0]
    ip_fac_city    = pdb_facs[ip_fac]['city']
    ip_fac_country = pdb_facs[ip_fac]['country']
    fac_city_str   = ip_fac_city + '|' + ip_fac_country

    cached = False
    if os.path.exists(location_cache):
        with open(location_cache, 'r') as f:
            city_cache_dict = json.load(f)
            if fac_city_str in city_cache_dict:
                cached_loc = city_cache_dict[fac_city_str]
                (lat, lon) = (cached_loc[0], cached_loc[1])
                cached     = True

    if not cached:
        (lat, lon) = util.city_to_coords(fac_city_str, location_cache)

    ips_geoloc[ip] = {
        'city'      : ip_fac_city,
        'country'   : ip_fac_country,
        'fac'       : ip_fac,
        'latitude'  : lat,
        'longitude' : lon
    }

print('Retrieved Colo-IP coordinates')

# write everything to file
with open(args.output_file, 'w') as f:
    json.dump(ips_geoloc,f,indent=1)