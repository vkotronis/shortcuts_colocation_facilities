#!/bin/bash

# http://stackoverflow.com/questions/670191/getting-a-source-not-found-error-when-using-source-in-a-bash-script

cd /home/vkotronis/colos/scripts
script_to_monitor='super_script.py'
if ps aux | grep $script_to_monitor | grep -v 'grep' > /dev/null
then
  echo "script is already running"
else
  /usr/bin/python3 -u $script_to_monitor >> /home/vkotronis/colos/scripts/super_script.log;
fi
