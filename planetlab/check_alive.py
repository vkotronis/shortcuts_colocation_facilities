#!/usr/bin/env python3
"""Friendly Python SSH2 interface."""

import os
import paramiko
import sys
import concurrent.futures
from multiprocessing import cpu_count
import argparse
import json


class Connection(object):
    """Connects and logs into the specified hostname.
    Arguments that are not given are guessed from the environment."""

    def __init__(self, host, username=None, private_key_file=None, password=None):
        self.client = paramiko.SSHClient()

        if password:
            self.client.connect(host, username=username, password=password)
        else:
            rsa_key = paramiko.RSAKey.from_private_key_file(private_key_file)
            self.client.load_system_host_keys()
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.client.connect(host, username=username, pkey=rsa_key)

    def execute(self, command, read=False):
        if read:
            self.client.exec_command(command)
        else:
            stdin, stdout, stderr = self.client.exec_command(command)
            return ''.join(stdout.readlines())

    def close(self):
        if self.client:
            self.client.close()

    def put(self, file):
        sftp = self.client.open_sftp()
        sftp.put(file, file)
        sftp.close()

    def __del__(self):
        self.close()


def main():
    """Little test when called directly."""
    # Set these to your own details.

    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input', required=True, type=str)
    parser.add_argument('-o', '--output', required=True, type=str)
    parser.add_argument('--key', required=True, type=str)

    args = parser.parse_args()

    with open(args.input, 'r') as f:
        data = list(f)

    alive_hosts = []

    def try_connect(line):
        username, host = line.split()

        try:
            myssh = Connection(host=host, username=username,
                               private_key_file=args.key)
            print(host + ' --- alive')
            alive_hosts.append(host)
            myssh.close()
        except Exception as e:
            print(host + ' --- dead')

    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(try_connect, data)

    with open(args.output, 'w') as f:
        json.dump(alive_hosts, f, indent=1)

# start the ball rolling.
if __name__ == "__main__":
    main()
