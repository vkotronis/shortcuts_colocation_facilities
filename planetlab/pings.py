import subprocess 
import thread
import time
import threading
import datetime

lock = threading.Lock()
threads = []

destination_nodes = ['google.com', 'facebook.com']

def write_to_file_ping(host):
	ping_response = subprocess.Popen(["/bin/ping", "-qc10", str(host)], stdout=subprocess.PIPE)
	beautify_response = subprocess.Popen(['grep', 'rtt'], stdout=subprocess.PIPE, stdin=ping_response.stdout)
	lock.acquire()
	with open('pings','a+') as f:
		f.write(time.strftime("%c") +'\t' + str(host) + '\t' + str(beautify_response.communicate()[0].split()) + '\n')
	lock.release()
	print "finished ping to %s" % (host)

def run_pings():
	for node in destination_nodes:
		print "running ping to %s" % (node)
		thread.start_new_thread(write_to_file_ping,(node,))

while(1):
    a = datetime.datetime.now()
    if (a.minute % 6) == 0 and (a.second / 1) == 1:
        run_pings()
    time.sleep(1)
