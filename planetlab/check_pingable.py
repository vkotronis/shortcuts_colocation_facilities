#!/usr/bin/python3

import concurrent.futures
from multiprocessing import cpu_count
import sys
import os
import json
import argparse


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input', required=True, type=str)
    parser.add_argument('-o', '--output', required=True, type=str)

    args = parser.parse_args()

    with open(args.input, 'r') as f:
        data = json.load(f)

    active_hosts = []

    def try_ping(hostname):
        response = os.system("ping -c 1 " + hostname)
        if response == 0:
            print(hostname + ' pingable')
            active_hosts.append(hostname)
        else:
            print(hostname + ' not pingable')

    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(try_ping, data)

    with open(args.output, 'w') as f:
        json.dump(active_hosts, f, indent=1)


if __name__ == '__main__':
    main()
