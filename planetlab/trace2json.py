#!/usr/bin/env python

import ujson as json
import sys
from pprint import pprint

json_list = []

id = 1

if len(sys.argv) != 3:
	sys.exit(0)

entry = {}

with open(sys.argv[1],'r') as f:
	for line in f:
		if 'traceroute to' in line:
			if entry and 'result' in entry.keys():
				json_list.append(entry)

			entry = {}
			entry['dst'] = (line.split('traceroute to ',1)[1]).split(' ')[0]
			entry['id'] = id
			entry['src'] = str(sys.argv[2])
			entry['info'] = ''
			id+=1
		elif line != '\n':
			if not 'result' in entry.keys():
				entry['result'] = {}

			data = filter(None,line.split(' '))
			hop = 'hop' + str(int(data[0])-1)
			
			if data[1] != '*':
				ip = data[2][1:-1]
			else:
				ip = data[1]

			entry['result'][hop] = {
				'from': ip,
				'info': ' '.join(data[3:]).strip()
			}

	if entry and 'result' in entry.keys():
		json_list.append(entry)

with open(sys.argv[1]+'.json','w') as f:
	json.dump(json_list, f, indent=4)