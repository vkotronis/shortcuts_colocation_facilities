#!/usr/bin/env python3

import sys
sys.path.append('..')
import geolocation.util
import glob
import os
import argparse
import numpy
import lib_analysis as liba
import pprint
pp = pprint.PrettyPrinter(indent=2)

total = {}
total_rae2rae = {}
total_rae2cor = {}
total_rae2plr = {}
total_rae2rar_eye = {}
total_rae2rar_other = {}

MIN_STEP  = 1
MAX_STEP  = 10

type2total = {
    'rae2cor'       : total_rae2cor,
    'rae2plr'       : total_rae2plr,
    'rae2rar_eye'   : total_rae2rar_eye,
    'rae2rar_other' : total_rae2rar_other
}


def augment_with_asn_cc_info(msm_dir):
    global total

    cc_to_cont = geolocation.util.get_cc_to_cont(input_csv_file='../geolocation/data/country_continent.csv')
        
    plb_asn_cc  = liba.import_data('../geolocation/data/planetlab/total.json')
    cor_cc      = liba.import_data(msm_dir+'/sampled_cor.json')
    cor_asn     = liba.import_data(msm_dir+'/initial_colo_files/pingable_checked_colo_ips.json')
    
    to_delete = {}
    for dest,src in total:
        for type in ['rae2rae','rae2rar_eye','rae2rar_other']:
            cc_asn = liba.import_data(msm_dir+'/sampled_'+type.split('2')[1]+'.json')
            
            for relay in total[(dest,src)][type]:
                total[(dest,src)][type][relay]['cc'] = cc_asn[relay]['cc']
                total[(dest,src)][type][relay]['asn'] = str(cc_asn[relay]['asn_v4'])
                total[(dest,src)][type][relay]['cont'] = cc_to_cont[cc_asn[relay]['cc']]
                if type == 'rae2rar_eye':
                    if relay==dest or relay==src:
                        to_delete[(dest,src)] = relay
        
        for relay in total[(dest,src)]['rae2cor']:
            total[(dest,src)]['rae2cor'][relay]['fac'] = cor_cc[relay]['fac']
            total[(dest,src)]['rae2cor'][relay]['cc'] = cor_cc[relay]['country']
            total[(dest,src)]['rae2cor'][relay]['asn'] = str(cor_asn[relay]['asn'])
            total[(dest,src)]['rae2cor'][relay]['cont'] = cc_to_cont[cor_cc[relay]['country']]
    
        for relay in total[(dest,src)]['rae2plr']:
            if relay in plb_asn_cc:
                total[(dest,src)]['rae2plr'][relay]['cc'] = plb_asn_cc[relay]['short_name']
                total[(dest,src)]['rae2plr'][relay]['cont'] = cc_to_cont[plb_asn_cc[relay]['short_name']]
                if 'asn' in plb_asn_cc[relay]:
                    total[(dest,src)]['rae2plr'][relay]['asn'] = str(plb_asn_cc[relay]['asn'])
       
    for (pair,relay) in to_delete.items():
        del total[pair]['rae2rar_eye'][relay]
    
        
def get_median(params, data):
    avges = [packet['avg'] for packet in data if packet['avg']>0]
    
    if not len(avges):
        return False
    elif len(avges)>0 and len(avges)<3: 
        #print(params[0],len(avges),'packet out of 6 in:',params[1])
        return False
    elif len(avges):
        return numpy.median(avges)


def parse_measurement(results, issue, type):
    global total_rae2rae
    
    for result in results:
        if result['msm_id'] in issue:
            for source in result['msm_results']:
                median = get_median([(result['dst'],source),type],result['msm_results'][source])
                if median:
                    if type=='rae2rae':
                        if ((result['dst'],source) not in total_rae2rae):
                            total_rae2rae[(result['dst'],source)] = median
                        #else:
                        #    print((result['dst'],source),'already exists in:',type)
                    else:
                        type2total[type].setdefault(source,{})
                        type2total[type][source][result['dst']] = median
    
    
def get_valid_latencies(type, dest, src):

    common_relays = type2total[type][dest].keys() & type2total[type][src].keys()
    
    return {relay:{'median':type2total[type][dest][relay]+type2total[type][src][relay]} for relay in common_relays}
    

def map_pairs(msm_dir):
    global total
    
    for dest,src in total_rae2rae:
        total.setdefault((dest,src),{})
        for type in type2total:
            total[(dest,src)].setdefault(type,{})
            if dest in type2total[type] and src in type2total[type]:
                total[(dest,src)][type] = get_valid_latencies(type,dest,src)
        if (dest,src) in total:
            total[(dest,src)]['rae2rae'] = {dest:{'median':total_rae2rae[(dest,src)]},src:{'median':total_rae2rae[(dest,src)]}}
                

def fix_probe_ids(msm_dir):

    to_fix = ['rae2rar_eye','rae2rar_other']
    mapIP2ID = {}
    
    mapIP2ID['rae2rar_eye'] = liba.get_prb_ip_to_id(msm_dir+'/sampled_rar_eye.json')
    mapIP2ID['rae2rar_other'] = liba.get_prb_ip_to_id(msm_dir+'/sampled_rar_other.json')
    
    for type in to_fix:
        for source in type2total[type]:
            type2total[type][source] = {mapIP2ID[type][key]:value for (key,value) in type2total[type][source].items()}
    

def empty_dict():
    global total,total_rae2rae,total_rae2cor,total_rae2plr,total_rae2rar_eye,total_rae2rar_other
    total.clear()
    total_rae2rae.clear()
    total_rae2cor.clear()
    total_rae2plr.clear()
    total_rae2rar_eye.clear()
    total_rae2rar_other.clear()
    
    
def issue_msm_ids(data):
    return [msm[0] for msm in data]       
            

def export_sub_measurements():
    for type in type2total:
        if type!='rae2rae':
            liba.export_data(type+'.json',type2total[type])        
        else:
            liba.export_data(type+'.json',total_rae2rae)
            

def start_parsing(msm_dir):

    metadata = liba.import_data('{}/metadata.json'.format(msm_dir))

    results_rae2rae = liba.import_data(msm_dir+'/rae2rae_results/results.json')
    results_rae2cor = liba.import_data(msm_dir+'/rae2cor_results/results.json')
    results_rae2plr = liba.import_data(msm_dir+'/rae2plr_results/results.json')
    results_rae2rar_eye = liba.import_data(msm_dir+'/rae2rar_eye_results/results.json')
    results_rae2rar_other = liba.import_data(msm_dir+'/rae2rar_other_results/results.json')
    
    issue_rae2rae = liba.import_data(msm_dir+'/rae2rae_results/issue.json')
    issue_rae2cor = liba.import_data(msm_dir+'/rae2cor_results/issue.json')
    issue_rae2plr = liba.import_data(msm_dir+'/rae2plr_results/issue.json')
    issue_rae2rar_eye = liba.import_data(msm_dir+'/rae2rar_eye_results/issue.json')
    issue_rae2rar_other = liba.import_data(msm_dir+'/rae2rar_other_results/issue.json')
    
    parse_measurement(results_rae2rae,issue_msm_ids(issue_rae2rae),'rae2rae')
    parse_measurement(results_rae2cor,issue_msm_ids(issue_rae2cor),'rae2cor')
    parse_measurement(results_rae2plr,issue_msm_ids(issue_rae2plr),'rae2plr')
    parse_measurement(results_rae2rar_eye,issue_msm_ids(issue_rae2rar_eye),'rae2rar_eye')
    parse_measurement(results_rae2rar_other,issue_msm_ids(issue_rae2rar_other),'rae2rar_other')
    
    fix_probe_ids(msm_dir)    
    map_pairs(msm_dir)
    #export_sub_measurements()
    augment_with_asn_cc_info(msm_dir)
    
    #Export final dataset
    liba.export_data(msm_dir+'_valid_ping_medians.json',total)
    empty_dict()


def main():
    parser = argparse.ArgumentParser(description="retrieve valid ping RTT medians from all msms")
    parser.add_argument('-m', '--all_msms_dir', dest='all_msms_dir', type=str, help='directory with all msms', required=True)
    args = parser.parse_args()

    for msm_tar_gz in glob.iglob('{}/msm_*.tar.gz'.format(args.all_msms_dir)):

        msm_dir  = msm_tar_gz.split('.tar.gz')[0]
        if not os.path.exists(msm_dir):
            liba.extract_file(msm_tar_gz, args.all_msms_dir)

        msm_id    = int(msm_dir.split('/')[-1].split('msm_')[1])
        is_valid  = liba.get_valid(msm_dir)
        last_step = liba.get_step(msm_dir)

        if is_valid and last_step == MAX_STEP and not os.path.exists(msm_dir+'_valid_ping_medians.json'):
            print(msm_dir,'- Is valid:',is_valid,'- Last step:',last_step)
            start_parsing(msm_dir)

        # when done, delete the created msm dir to save disk space
        liba.delete_directory(msm_dir)

if __name__ == "__main__":
    main()
