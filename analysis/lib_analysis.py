#!/usr/bin/env python3

import sys
import os
import glob
import random
import time
import ujson as json
from pprint import pprint as pp
from collections import defaultdict
import concurrent.futures
import threading
import tarfile
import zipfile

ALL_MSM_DATA_DIR = '/home/vkotronis/colos/all_msm_data'
ALL_MSM_SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
MSM_INTERVAL = 12 * 3600  # 12 hours
MIN_STEP = 1
MAX_STEP = 10
MAX_INTER_STEP_DURATION = 1 * 3600  # 1 h
EYEBALL_CUTOFF = 10.0
MAX_IPS_PER_FAC = 3
MAX_PLS_PER_SITE = 2
PING_INTERVAL = 5 * 60  # 5 minutes
PING_PKTS = 6

def get_valid(msm_dir=None):
    valid = True
    try:
        with open('{}/metadata.json'.format(msm_dir), 'r') as f:
            metadata = json.load(f)
            valid = metadata['valid']
    except:
        valid = True

    return valid

def get_step(msm_dir=None):
    step = 0
    try:
        with open('{}/metadata.json'.format(msm_dir), 'r') as f:
            metadata = json.load(f)
            step = metadata['step']
    except:
        return 0

    return step


def get_all_start_time(msm_dir=None):
    try:
        with open('{}/metadata.json'.format(msm_dir), 'r') as f:
            metadata = json.load(f)
    except:
        return None

    return metadata['step_times']['{}'.format(MIN_STEP)]['start_time']


def get_all_end_time(msm_dir=None):
    try:
        with open('{}/metadata.json'.format(msm_dir), 'r') as f:
            metadata = json.load(f)
    except:
        return None

    return metadata['step_times']['{}'.format(MAX_STEP)]['end_time']

def extract_file(file_path=None, to_directory=None):
    if file_path.endswith('.zip'):
        (opener, mode) = (zipfile.ZipFile, 'r')
    elif file_path.endswith('.tar.gz') or path.endswith('.tgz'):
        (opener, mode) = (tarfile.open, 'r:gz')
    elif file_path.endswith('.tar.bz2') or file_path.endswith('.tbz'):
        (opener, mode) = (tarfile.open, 'r:bz2')
    else:
        raise ValueError("Could not extract `{}` as no appropriate extractor is found".format(file_path))

    file_opener = opener(file_path, mode)
    try:
        file_opener.extractall(path=to_directory)
    finally:
        file_opener.close()

def delete_directory(dir=None):
    os.system('rm -r {}'.format(dir))

def import_data(filename=None):
    try:
        with open(filename, 'r') as file:
            return json.load(file)
    except:
        print("Error loading file.")

def export_data(filename=None, data={}):
    with open(filename, 'w') as f:
        json.dump(data, f)

def get_prb_ip_to_id(prb_info_file=None):
    prb_info = import_data(prb_info_file)

    prb_ip_to_id = {}
    for prb_id in prb_info.keys():

        if prb_info[prb_id]['address_v4'] in prb_ip_to_id:
            raise Exception("same IP for different probes!")

        prb_ip_to_id[prb_info[prb_id]['address_v4']] = prb_id

    return prb_ip_to_id
