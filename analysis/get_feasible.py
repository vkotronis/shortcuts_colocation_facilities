#!/usr/bin/env python3

import os
import argparse
import glob
import lib_analysis as liba

MIN_STEP  = 1
MAX_STEP  = 10
RA_TYPES  = ['rae', 'rar_eye', 'rar_other']
RAR_TYPES = ['rar_eye', 'rar_other']

parser = argparse.ArgumentParser(description="retrieve results related to the numbers of feasible relays per pair")
parser.add_argument('-m', '--all_msms_dir', dest='all_msms_dir', type=str, help='directory with all msms', required=True)
parser.add_argument('-o', '--out_file', dest='out_file', type=str, help='file where the results are stored in json format', required=True)
args = parser.parse_args()

feasible_relays_per_pair = {
    'cor' : []
    'plr' : []
    'rar_other' : []
    'rar_eye' : []
}



    for msm_tar_gz in glob.iglob('{}/msm_*.tar.gz'.format(args.all_msms_dir)):
        msm_dir  = msm_tar_gz.split('.tar.gz')[0]
        if not os.path.exists(msm_dir):
            liba.extract_file(msm_tar_gz, args.all_msms_dir)

        msm_id    = int(msm_dir.split('/')[-1].split('msm_')[1])
        is_valid  = liba.get_valid(msm_dir)
        last_step = liba.get_step(msm_dir)

        print(msm_dir,'- Is valid:',is_valid,'- Last step:',last_step)

        if is_valid and last_step == MAX_STEP:
            start_parsing(msm_dir)

        # when done, delete the created msm dir to save disk space
        liba.delete_directory(msm_dir)

    print('Removed rae2rae pairs with common rae2rar_eye relays:', total_removed_rae2rae_with_rae2rar_eye)
    print('Pairs without responses:',total_pairs_without_responses,'- {:.2f}%'.format((total_pairs_without_responses/total_pairs)*100))
    print('Pairs with less than three responses:',total_pairs_with_less_than_three_responses,'- {:.2f}%'.format((total_pairs_with_less_than_three_responses/total_pairs)*100))
    print('Pairs with more than three responses:',total_pairs_with_more_than_three_responses,'- {:.2f}%'.format((total_pairs_with_more_than_three_responses/total_pairs)*100))
    print('Rae2rae pairs:',total_rae2rae_pairs)
    print('Rae2rae pairs with cor:',total_rae2rae_with_cor)
    print('Rae2rae pairs with plr:',total_rae2rae_with_plr)
    print('Rae2rae pairs with eye:',total_rae2rae_with_eye)
    print('Rae2rae pairs with other:',total_rae2rae_with_other)
    print('Rae2rae pairs with all:',total_rae2rae_with_all)
    print('Total pings:',total_pings)

if __name__ == "__main__":
    main()

for msm_tar_gz in glob.iglob('{}/msm_*.tar.gz'.format(args.all_msms_dir)):
    msm_dir  = msm_tar_gz.split('.tar.gz')[0]

    if not os.path.exists(msm_dir):
        liba.extract_file(msm_tar_gz, args.all_msms_dir)

    msm_id    = int(msm_dir.split('/')[-1].split('msm_')[1])

    is_valid  = liba.get_valid(msm_dir)
    last_step = liba.get_step(msm_dir)

    if is_valid and last_step == MAX_STEP:

        # populate set sizes from feasible sets (RAR_EYE, RAR_OTHER, COR, PLR)
        for rar_type in RAR_TYPES:
            feasible_rar_dict = liba.import_data('{}/feasible_{}.json'.format(msm_dir, rar_type))
            feasible_rar = set()
            for v in feasible_rar_dict.values():
                feasible_rar.update(set(v))
            results[msm_id]['ripe_atlas']['feasible'][rar_type] = len(feasible_rar)

        feasible_cor_dict = liba.import_data('{}/feasible_cor.json'.format(msm_dir))
        feasible_cor = set()
        for v in feasible_cor_dict.values():
            feasible_cor.update(set(v))
        results[msm_id]['colos']['feasible'] = len(feasible_cor)

        feasible_plr_dict = liba.import_data('{}/feasible_plr.json'.format(msm_dir))
        feasible_plr = set()
        for v in feasible_plr_dict.values():
            feasible_plr.update(set(v))
        results[msm_id]['planetlab']['feasible'] = len(feasible_plr)

    # when done, delete the created msm dir to save disk space
    liba.delete_directory(msm_dir)

# dump the results to file
liba.export_data(args.out_file, results)
