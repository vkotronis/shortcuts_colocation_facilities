#!/usr/bin/env python

import sys
import os
import glob
import argparse
import re
import time
import ujson as json
import lib_analysis as liba
# for reverse geocoding coords to city
# https://pypi.python.org/pypi/reverse_geocode/1.0
#import reverse_geocode
import reverse_geocoder as rg
from pprint import pprint as pp


MIN_STEP = 1
MAX_STEP = 10
REL_TYPES = [
    'rae2cor',
    'rae2rar_eye',
    'rae2rar_other',
    'rae2plr'
]


def calc_geo_city_to_coords(geo_dict, geo_coords_to_city={}):
    coords_list =[]
    for node in sorted(geo_dict.keys()):
        lat = geo_dict[node]['latitude']
        lon = geo_dict[node]['longitude']
        coords = (lat, lon)
        if coords not in geo_coords_to_city:
            coords_list.append(coords)
    if len(coords_list) > 0:
        rev_geo = rg.search(coords_list)
    for i, coords in enumerate(coords_list):
        geo_coords_to_city[coords] = (rev_geo[i]['name'], rev_geo[i]['cc'])

    return geo_coords_to_city


def calc_node_to_city(node, geo_dict, geo_coords_to_city={}):
    lat = geo_dict[node]['latitude']
    lon = geo_dict[node]['longitude']
    coords = (lat, lon)

    return geo_coords_to_city[coords]


parser = argparse.ArgumentParser(description="calculate all node-to-node medians")
parser.add_argument('-i', '--input', dest='input_dir', type=str, help='directory with all valid_ping_median jsons', required=True)
parser.add_argument('-g', '--geo_coords_to_city', dest='geo_coords_to_city', type=str, help='cache file with coords to city mappings', required=True)
parser.add_argument('-o', '--output', dest='output_file', help='file with dict with values of rtts per city pair (symmetric)', required=True)
parser.add_argument('-p', '--pdb_facs', type=str, help='file with all pdb facilities', default=None)
args = parser.parse_args()

#  load pdb facilities, if feasible
pdb_facs = {}
if args.pdb_facs != None:
    pdb_fac_list = []
    with open(args.pdb_facs, 'r') as f:
        pdb_fac_list = json.load(f)

    for fac in pdb_fac_list:
        pdb_facs[fac['id']] = fac

# initialize results (RTTs per node pair)
results = {}

# initialize geolocation information
if not os.path.isfile(args.geo_coords_to_city):
    geo_coords_to_city = {}
else:
    geo_coords_to_city = liba.import_data(args.geo_coords_to_city)

# gather results
for msm_res_json in sorted(glob.iglob('{}/msm_*_valid_ping_medians.json'.format(args.input_dir))):
    msm_round = None
    match = re.match('.*msm_(\d+)_valid_ping_medians.json', msm_res_json)
    if match:
        msm_round = int(match.group(1))
    else:
        sys.exit(1)

    print('Processing msm {}'.format(msm_round))

    msm_tar_gz = '{}/msm_{}.tar.gz'.format(args.input_dir, msm_round)
    msm_dir = msm_tar_gz.split('.tar.gz')[0]

    if not os.path.exists(msm_dir):
        liba.extract_file(msm_tar_gz, args.input_dir)

    # retrieve geolocation information
    rae_classifier = liba.import_data('{}/initial_ra_files/ra_classifier_ret_dict.json'.format(msm_dir))
    timestamp = rae_classifier['timestamp']

    geo_rae = liba.import_data('{}/initial_ra_files/eyeball_{}.json'.format(msm_dir, timestamp))
    geo_coords_to_city = calc_geo_city_to_coords(geo_rae, geo_coords_to_city)
    geo_rar_eye = geo_rae
    geo_coords_to_city = calc_geo_city_to_coords(geo_rar_eye, geo_coords_to_city)
    geo_rar_other = liba.import_data('{}/initial_ra_files/unknown_{}.json'.format(msm_dir, timestamp))
    geo_coords_to_city = calc_geo_city_to_coords(geo_rar_other, geo_coords_to_city)
    geo_cor = liba.import_data('{}/initial_colo_files/geolocated_pingable_checked_colo_ips.json'.format(msm_dir))
    geo_coords_to_city = calc_geo_city_to_coords(geo_cor, geo_coords_to_city)
    geo_plr = liba.import_data('{}/initial_pl_files/geolocated_pingable_alive_planetlab_nodes.json'.format(msm_dir))
    geo_coords_to_city = calc_geo_city_to_coords(geo_plr, geo_coords_to_city)

    # save cache
    liba.export_data(args.geo_coords_to_city, geo_coords_to_city)

    # for cor in geo_cor:
    #     lat = geo_cor[cor]['latitude']
    #     lon = geo_cor[cor]['longitude']
    #     coords = (lat, lon)
    #     print('FROM GEO')
    #     print(geo_coords_to_city[coords])
    #     print('FROM DATA')
    #     print('{}'.format((geo_cor[cor]['city'], geo_cor[cor]['country'])))
    #     raw_input()

    with open(msm_res_json, 'r') as f:
        this_res = json.load(f)

    for pair in this_res:
        for type in REL_TYPES:
            if len(this_res[pair][type].values()) > 0:

                rae_src = eval(pair)[1]
                rae_src_city = calc_node_to_city(rae_src, geo_rae, geo_coords_to_city)
                rae_dst = eval(pair)[0]
                rae_dst_city = calc_node_to_city(rae_dst, geo_rae, geo_coords_to_city)

                city_key_1 = rae_src_city
                city_key_2 = rae_dst_city
                if city_key_1 != city_key_2:
                    if city_key_1 not in results:
                        results[city_key_1] = {}
                    if city_key_2 not in results[city_key_1]:
                        results[city_key_1][city_key_2] = []
                    if city_key_2 not in results:
                        results[city_key_2] = {}
                    if city_key_1 not in results[city_key_2]:
                        results[city_key_2][city_key_1] = []
                    rae2rae_median = this_res[pair]['rae2rae'][rae_src]['median']
                    results[city_key_1][city_key_2].append(rae2rae_median)
                    results[city_key_2][city_key_1].append(rae2rae_median)

                for rel in this_res[pair][type].keys():
                    for city_key_1 in [rae_src_city, rae_dst_city]:

                        rel_city = None
                        if type == 'rae2cor':
                            rel_city = calc_node_to_city(rel, geo_cor, geo_coords_to_city)
                        elif type == 'rae2rar_eye':
                            rel_city = calc_node_to_city(rel, geo_rar_eye, geo_coords_to_city)
                        elif type == 'rae2rar_other':
                            rel_city = calc_node_to_city(rel, geo_rar_other, geo_coords_to_city)
                        elif type == 'rae2plr':
                            rel_city = calc_node_to_city(rel, geo_plr, geo_coords_to_city)
                        assert rel_city is not None, 'ERROR!'
                        city_key_2 = rel_city

                        if city_key_1 != city_key_2:
                            if city_key_1 not in results:
                                results[city_key_1] = {}
                            if city_key_2 not in results[city_key_1]:
                                results[city_key_1][city_key_2] = []
                            if city_key_2 not in results:
                                results[city_key_2] = {}
                            if city_key_1 not in results[city_key_2]:
                                results[city_key_2][city_key_1] = []

                            rae2rel_median = this_res[pair][type][rel]['median']
                            results[city_key_1][city_key_2].append(rae2rel_median)
                            results[city_key_2][city_key_1].append(rae2rel_median)

    # when done, delete the created msm dir to save disk space
    liba.delete_directory(msm_dir)

    print('Processed msm {}'.format(msm_round))

print('Dumping all results to json')
liba.export_data(args.output_file, results)
print('Dumped all results to json')
