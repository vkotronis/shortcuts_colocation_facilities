#!/usr/bin/env python3

import os
import argparse
import glob
import lib_analysis as liba

MIN_STEP  = 1
MAX_STEP  = 10
RA_TYPES  = ['rae', 'rar_eye', 'rar_other']
RAR_TYPES = ['rar_eye', 'rar_other']

parser = argparse.ArgumentParser(description="retrieve results related to the timing of the msms")
parser.add_argument('-m', '--all_msms_dir', dest='all_msms_dir', type=str, help='directory with all msms', required=True)
parser.add_argument('-o', '--out_file', dest='out_file', type=str, help='file where the results are stored in json format', required=True)
args = parser.parse_args()

results = {}

for msm_tar_gz in glob.iglob('{}/msm_*.tar.gz'.format(args.all_msms_dir)):
    msm_dir  = msm_tar_gz.split('.tar.gz')[0]

    if not os.path.exists(msm_dir):
        liba.extract_file(msm_tar_gz, args.all_msms_dir)

    msm_id = int(msm_dir.split('/')[-1].split('msm_')[1])

    is_valid  = liba.get_valid(msm_dir)
    last_step = liba.get_step(msm_dir)

    if is_valid and last_step == MAX_STEP:
        metadata = liba.import_data('{}/metadata.json'.format(msm_dir))

        start = metadata['step_times'][str(MAX_STEP)]['start_time']
        stop = metadata['step_times'][str(MAX_STEP)]['end_time']
        results[msm_id] = (start, stop)

    # when done, delete the created msm dir
    liba.delete_directory(msm_dir)

for msm_id in sorted(results.keys()):
    print('MSM = {}'.format(msm_id))
    print('Step 10 Hours = {}'.format((results[msm_id][1]-results[msm_id][0])/3600.0))

# dump results to file
liba.export_data(args.out_file, results)
