#!/usr/bin/env python3

import sys
sys.path.append('..')
import geolocation.util
import glob
import os
import argparse
import numpy
import lib_analysis as liba
import pprint
pp = pprint.PrettyPrinter(indent=2)

total_rae2rae_pairs = 0
total_rae2rae_with_cor = 0
total_rae2rae_with_plr = 0
total_rae2rae_with_eye = 0
total_rae2rae_with_other = 0
total_rae2rae_with_all = 0

total_pings = 0
total_pairs = 0
total_pairs_without_responses = 0
total_pairs_with_less_than_three_responses = 0
total_pairs_with_more_than_three_responses = 0
total_removed_rae2rae_with_rae2rar_eye = 0


total = {}
total_rae2rae = {}
total_rae2cor = {}
total_rae2plr = {}
total_rae2rar_eye = {}
total_rae2rar_other = {}

MIN_STEP  = 1
MAX_STEP  = 10

type2total = {
    'rae2cor'       : total_rae2cor,
    'rae2plr'       : total_rae2plr,
    'rae2rar_eye'   : total_rae2rar_eye,
    'rae2rar_other' : total_rae2rar_other
}


def delete_rae2rar_eye(msm_dir):
    global total_removed_rae2rae_with_rae2rar_eye
    
    to_delete = {}
    for dest,src in total:
        for relay in total[(dest,src)]['rae2rar_eye']:
            if relay==dest or relay==src:
                to_delete[(dest,src)] = relay
    
    for (pair,relay) in to_delete.items():
        del total[pair]['rae2rar_eye'][relay]
            
    total_removed_rae2rae_with_rae2rar_eye+=len(to_delete)
    
        
def get_median(params, data):
    global total_pairs_without_responses, total_pairs_with_less_than_three_responses, total_pairs_with_more_than_three_responses, total_pairs, total_pings

    avges = [packet['avg'] for packet in data if packet['avg']>0]

    total_pings+=len([packet['avg'] for packet in data])
    
    total_pairs+=1
    if not len(avges):
        total_pairs_without_responses+=1
        return False
    elif len(avges)>0 and len(avges)<3: 
        total_pairs_with_less_than_three_responses+=1
        return False
    elif len(avges):
        total_pairs_with_more_than_three_responses+=1
        return numpy.median(avges)


def parse_measurement(results, issue, type):
    global total_rae2rae
    
    for result in results:
        if result['msm_id'] in issue:
            for source in result['msm_results']:
                median = get_median([(result['dst'],source),type],result['msm_results'][source])
                if median:
                    if type=='rae2rae':
                        if ((result['dst'],source) not in total_rae2rae):
                            total_rae2rae[(result['dst'],source)] = median
                    else:
                        type2total[type].setdefault(source,{})
                        type2total[type][source][result['dst']] = median
    
    
def get_valid_latencies(type, dest, src):

    common_relays = type2total[type][dest].keys() & type2total[type][src].keys()
    
    return {relay:{'median':type2total[type][dest][relay]+type2total[type][src][relay]} for relay in common_relays}
    

def map_pairs(msm_dir):
    global total
    
    for dest,src in total_rae2rae:
        for type in type2total:
            if dest in type2total[type] and src in type2total[type]:
                total.setdefault((dest,src),{})
                total[(dest,src)][type] = get_valid_latencies(type,dest,src)
        if (dest,src) in total:
            total[(dest,src)]['rae2rae'] = {dest:{'median':total_rae2rae[(dest,src)]},src:{'median':total_rae2rae[(dest,src)]}}
                

def fix_probe_ids(msm_dir):

    to_fix = ['rae2rar_eye','rae2rar_other']
    mapIP2ID = {}
    
    mapIP2ID['rae2rar_eye'] = liba.get_prb_ip_to_id(msm_dir+'/sampled_rar_eye.json')
    mapIP2ID['rae2rar_other'] = liba.get_prb_ip_to_id(msm_dir+'/sampled_rar_other.json')
    
    for type in to_fix:
        for source in type2total[type]:
            type2total[type][source] = {mapIP2ID[type][key]:value for (key,value) in type2total[type][source].items()}
    

def empty_dict():
    global total,total_rae2rae,total_rae2cor,total_rae2plr,total_rae2rar_eye,total_rae2rar_other
    total.clear()
    total_rae2rae.clear()
    total_rae2cor.clear()
    total_rae2plr.clear()
    total_rae2rar_eye.clear()
    total_rae2rar_other.clear()
    
    
def issue_msm_ids(data):
    return [msm[0] for msm in data]       
            
            

def start_parsing(msm_dir):
    global total_rae2rae_pairs,total_rae2rae_with_cor,total_rae2rae_with_plr,total_rae2rae_with_eye,total_rae2rae_with_other,total_rae2rae_with_all
    
    metadata = liba.import_data('{}/metadata.json'.format(msm_dir))

    results_rae2rae = liba.import_data(msm_dir+'/rae2rae_results/results.json')
    results_rae2cor = liba.import_data(msm_dir+'/rae2cor_results/results.json')
    results_rae2plr = liba.import_data(msm_dir+'/rae2plr_results/results.json')
    results_rae2rar_eye = liba.import_data(msm_dir+'/rae2rar_eye_results/results.json')
    results_rae2rar_other = liba.import_data(msm_dir+'/rae2rar_other_results/results.json')
    
    issue_rae2rae = liba.import_data(msm_dir+'/rae2rae_results/issue.json')
    issue_rae2cor = liba.import_data(msm_dir+'/rae2cor_results/issue.json')
    issue_rae2plr = liba.import_data(msm_dir+'/rae2plr_results/issue.json')
    issue_rae2rar_eye = liba.import_data(msm_dir+'/rae2rar_eye_results/issue.json')
    issue_rae2rar_other = liba.import_data(msm_dir+'/rae2rar_other_results/issue.json')
    
    parse_measurement(results_rae2rae,issue_msm_ids(issue_rae2rae),'rae2rae')
    parse_measurement(results_rae2cor,issue_msm_ids(issue_rae2cor),'rae2cor')
    parse_measurement(results_rae2plr,issue_msm_ids(issue_rae2plr),'rae2plr')
    parse_measurement(results_rae2rar_eye,issue_msm_ids(issue_rae2rar_eye),'rae2rar_eye')
    parse_measurement(results_rae2rar_other,issue_msm_ids(issue_rae2rar_other),'rae2rar_other')
    
    fix_probe_ids(msm_dir)    
    map_pairs(msm_dir)
    delete_rae2rar_eye(msm_dir)
    
    total_rae2rae_pairs+=len(total)
    for pair in total:
        if 'rae2cor' in total[pair]: total_rae2rae_with_cor+=1
        if 'rae2plr' in total[pair]: total_rae2rae_with_plr+=1
        if 'rae2rar_eye' in total[pair]: total_rae2rae_with_eye+=1
        if 'rae2rar_other' in total[pair]: total_rae2rae_with_other+=1
        if 'rae2cor' in total[pair] and 'rae2plr' in total[pair] and 'rae2rar_eye' in total[pair] and 'rae2rar_other' in total[pair]: total_rae2rae_with_all+=1
    
    empty_dict()


def main():
    parser = argparse.ArgumentParser(description="retrieve valid ping RTT medians from all msms")
    parser.add_argument('-m', '--all_msms_dir', dest='all_msms_dir', type=str, help='directory with all msms', required=True)
    args = parser.parse_args()

    for msm_tar_gz in glob.iglob('{}/msm_*.tar.gz'.format(args.all_msms_dir)):
        msm_dir  = msm_tar_gz.split('.tar.gz')[0]
        if not os.path.exists(msm_dir):
            liba.extract_file(msm_tar_gz, args.all_msms_dir)

        msm_id    = int(msm_dir.split('/')[-1].split('msm_')[1])
        is_valid  = liba.get_valid(msm_dir)
        last_step = liba.get_step(msm_dir)

        print(msm_dir,'- Is valid:',is_valid,'- Last step:',last_step)

        if is_valid and last_step == MAX_STEP:
            start_parsing(msm_dir)

        # when done, delete the created msm dir to save disk space
        liba.delete_directory(msm_dir)
            
    print('Removed rae2rae pairs with common rae2rar_eye relays:', total_removed_rae2rae_with_rae2rar_eye)
    print('Pairs without responses:',total_pairs_without_responses,'- {:.2f}%'.format((total_pairs_without_responses/total_pairs)*100))
    print('Pairs with less than three responses:',total_pairs_with_less_than_three_responses,'- {:.2f}%'.format((total_pairs_with_less_than_three_responses/total_pairs)*100))
    print('Pairs with more than three responses:',total_pairs_with_more_than_three_responses,'- {:.2f}%'.format((total_pairs_with_more_than_three_responses/total_pairs)*100))
    print('Rae2rae pairs:',total_rae2rae_pairs)
    print('Rae2rae pairs with cor:',total_rae2rae_with_cor)
    print('Rae2rae pairs with plr:',total_rae2rae_with_plr)
    print('Rae2rae pairs with eye:',total_rae2rae_with_eye)
    print('Rae2rae pairs with other:',total_rae2rae_with_other)
    print('Rae2rae pairs with all:',total_rae2rae_with_all)
    print('Total pings:',total_pings)
    
if __name__ == "__main__":
    main()
    
    
