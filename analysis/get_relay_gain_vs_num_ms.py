#!/usr/bin/env python3

import sys
import glob
import argparse
import ujson as json
import numpy as np
import re
from pprint import pprint as pp

MIN_STEP = 1
MAX_STEP = 10
REL_TYPES = [
    'rae2cor',
    'rae2rar_eye',
    'rae2rar_other',
    'rae2plr'
]

VALID_CRITERIA = ['cc', 'cont', 'asn']

parser = argparse.ArgumentParser(description="calculate top relay gains (in ms) vs number")
parser.add_argument('-i', '--input', dest='input_dir', type=str, help='directory with all valid_ping_median jsons', required=True)
parser.add_argument('-o', '--output', dest='output_file', help='file with dict with list of relay gains', required=True)
parser.add_argument('-p', '--pdb_facs', type=str, help='file with all pdb facilities', default=None)
args = parser.parse_args()

#  load pdb facilities, if feasible
pdb_facs = {}
if args.pdb_facs != None:
    pdb_fac_list = []
    with open(args.pdb_facs, 'r') as f:
        pdb_fac_list = json.load(f)

    for fac in pdb_fac_list:
        pdb_facs[fac['id']] = fac

# initialize results
results = {}
# auxiliary variable for best improvements
best_improvements = {}
for type in REL_TYPES:
    results[type] = {}
    best_improvements[type] = {}

all_instances = 0

# gather results
for msm_res_json in glob.iglob('{}/msm_*_valid_ping_medians.json'.format(args.input_dir)):

    with open(msm_res_json, 'r') as f:
        this_res = json.load(f)

    for pair in this_res:
        for type in REL_TYPES:
            if len(this_res[pair][type].values()) > 0:
                rae_src = eval(pair)[1]
                rae_dst = eval(pair)[0]

                rae_src_cc = None
                rae_dst_cc = None

                if 'cc' in this_res[pair]['rae2rae'][rae_src] and 'cc' in this_res[pair]['rae2rae'][rae_dst]:
                    rae_src_cc = this_res[pair]['rae2rae'][rae_src]['cc']
                    rae_dst_cc = this_res[pair]['rae2rae'][rae_dst]['cc']

                rae2rae_median = this_res[pair]['rae2rae'][rae_src]['median']

                for rel in this_res[pair][type].keys():
                    if this_res[pair][type][rel]['median'] < rae2rae_median:

                        if rel not in results[type]:
                            results[type][rel] = {
                                'improvements' : {},
                                'instances_used' : set()
                            }

                        if type == 'rae2cor':
                            results[type][rel]['fac'] = this_res[pair][type][rel]['fac']

                        if 'cc' in this_res[pair][type][rel]:
                            results[type][rel]['cc'] = this_res[pair][type][rel]['cc']

                        if 'cont' in this_res[pair][type][rel]:
                            results[type][rel]['cont'] = this_res[pair][type][rel]['cont']

                        if 'asn' in this_res[pair][type][rel]:
                            results[type][rel]['asn'] = this_res[pair][type][rel]['asn']

                        results[type][rel]['instances_used'].add(all_instances)
                        results[type][rel]['improvements'][all_instances] = rae2rae_median - this_res[pair][type][rel]['median']

                        if all_instances not in best_improvements[type]:
                            best_improvements[type][all_instances] = -1
                        if results[type][rel]['improvements'][all_instances] > best_improvements[type][all_instances]:
                            best_improvements[type][all_instances] = results[type][rel]['improvements'][all_instances]

        all_instances += 1

# calculate top relays per type
top_relays = {}
for type in REL_TYPES:
    top_relays[type] = sorted([k for k in results[type].keys()], key = lambda x : len(results[type][x]['instances_used']), reverse=True)

cum_res = {}
cum_res['all_instances'] = all_instances
print("ALL INSTANCES = {}".format(all_instances))
for type in REL_TYPES:
    cum_res[type] = {
        'instances' : [],
        'improvements' : [],
        'best_improvements' : list(best_improvements[type].values())
    }

    print("---TYPE {}---".format(type))
    print("LEN={}".format(len(top_relays[type])))
    cum_instances = set()
    cum_improvements = {}
    for rel in top_relays[type][:20]:
        cum_instances = cum_instances.union(results[type][rel]['instances_used'])
        for instance in cum_instances:
            if instance not in cum_improvements:
                cum_improvements[instance] = -1
            if instance in results[type][rel]['improvements']:
                if results[type][rel]['improvements'][instance] > cum_improvements[instance]:
                    cum_improvements[instance] = results[type][rel]['improvements'][instance]

        cum_perc = 100.0*len(cum_instances)/(1.0*all_instances)
        cum_res[type]['instances'].append(cum_perc)
        cum_res[type]['improvements'].append(list(cum_improvements.values()))

with open(args.output_file, 'w') as f:
    json.dump(cum_res, f)
