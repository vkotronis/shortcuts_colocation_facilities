#!/usr/bin/env python3

import matplotlib.pyplot as plt
import glob
import argparse
import ujson as json
import numpy as np
import statsmodels.api as sm # recommended import according to the docs
#http://stackoverflow.com/questions/23343484/python-3-statsmodels
import matplotlib
matplotlib.rcParams['text.usetex'] = True
from pprint import pprint as pp

MIN_STEP = 1
MAX_STEP = 10
TYPES    = [
    'rae2cor',
    'rae2rar_eye',
    'rae2rar_other',
    'rae2plr'
]

# http://stackoverflow.com/questions/7799156/can-i-cycle-through-line-styles-in-matplotlib
colors      = ('r', 'g', 'b', 'k')
linestyles  = ('-','--','-.',':')
styles      = ['{}{}'.format(color,linestyles[i]) for i,color in enumerate(colors)]
style_index = 0
fontsize    = 20

parser = argparse.ArgumentParser(description="calculate misc stats")
parser.add_argument('-i', '--input', dest='input_dir', type=str, help='directory with all valid_ping_median jsons', required=True)
args = parser.parse_args()

# initialize results
results = {}
good_cases = {}
relayed_cases = 0
direct_paths_over_thres = 0
direct_paths_over_thres_cured = 0
total_good_cases = set()
good_cases_below_200 = {}
good_cases_above_200 = {}
total_good_cases_below_200 = set()
total_good_cases_above_200 = set()
total_cases = 0
same_cc_cases = 0
diff_cc_cases = 0
max_improvement_perc = {}
max_improvement_abs = {}
all_improvements_perc = {}
all_improvements_abs = {}
good_improvements_abs = {}
for type in TYPES:
    results[type] = []
    good_cases[type] = 0
    good_cases_below_200[type] = 0
    good_cases_above_200[type] = 0
    max_improvement_perc[type] = 0
    max_improvement_abs[type] = 0
    all_improvements_perc[type] = []
    all_improvements_abs[type] = []
    good_improvements_abs[type] = []

# gather results
for msm_res_json in glob.iglob('{}/msm_*_valid_ping_medians.json'.format(args.input_dir)):
    with open(msm_res_json, 'r') as f:
        this_res = json.load(f)

    for pair in this_res:
        rae_src = eval(pair)[1]
        rae_dst = eval(pair)[0]
        rae2rae_median  = this_res[pair]['rae2rae'][rae_src]['median']

        if 'cont' in this_res[pair]['rae2rae'][rae_src] and 'cont' in this_res[pair]['rae2rae'][rae_dst]:
            rae_src_cont = this_res[pair]['rae2rae'][rae_src]['cont']
            rae_dst_cont = this_res[pair]['rae2rae'][rae_dst]['cont']
            if rae_src_cont != rae_dst_cont:
                diff_cc_cases += 1
            else:
                same_cc_cases += 1

        if rae2rae_median > 320:
            direct_paths_over_thres  += 1

        for type in TYPES:
            if len(this_res[pair][type].values()) > 0:
                rae_node        = list(this_res[pair]['rae2rae'].keys())[0]
                rae2rae_median  = this_res[pair]['rae2rae'][rae_node]['median']
                good_relays = 0
                good_relays_below_200 = 0
                good_relays_above_200 = 0

                rel_min = min([v['median'] for v in this_res[pair][type].values()])
                diff      = rae2rae_median - rel_min
                perc_diff = 100.0*(rae2rae_median - rel_min)/(1.0*rae2rae_median)
                all_improvements_abs[type].append(diff)
                all_improvements_perc[type].append(perc_diff)
                if diff > 0:
                    good_improvements_abs[type].append(diff)

                if rae2rae_median > 320 and rel_min < 320:
                    direct_paths_over_thres_cured  += 1

                for k in this_res[pair][type].keys():
                    relayed_cases += 1

                for v in this_res[pair][type].values():
                    if v['median'] < rae2rae_median:
                        cur_improvement_perc = 100.0*((rae2rae_median - v['median'])/(rae2rae_median*1.0))
                        cur_improvement_abs  = rae2rae_median - v['median']
                        if cur_improvement_perc > max_improvement_perc[type]:
                            max_improvement_perc[type] = cur_improvement_perc
                        if cur_improvement_abs > max_improvement_abs[type]:
                            max_improvement_abs[type] = cur_improvement_abs
                        #all_improvements_perc[type].append(cur_improvement_perc)
                        #all_improvements_abs[type].append(cur_improvement_abs)
                        good_relays += 1

                        if cur_improvement_abs <= 100:
                            good_relays_below_200 += 1
                        else:
                            good_relays_above_200 += 1

                if good_relays > 0:
                    results[type].append(good_relays)
                    good_cases[type] += 1
                    total_good_cases.add(total_cases)

                if good_relays_below_200 > 0:
                    good_cases_below_200[type] += 1
                    total_good_cases_below_200.add(total_cases)

                if good_relays_above_200 > 0:
                    good_cases_above_200[type] += 1
                    total_good_cases_above_200.add(total_cases)

        total_cases += 1

print('---OVERALL---')
print('---BEFORE RELAYING---')
print('PAIRS WITH SAME CC = {} ({}%), DIFF CC = {} ({}%), PAIRS OVER THRES = {}%, ALL = {}'.format(
                                                                           same_cc_cases,
                                                                           np.round(100.0*same_cc_cases/total_cases),
                                                                           diff_cc_cases,
                                                                           np.round(100.0*diff_cc_cases/total_cases),
                                                                           np.round(100.0*direct_paths_over_thres/total_cases),
                                                                           total_cases))

print('---AFTER RELAYING OVER COLOS---')
print('PAIRS OVER THRES = {}%'.format(np.round(100.0*(direct_paths_over_thres-direct_paths_over_thres_cured)/total_cases)))


print("DIRECT PAIRS (PATHS) = {}, RELAYED PAIRS (PATHS) = {}".format(total_cases, relayed_cases))

print('GOOD CASES = {}, GOOD_CASES UP TO 200 ms = {}, GOOD CASES OVER 200 ms = {}, TOTAL CASES = {} ({}%, {}%, {}%)'.format(
                                                                                        len(total_good_cases),
                                                                                        len(total_good_cases_below_200),
                                                                                        len(total_good_cases_above_200),
                                                                                        total_cases,
                                                                                        np.round(100.0*len(total_good_cases)/(1.0*total_cases)),
                                                                                        np.round(100.0*len(total_good_cases_below_200)/(1.0*total_cases)),
                                                                                        np.round(100.0*len(total_good_cases_above_200)/(1.0*total_cases))))

for type in TYPES:
    avg = np.round(np.average(results[type]))
    std = np.round(np.std(results[type]))
    med = np.round(np.median(results[type]))
    print('---TYPE {}---'.format(type))
    print('GOOD CASES = {}, GOOD_CASES UP TO 200 ms = {}, GOOD CASES OVER 200 ms = {}, TOTAL CASES = {} ({}%, {}%, {}%)'.format(
                                                                                            good_cases[type],
                                                                                            good_cases_below_200[type],
                                                                                            good_cases_above_200[type],
                                                                                            total_cases,
                                                                                            np.round(100.0*good_cases[type]/(1.0*len(total_good_cases))),
                                                                                            np.round(100.0*good_cases_below_200[type]/(1.0*len(total_good_cases))),
                                                                                            np.round(100.0*good_cases_above_200[type]/(1.0*len(total_good_cases)))))
    print("GOOD RELAY NUMS: avg = {}, std = {}, med = {}".format(avg, std, med))
    print("BEST IMPROVEMENT (%) = {}".format(np.round(max_improvement_perc[type])))
    print("BEST IMPROVEMENT (ms) = {}".format(np.round(max_improvement_abs[type])))
    print("MEDIAN IMPROVEMENT (%) = {}".format(np.round(np.median(all_improvements_perc[type]))))
    print("MEDIAN (GOOD) IMPROVEMENT (ms) = {}".format(np.round(np.median(good_improvements_abs[type]))))
    print("MEDIAN IMPROVEMENT (ms) = {}".format(np.round(np.median(all_improvements_abs[type]))))
