#!/usr/bin/env python3

import sys
import glob
import argparse
import ujson as json
from pprint import pprint as pp

MIN_STEP = 1
MAX_STEP = 10
REL_TYPES = [
    'rae2cor',
    #'rae2rar_eye',
    #'rae2rar_other',
    #'rae2plr'
]

VALID_CRITERIA = ['cc', 'cont', 'asn']

parser = argparse.ArgumentParser(description="calculate top relays over all msm data")
parser.add_argument('-i', '--input', dest='input_dir', type=str, help='directory with all valid_ping_median jsons', required=True)
parser.add_argument('-n', '--num', dest='N', type=int, help='number of top relays to show')
parser.add_argument('-p', '--pdb_facs', type=str, help='file with all pdb facilities', default=None)
args = parser.parse_args()

#  load pdb facilities, if feasible
pdb_facs = {}
if args.pdb_facs != None:
    pdb_fac_list = []
    with open(args.pdb_facs, 'r') as f:
        pdb_fac_list = json.load(f)

    for fac in pdb_fac_list:
        pdb_facs[fac['id']] = fac

top_10_pdb_facs = sorted([k for k in pdb_facs.keys()], key = lambda x : pdb_facs[x]['net_count'], reverse=True)
top_10_pdb_facs = top_10_pdb_facs[0:10]

# initialize results
results = {}
for type in REL_TYPES:
    results[type] = {}

all_instances = 0
all_good_instances = 0
all_cc_pair_instances = set()

# gather results
for msm_res_json in glob.iglob('{}/msm_*_valid_ping_medians.json'.format(args.input_dir)):

    with open(msm_res_json, 'r') as f:
        this_res = json.load(f)

    for pair in this_res:
        for type in REL_TYPES:
            if len(this_res[pair][type].values()) > 0:
                rae_src = eval(pair)[1]
                rae_dst = eval(pair)[0]

                rae_src_cc = None
                rae_dst_cc = None

                if 'cc' in this_res[pair]['rae2rae'][rae_src] and 'cc' in this_res[pair]['rae2rae'][rae_dst]:
                    rae_src_cc = this_res[pair]['rae2rae'][rae_src]['cc']
                    rae_dst_cc = this_res[pair]['rae2rae'][rae_dst]['cc']
                    all_cc_pair_instances.add((rae_src_cc, rae_dst_cc))

                rae2rae_median = this_res[pair]['rae2rae'][rae_src]['median']

                #min_relay  = None
                #min_median = 1000000
                #for rel in this_res[pair][type].keys():
                #    if this_res[pair][type][rel]['median'] < rae2rae_median and this_res[pair][type][rel]['median'] < min_median:
                #        min_median = this_res[pair][type][rel]['median']
                #        min_relay  = rel

                #if min_relay is not None:
                #    if min_relay not in results[type]:
                #        results[type][min_relay] = {
                #            'best_improvement'  : -100000,
                #            'instances_used'    : 0
                #        }

                #        if type == 'rae2cor':
                #            results[type][min_relay]['fac'] = this_res[pair][type][min_relay]['fac']

                #        if 'cc' in this_res[pair][type][min_relay]:
                #            results[type][min_relay]['cc'] = this_res[pair][type][min_relay]['cc']

                #        if 'cont' in this_res[pair][type][min_relay]:
                #            results[type][min_relay]['cont'] = this_res[pair][type][min_relay]['cont']

                #        if 'asn' in this_res[pair][type][min_relay]:
                #            results[type][min_relay]['asn'] = this_res[pair][type][min_relay]['asn']

                #    results[type][min_relay]['instances_used'] += 1
                #    cur_improvement = ((rae2rae_median - min_median)/(1.0*rae2rae_median)) * 100.0
                #    if cur_improvement > results[type][min_relay]['best_improvement']:
                #        results[type][min_relay]['best_improvement'] = cur_improvement

                count_good = True

                for rel in this_res[pair][type].keys():
                    if this_res[pair][type][rel]['median'] < rae2rae_median:

                        if count_good:
                            all_good_instances += 1
                            count_good = False

                        if rel not in results[type]:
                            results[type][rel] = {
                                'best_improvement'  : -100000,
                                'instances_used'    : set()
                            }

                        if type == 'rae2cor':
                            results[type][rel]['fac'] = this_res[pair][type][rel]['fac']

                        if 'cc' in this_res[pair][type][rel]:
                            results[type][rel]['cc'] = this_res[pair][type][rel]['cc']

                        if 'cont' in this_res[pair][type][rel]:
                            results[type][rel]['cont'] = this_res[pair][type][rel]['cont']

                        if 'asn' in this_res[pair][type][rel]:
                            results[type][rel]['asn'] = this_res[pair][type][rel]['asn']

                        if 'cc_pairs' not in results[type][rel]:
                            results[type][rel]['cc_pairs'] = {}

                        if 'ccs' not in results[type][rel]:
                            results[type][rel]['ccs'] = {}

                        results[type][rel]['instances_used'].add(all_instances)
                        #cur_improvement = ((rae2rae_median - this_res[pair][type][rel]['median'])/(1.0*rae2rae_median)) * 100.0
                        cur_improvement = rae2rae_median - this_res[pair][type][rel]['median']
                        if cur_improvement > results[type][rel]['best_improvement']:
                            results[type][rel]['best_improvement'] = cur_improvement

                        if type == 'rae2cor':
                            fac_id = int(this_res[pair][type][rel]['fac'])

                            if 'fac' not in results:
                                results['fac'] = {}

                            if fac_id not in results['fac']:
                                results['fac'][fac_id] = {}

                            if 'ccs' not in results['fac'][fac_id]:
                                results['fac'][fac_id]['ccs'] = {}

                            if rae_src_cc is not None:
                                if rae_src_cc not in results['fac'][fac_id]['ccs']:
                                    results['fac'][fac_id]['ccs'][rae_src_cc] = set()

                                results['fac'][fac_id]['ccs'][rae_src_cc].add(all_instances)

                            if rae_dst_cc is not None:
                                if rae_dst_cc not in results['fac'][fac_id]['ccs']:
                                    results['fac'][fac_id]['ccs'][rae_dst_cc] = set()

                                results['fac'][fac_id]['ccs'][rae_dst_cc].add(all_instances)

                            if 'instances_used' not in results['fac'][fac_id]:
                                results['fac'][fac_id]['instances_used'] = set()

                            results['fac'][fac_id]['instances_used'].add(all_instances)

                        if rae_src_cc is not None and rae_dst_cc is not None:
                            if (rae_src_cc, rae_dst_cc) not in results[type][rel]['cc_pairs']:
                                results[type][rel]['cc_pairs'][(rae_src_cc, rae_dst_cc)] = (cur_improvement, rae2rae_median)

                            if cur_improvement > results[type][rel]['cc_pairs'][(rae_src_cc, rae_dst_cc)][0]:
                                results[type][rel]['cc_pairs'][(rae_src_cc, rae_dst_cc)] = (cur_improvement, rae2rae_median)

        all_instances += 1

# calculate top 10 relays per type
top_N_relays = {}
for type in REL_TYPES:
    top_N_relays[type] = sorted([k for k in results[type].keys()], key = lambda x : len(results[type][x]['instances_used']), reverse=True)
    top_N_relays[type] = top_N_relays[type][0:min(len(top_N_relays[type]), args.N)]

#print("ALL CC INSTANCES = {}".format(len(all_cc_pair_instances)))

best_facs = []
best_fac_stats = {}

for type in REL_TYPES:
    print('----TYPE {}----'.format(type))
    for i,rel in enumerate(top_N_relays[type]):
        print('top-{} RELAY = {}:'.format(i+1, rel))

        print('USES = {} ({} % of all cases, {} % of all good cases)'.format(
            len(results[type][rel]['instances_used']),
            100.0*len(results[type][rel]['instances_used'])/all_instances,
            100.0*len(results[type][rel]['instances_used'])/all_good_instances))

        if 'cc' in results[type][rel]:
            print('REL COUNTRY = {}'.format(results[type][rel]['cc']))

        #print('TOP COUNTRY PAIRS')
        #top_cc_pairs = sorted(results[type][rel]['cc_pairs'].keys(), key=lambda k: results[type][rel]['cc_pairs'][k][0], reverse=True)
        #for cc_pair in top_cc_pairs[:3]:
        #    print('{} ({} ms))'.format(cc_pair, results[type][rel]['cc_pairs'][cc_pair][0]))

        #print('TOP COUNTRIES')
        #top_ccs = sorted(results[type][rel]['ccs'].keys(), key=lambda k: len(results[type][rel]['ccs'][k]), reverse=True)
        #for cc in top_ccs[:5]:
        #    print('{} ({}))'.format(cc, len(results[type][rel]['ccs'][cc])))

        if 'fac' in results[type][rel]:
            fac_id = int(results[type][rel]['fac'])
            if  fac_id not in best_facs:
                best_facs.append(fac_id)
                best_fac_stats[fac_id] = {
                    'rel_num' : 0,
                    'rel_list' : [],
                    'top_ccs' : {}
                }

            best_fac_stats[fac_id]['rel_num'] += 1
            best_fac_stats[fac_id]['rel_list'].append(rel)
            best_fac_stats[fac_id]['name'] = pdb_facs[fac_id]['name']
            best_fac_stats[fac_id]['fac_net_count'] = pdb_facs[fac_id]['net_count']
            best_fac_stats[fac_id]['city'] =  pdb_facs[fac_id]['city']
            best_fac_stats[fac_id]['country'] =  pdb_facs[fac_id]['country']

            print('REL FAC = {} ({})'.format(pdb_facs[fac_id]['name'], int(results[type][rel]['fac'])))

print('---TOP FACS---')
for i,fac_id in enumerate(sorted(best_facs, key=lambda x: len(results['fac'][x]['instances_used']), reverse=True)):
    print('---TOP-{}---'.format(i+1))
    print('ID = {}, NAME = {}, CITY = {}, COUNTRY = {}'.format(fac_id,
                                                      best_fac_stats[fac_id]['name'],
                                                      best_fac_stats[fac_id]['city'],
                                                      best_fac_stats[fac_id]['country']))
    print('NET COUNT = {}'.format(best_fac_stats[fac_id]['fac_net_count']))
    print('RELAY LIST = {}'.format(best_fac_stats[fac_id]['rel_list']))

    print('USES = {} ({} % of all cases, {} % of all good cases)'.format(
            len(results['fac'][fac_id]['instances_used']),
            100.0*len(results['fac'][fac_id]['instances_used'])/all_instances,
            100.0*len(results['fac'][fac_id]['instances_used'])/all_good_instances))

    print('TOP COUNTRIES')
    for cc in sorted(results['fac'][fac_id]['ccs'].keys(), key=lambda k : len(results['fac'][fac_id]['ccs'][k]), reverse=True)[:5]:
        print('{} ({}))'.format(cc, len(results['fac'][fac_id]['ccs'][cc])))

#    ccs_dict = {}
#    ccs_list = []
#    for rel in top_N_relays[type]:
#        if 'cc' in results[type][rel]:
#            cc = results[type][rel]['cc']
#            if cc not in ccs_dict:
#                ccs_dict[cc] = 0
#                ccs_list.append(cc)
#            ccs_dict[cc] += 1
#    print('---CCs---')
#    for cc in ccs_list:
#        print('{}({})'.format(cc, ccs_dict[cc]))

#    conts_dict = {}
#    conts_list = []
#    for rel in top_N_relays[type]:
#        if 'cont' in results[type][rel]:
#            cont = results[type][rel]['cont']
#            if cont not in conts_dict:
#                conts_dict[cont] = 0
#                conts_list.append(cont)
#            conts_dict[cont] += 1
#    print('---CONTs---')
#    for cont in conts_list:
#        print('{}({})'.format(cont, conts_dict[cont]))

#    asns_dict = {}
#    asns_list = []
#    for rel in top_N_relays[type]:
#        if 'asn' in results[type][rel]:
#            asn = results[type][rel]['asn']
#            if asn not in asns_dict:
#                asns_dict[asn] = 0
#                asns_list.append(asn)
#            asns_dict[asn] += 1
#    print('---ASNs---')
#    for asn in asns_list:
#        print('{}({})'.format(asn, asns_dict[asn]))

#    if type == 'rae2cor':
#        best_facs_list  = []
#        best_facs_dict = {}
#        net_counts = []
#        for rel in top_N_relays[type]:
#            fac = results[type][rel]['fac']
#            if fac not in best_facs_dict:
#                best_facs_dict[fac] = 0
#                best_facs_list.append(fac)
#                net_counts.append(pdb_facs[int(fac)]['net_count'])
#            best_facs_dict[fac] += 1
#        print('---FACS---')
#        for fac in best_facs_list:
#            print('{}({})'.format(fac, best_facs_dict[fac]))
#        print('---FAC NET COUNTS {}---'.format(net_counts))
#        for fac in best_facs_list:
#            if fac in top_10_pdb_facs:
#                print('FAC {} is in the PDBs top-10!'.format(fac))
