#!/usr/bin/env python3

import os
import argparse
import glob
import lib_analysis as liba

MIN_STEP  = 1
MAX_STEP  = 10
RA_TYPES  = ['rae', 'rar_eye', 'rar_other']
RAR_TYPES = ['rar_eye', 'rar_other']

parser = argparse.ArgumentParser(description="retrieve results related to the node set sizes employed in the msms")
parser.add_argument('-m', '--all_msms_dir', dest='all_msms_dir', type=str, help='directory with all msms', required=True)
parser.add_argument('-o', '--out_file', dest='out_file', type=str, help='file where the results are stored in json format', required=True)
args = parser.parse_args()

results = {}

for msm_tar_gz in glob.iglob('{}/msm_*.tar.gz'.format(args.all_msms_dir)):
    msm_dir  = msm_tar_gz.split('.tar.gz')[0]

    if not os.path.exists(msm_dir):
        liba.extract_file(msm_tar_gz, args.all_msms_dir)

    msm_id    = int(msm_dir.split('/')[-1].split('msm_')[1])

    is_valid  = liba.get_valid(msm_dir)
    last_step = liba.get_step(msm_dir)

    if is_valid and last_step == MAX_STEP:

        # timing of msm
        start_timestamp = liba.get_all_start_time(msm_dir)
        end_timestamp   = liba.get_all_end_time(msm_dir)
        total_duration  = end_timestamp - start_timestamp

        # initialize results dict
        results[msm_id] = {
            'start_timestamp' : start_timestamp,
            'end_timestamp'   : end_timestamp,
            'total_duration'  : total_duration,
            'ripe_atlas' : {
                'eyeball_total' : 0,
                'other_total'   : 0,
                'total'         : 0,
                'sampled' : {
                    'rae'       : 0,
                    'rar_eye'   : 0,
                    'rar_other' : 0
                },
                'feasible' : {
                    'rar_eye'   : 0,
                    'rar_other' : 0
                },
            },
            'colos' : {
                'total'     : 0,
                'sampled'   : 0,
                'feasible'  : 0
            },
            'planetlab' : {
                'total'    : 0,
                'sampled'  : 0,
                'feasible' : 0
            }
        }

        # populate set sizes from initial sets (RA, COLO, PL)
        all_ra_info = liba.import_data('{}/initial_ra_files/ra_classifier_ret_dict.json'.format(msm_dir))
        results[msm_id]['ripe_atlas']['total']         = all_ra_info['total']
        results[msm_id]['ripe_atlas']['eyeball_total'] = all_ra_info['eyeball_total']
        results[msm_id]['ripe_atlas']['other_total']   = all_ra_info['unknown_total']

        init_colos = liba.import_data('{}/initial_colo_files/geolocated_pingable_checked_colo_ips.json'.format(msm_dir))
        results[msm_id]['colos']['total'] = len(init_colos)

        init_pl_nodes = liba.import_data('{}/initial_pl_files/geolocated_pingable_alive_planetlab_nodes.json'.format(msm_dir))
        results[msm_id]['planetlab']['total'] = len(init_pl_nodes)

        # populate set sizes from sampled sets (RAE, RAR_EYE, RAR_OTHER, COR, PLR)
        for ra_type in RA_TYPES:
            sampled_ra = liba.import_data('{}/sampled_{}.json'.format(msm_dir, ra_type))
            results[msm_id]['ripe_atlas']['sampled'][ra_type] = len(sampled_ra)

        sampled_cor = liba.import_data('{}/sampled_cor.json'.format(msm_dir))
        results[msm_id]['colos']['sampled'] = len(sampled_cor)

        sampled_plr = liba.import_data('{}/sampled_plr.json'.format(msm_dir))
        results[msm_id]['planetlab']['sampled'] = len(sampled_plr)

        # populate set sizes from feasible sets (RAR_EYE, RAR_OTHER, COR, PLR)
        for rar_type in RAR_TYPES:
            feasible_rar_dict = liba.import_data('{}/feasible_{}.json'.format(msm_dir, rar_type))
            feasible_rar = set()
            for v in feasible_rar_dict.values():
                feasible_rar.update(set(v))
            results[msm_id]['ripe_atlas']['feasible'][rar_type] = len(feasible_rar)

        feasible_cor_dict = liba.import_data('{}/feasible_cor.json'.format(msm_dir))
        feasible_cor = set()
        for v in feasible_cor_dict.values():
            feasible_cor.update(set(v))
        results[msm_id]['colos']['feasible'] = len(feasible_cor)

        feasible_plr_dict = liba.import_data('{}/feasible_plr.json'.format(msm_dir))
        feasible_plr = set()
        for v in feasible_plr_dict.values():
            feasible_plr.update(set(v))
        results[msm_id]['planetlab']['feasible'] = len(feasible_plr)

    # when done, delete the created msm dir to save disk space
    liba.delete_directory(msm_dir)

# dump the results to file
liba.export_data(args.out_file, results)
