#!/usr/bin/env python3

import sys
import os
import glob
import ujson as json
import argparse
import re
import lib_analysis as liba

affected_dirs = [
    'rae2rae_results',
    'rae2rar_eye_results',
    'rae2rar_other_results',
    'rae2cor_results',
    'rae2plr_results'
]

parser = argparse.ArgumentParser(description="clean msm results according to what is issued")
parser.add_argument('-m', '--all_msms_dir', dest='all_msms_dir', type=str, help='directory with all msms', required=True)
args = parser.parse_args()

for msm_dir in glob.iglob('{}/msm_*'.format(args.all_msms_dir)):
    match = re.match('.*/msm_(\d+)$', msm_dir)
    if match:
        msm_id = int(match.group(1))

        for affected_dir in affected_dirs:

            if os.path.exists('{}/{}/issue.json'.format(msm_dir, affected_dir)) \
                    and os.path.exists('{}/{}/results.json'.format(msm_dir, affected_dir)):

                issued_msm_infos = liba.import_data('{}/{}/issue.json'.format(msm_dir, affected_dir))
                issued_msm_ids = set()
                for info in issued_msm_infos:
                    issued_msm_ids.add(info[0])

                init_results  = liba.import_data('{}/{}/results.json'.format(msm_dir, affected_dir))
                final_results = []

                for res in init_results:
                    if res['msm_id'] not in issued_msm_ids:
                        pass
                    else:
                        final_results.append(res)

                liba.export_data('{}/{}/results.json'.format(msm_dir, affected_dir), final_results)



