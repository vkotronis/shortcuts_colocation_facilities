#!/usr/bin/env python3

import sys
import glob
import argparse
import ujson as json
import numpy as np
import re
from pprint import pprint as pp

MIN_STEP = 1
MAX_STEP = 10
REL_TYPES = [
    'rae2cor',
    'rae2rar_eye',
    'rae2rar_other',
    'rae2plr'
]

VALID_CRITERIA = ['cc', 'cont', 'asn']

parser = argparse.ArgumentParser(description="calculate top relay gain vs number")
parser.add_argument('-i', '--input', dest='input_dir', type=str, help='directory with all valid_ping_median jsons', required=True)
parser.add_argument('-o', '--output', dest='output_file', help='file with dict with list of relay gains', required=True)
parser.add_argument('-p', '--pdb_facs', type=str, help='file with all pdb facilities', default=None)
args = parser.parse_args()

#  load pdb facilities, if feasible
pdb_facs = {}
if args.pdb_facs != None:
    pdb_fac_list = []
    with open(args.pdb_facs, 'r') as f:
        pdb_fac_list = json.load(f)

    for fac in pdb_fac_list:
        pdb_facs[fac['id']] = fac

# initialize results
results = {}
for type in REL_TYPES:
    results[type] = {}

all_instances = 0

# gather results
for msm_res_json in glob.iglob('{}/msm_*_valid_ping_medians.json'.format(args.input_dir)):

    with open(msm_res_json, 'r') as f:
        this_res = json.load(f)

    for pair in this_res:
        for type in REL_TYPES:
            if len(this_res[pair][type].values()) > 0:
                rae_src = eval(pair)[1]
                rae_dst = eval(pair)[0]

                rae_src_cc = None
                rae_dst_cc = None

                if 'cc' in this_res[pair]['rae2rae'][rae_src] and 'cc' in this_res[pair]['rae2rae'][rae_dst]:
                    rae_src_cc = this_res[pair]['rae2rae'][rae_src]['cc']
                    rae_dst_cc = this_res[pair]['rae2rae'][rae_dst]['cc']

                rae2rae_median = this_res[pair]['rae2rae'][rae_src]['median']

                for rel in this_res[pair][type].keys():
                    if this_res[pair][type][rel]['median'] < rae2rae_median:

                        if rel not in results[type]:
                            results[type][rel] = {
                                'best_improvement'  : -100000,
                                'instances_used'    : set()
                            }

                        if type == 'rae2cor':
                            results[type][rel]['fac'] = this_res[pair][type][rel]['fac']

                        if 'cc' in this_res[pair][type][rel]:
                            results[type][rel]['cc'] = this_res[pair][type][rel]['cc']

                        if 'cont' in this_res[pair][type][rel]:
                            results[type][rel]['cont'] = this_res[pair][type][rel]['cont']

                        if 'asn' in this_res[pair][type][rel]:
                            results[type][rel]['asn'] = this_res[pair][type][rel]['asn']

                        results[type][rel]['instances_used'].add(all_instances)
                        cur_improvement = ((rae2rae_median - this_res[pair][type][rel]['median'])/(1.0*rae2rae_median)) * 100.0
                        if cur_improvement > results[type][rel]['best_improvement']:
                            results[type][rel]['best_improvement'] = cur_improvement

        all_instances += 1

# calculate top relays per type
top_relays = {}
for type in REL_TYPES:
    top_relays[type] = sorted([k for k in results[type].keys()], key = lambda x : len(results[type][x]['instances_used']), reverse=True)

cum_res = {}
print("ALL INSTANCES = {}".format(all_instances))
for type in REL_TYPES:
    cum_res[type] = []

    print("---TYPE {}---".format(type))
    print("LEN={}".format(len(top_relays[type])))
    cum_instances = set()
    for rel in top_relays[type]:
        cum_instances = cum_instances.union(results[type][rel]['instances_used'])
        cum_perc = 100.0*len(cum_instances)/(1.0*all_instances)
        cum_res[type].append(cum_perc)

with open(args.output_file, 'w') as f:
    json.dump(cum_res, f)
