#!/usr/bin/env python3

import matplotlib.pyplot as plt
import argparse
import ujson as json
import numpy as np

# http://stackoverflow.com/questions/7799156/can-i-cycle-through-line-styles-in-matplotlib
colors      = ('r', 'g', 'b', 'k')
linestyles  = ('-','--','-.',':')
styles      = ['{}{}'.format(color,linestyle) for color in colors for linestyle in linestyles]
style_index = 0
fontsize    = 12

parser = argparse.ArgumentParser(description="plot node set sizes in time")
parser.add_argument('-i', '--input', dest='input_file', type=str, help='json dict file with node sets', required=True)
parser.add_argument('-o', '--output', dest='output_figure_file', type=str, help='png/ps figure file for output', required=True)
args = parser.parse_args()

with open(args.input_file) as f:
    input = json.load(f)

x = sorted([int(i) for i in input.keys()])

y_tot_ra_other  = []
y_tot_ra_eye    = []
y_tot_ra        = []
y_sam_rae       = []
y_sam_rar_eye   = []
y_sam_rar_other = []
y_fea_rar_eye   = []
y_fea_rar_other = []
y_tot_cor       = []
y_sam_cor       = []
y_fea_cor       = []
y_tot_plr       = []
y_sam_plr       = []
y_fea_plr       = []

for msm_id in x:
    msm_id = str(msm_id)
    y_tot_ra_eye.append(input[msm_id]['ripe_atlas']['eyeball_total'])
    y_tot_ra_other.append(input[msm_id]['ripe_atlas']['other_total'])
    y_tot_ra.append(input[msm_id]['ripe_atlas']['total'])
    y_sam_rae.append(input[msm_id]['ripe_atlas']['sampled']['rae'])
    y_sam_rar_eye.append(input[msm_id]['ripe_atlas']['sampled']['rar_eye'])
    y_sam_rar_other.append(input[msm_id]['ripe_atlas']['sampled']['rar_other'])
    y_fea_rar_eye.append(input[msm_id]['ripe_atlas']['feasible']['rar_eye'])
    y_fea_rar_other.append(input[msm_id]['ripe_atlas']['feasible']['rar_other'])
    y_tot_cor.append(input[msm_id]['colos']['total'])
    y_sam_cor.append(input[msm_id]['colos']['sampled'])
    y_fea_cor.append(input[msm_id]['colos']['feasible'])
    y_tot_plr.append(input[msm_id]['planetlab']['total'])
    y_sam_plr.append(input[msm_id]['planetlab']['sampled'])
    y_fea_plr.append(input[msm_id]['planetlab']['feasible'])

print('---AVERAGES---')
print('SAMPLED RAE = {}'.format(np.round(np.average(y_sam_rae))))
print('SAMPLED RAR_EYE = {}'.format(np.round(np.average(y_sam_rar_eye))))
print('SAMPLED RAR_OTHER = {}'.format(np.round(np.average(y_sam_rar_other))))
print('SAMPLED PLR = {}'.format(np.round(np.average(y_sam_plr))))
print('SAMPLED COR = {}'.format(np.round(np.average(y_sam_cor))))

fig, ax = plt.subplots()

ax.plot(x, y_tot_ra, styles[style_index], label='Total RA')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_tot_ra_eye, styles[style_index], label='Total Eyeball RA')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_tot_ra_other, styles[style_index], label='Total Other RA')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_sam_rae, styles[style_index], label='Sampled RAE')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_sam_rar_eye, styles[style_index], label='Sampled RAR_EYE')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_sam_rar_other, styles[style_index], label='Sampled RAR_OTHER')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_fea_rar_eye, styles[style_index], label='Feasible RAR_EYE')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_fea_rar_other, styles[style_index], label='Feasible RAR_OTHER')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_tot_cor, styles[style_index], label='Total COR')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_sam_cor, styles[style_index], label='Sampled COR')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_fea_cor, styles[style_index], label='Feasible COR')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_tot_plr, styles[style_index], label='Total PLR')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_sam_plr, styles[style_index], label='Sampled PLR')
style_index = (style_index + 1) % len(styles)

ax.plot(x, y_fea_plr, styles[style_index], label='Feasible PLR')
style_index = (style_index + 1) % len(styles)

legend = ax.legend(loc='lower right', shadow=True)
plt.xticks(x, x, size=fontsize)

plt.xlabel("Measurement round", size=fontsize)
plt.ylabel("Size of node set", size=fontsize)
plt.title("Size of node sets in time", size=fontsize)
plt.tight_layout()
plt.show()
#plt.savefig(args.output_figure_file)
#plt.close()

