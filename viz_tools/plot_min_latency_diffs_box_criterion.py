#!/usr/bin/env python3

import matplotlib.pyplot as plt
import sys
import glob
import argparse
import ujson as json
import numpy as np
#http://matplotlib.org/examples/pylab_examples/boxplot_demo2.html
import matplotlib
matplotlib.rcParams['text.usetex'] = True

MIN_STEP = 1
MAX_STEP = 10
REL_TYPES = [
    'rae2cor',
    'rae2rar_eye',
    'rae2rar_other',
    'rae2plr'
]

VALID_CRITERIA = ['cc', 'cont', 'asn']

# http://stackoverflow.com/questions/7799156/can-i-cycle-through-line-styles-in-matplotlib
colors      = ('r', 'g', 'b', 'k')
linestyles  = ('-','--','-.',':')
styles      = ['{}{}'.format(color,linestyles[i]) for i,color in enumerate(colors)]
style_index = 0
fontsize    = 20
box_width   = 0.15

parser = argparse.ArgumentParser(description="plot % latency diffs boxplot over selected criterion")
parser.add_argument('-i', '--input', dest='input_dir', type=str, help='directory with all valid_ping_median jsons', required=True)
parser.add_argument('-c', '--criterion', dest='criterion', type=str, help='criterion against which to plot', required=True)
parser.add_argument('-u', '--use_median', dest='use_median', type=bool, help='use median instead of min', default=False)
parser.add_argument('-o', '--output', dest='output_figure_file', type=str, help='png/ps figure file for output', required=True)
args = parser.parse_args()

if args.criterion not in VALID_CRITERIA:
    print("Valid Criteria: {}".format(str(VALID_CRITERIA)))
    sys.exit(1)

# initialize results
results = {}
for type in REL_TYPES:
    results[type] = {}
    results[type]['same-src-rel-{}'.format(args.criterion)] = []
    results[type]['same-dst-rel-{}'.format(args.criterion)] = []
    results[type]['diff-src-dst-rel-{}'.format(args.criterion)] = []
    results[type]['all-combinations'] = []

# this will be used as the x-axis
x = [
    'same-src-rel-{}'.format(args.criterion),
    'same-dst-rel-{}'.format(args.criterion),
    'diff-src-dst-rel-{}'.format(args.criterion),
    'all-combinations'
]

# gather results
for msm_res_json in glob.iglob('{}/msm_*_valid_ping_medians.json'.format(args.input_dir)):

    with open(msm_res_json, 'r') as f:
        this_res = json.load(f)

    for pair in this_res:
        for type in REL_TYPES:
            if len(this_res[pair][type].values()) > 0:
                rae_src = eval(pair)[1]
                rae_dst = eval(pair)[0]

                same_src_relays = [ k for k in this_res[pair][type].keys()
                                    if (args.criterion in this_res[pair][type][k] and
                                        args.criterion in this_res[pair]['rae2rae'][rae_src] and
                                        this_res[pair][type][k][args.criterion] == this_res[pair]['rae2rae'][rae_src][args.criterion]
                                        )]

                same_dst_relays = [ k for k in this_res[pair][type].keys()
                                    if (args.criterion in this_res[pair][type][k] and
                                        args.criterion in this_res[pair]['rae2rae'][rae_dst] and
                                        this_res[pair][type][k][args.criterion] == this_res[pair]['rae2rae'][rae_dst][args.criterion]
                                        )]

                diff_relays = [ k for k in this_res[pair][type].keys()
                                if (args.criterion in this_res[pair][type][k] and
                                    args.criterion in this_res[pair]['rae2rae'][rae_src] and
                                    args.criterion in this_res[pair]['rae2rae'][rae_dst] and
                                    this_res[pair][type][k][args.criterion] != this_res[pair]['rae2rae'][rae_src][args.criterion] and
                                    this_res[pair][type][k][args.criterion] != this_res[pair]['rae2rae'][rae_dst][args.criterion]
                                    )]

                all_relays = [k for k in this_res[pair][type].keys()]

                rae2rae_median      = this_res[pair]['rae2rae'][rae_src]['median']
                if len(same_src_relays) > 0:
                    if not args.use_median:
                        same_src_rel_median = min([this_res[pair][type][k]['median'] for k in same_src_relays])
                    else:
                        same_src_rel_median = np.median([this_res[pair][type][k]['median'] for k in same_src_relays])
                    same_src_rel_diff       = (rae2rae_median - same_src_rel_median)/(1.0*rae2rae_median)
                    same_src_rel_perc_diff  = same_src_rel_diff * 100.0
                    results[type]['same-src-rel-{}'.format(args.criterion)].append(same_src_rel_perc_diff)

                if len(same_dst_relays) > 0:
                    if not args.use_median:
                        same_dst_rel_median = min([this_res[pair][type][k]['median'] for k in same_dst_relays])
                    else:
                        same_dst_rel_median = np.median([this_res[pair][type][k]['median'] for k in same_dst_relays])
                    same_dst_rel_diff       = (rae2rae_median - same_dst_rel_median)/(1.0*rae2rae_median)
                    same_dst_rel_perc_diff  = same_dst_rel_diff * 100.0
                    results[type]['same-dst-rel-{}'.format(args.criterion)].append(same_dst_rel_perc_diff)

                if len(diff_relays) > 0:
                    if not args.use_median:
                        diff_rel_median = min([this_res[pair][type][k]['median'] for k in diff_relays])
                    else:
                        diff_rel_median = np.median([this_res[pair][type][k]['median'] for k in diff_relays])
                    diff_rel_diff       = (rae2rae_median - diff_rel_median)/(1.0*rae2rae_median)
                    diff_rel_perc_diff  = diff_rel_diff * 100.0
                    results[type]['diff-src-dst-rel-{}'.format(args.criterion)].append(diff_rel_perc_diff)

                if len(all_relays) > 0:
                    if not args.use_median:
                        all_rel_median = min([this_res[pair][type][k]['median'] for k in all_relays])
                    else:
                        all_rel_median = np.median([this_res[pair][type][k]['median'] for k in all_relays])
                    all_rel_diff        = (rae2rae_median - all_rel_median)/(1.0*rae2rae_median)
                    all_rel_perc_diff   = all_rel_diff * 100.0
                    results[type]['all-combinations'].append(all_rel_perc_diff)

fig, ax = plt.subplots()
for i,type in enumerate(REL_TYPES):
    pos = []
    for j,k in enumerate(x):
        if (i==0):
            pos.append(j-0.3)
        elif (i==1):
            pos.append(j-0.1)
        elif (i==2):
            pos.append(j+0.1)
        else:
            pos.append(j+0.3)

    list_of_vectors = []
    for k in x:
        list_of_vectors.append(results[type][k])
        print('---TYPE {}---'.format(type))
        print('---K = {}---'.format(k))
        print(len(results[type][k]))

    bp = ax.boxplot(list_of_vectors, positions=pos, widths=box_width, notch=True, showfliers=False, manage_xticks=False)
    plt.setp(bp['boxes'], color=colors[i], linestyle=linestyles[i])
    plt.setp(bp['caps'], color=colors[i], linestyle=linestyles[i])
    plt.setp(bp['whiskers'], color=colors[i], linestyle=linestyles[i])
    plt.setp(bp['medians'], color=colors[i], linestyle=linestyles[i])

# draw temporary lines and use them to create a legend
temp_h = []
for i,type in enumerate(REL_TYPES):
    h, = ax.plot([1,1], color=colors[i], linestyle=linestyles[i], label='{}'.format(type).replace('rae2', '').replace('_', '\_').upper())
    temp_h.append(h)

# set legend
legend = ax.legend(loc='lower right', fontsize=16, frameon=False)

# hide temporary lines
for h in temp_h:
    h.set_visible(False)

# set xlabel
#plt.xlabel('')

plt.yticks(fontsize=fontsize)

# set ylabel
plt.ylabel("\% RTT diff vs direct", fontsize=fontsize)

# set xticks
plt.subplots_adjust(bottom=0.1)
xticks = ['src=rel', 'dst=rel', 'src!=rel,\ndst!=rel', 'any']
plt.xticks(range(len(x)), xticks, size=fontsize)

# set title
#plt.title("Boxplots of total % overlay latency improvement", fontsize=fontsize)

# activate grid
ax.grid(True, which='y')
ax.yaxis.grid(linestyle=':')

# misc
plt.tight_layout()
#plt.show()
plt.savefig(args.output_figure_file)
plt.close()



