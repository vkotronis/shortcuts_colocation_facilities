#!/usr/bin/env python3

import ujson
import pprint
#import statsmodels.api as sm
from statsmodels.distributions.empirical_distribution import ECDF
import numpy as np
import matplotlib.pyplot as plt
import operator
pp = pprint.PrettyPrinter(indent=2)
plt.switch_backend('Qt4Agg')  

def import_data(filename):
    try:
        with open(filename, 'r') as file:
            return ujson.load(file)
    except:
        print("Error")


def plot_cdf_AsnCC_coverage(dataset):

    data={}
    for asn in dataset:
        for cc in dataset[asn]:
            data[(asn,cc['CC'])] = cc['% of country (Users)']
    
#    print(list(data.values()))
    x_con = list(data.values())
    
    ecdf_con    = ECDF(x_con)
    pp.pprint(ecdf_con)
    x_con_np    = np.linspace(min(x_con), max(x_con))
    y_con       = ecdf_con(x_con_np)
    #fig, ax = plt.subplots()
    plt.plot(x_con_np, y_con)
    plt.show()
    
    
def export(data,filename):
    file = open(filename,'w')
    
    data = {perc:len(cc) for (perc,cc) in data.items()}
    data = sorted(data.items(), key=operator.itemgetter(0))
    for pair in data:
        file.write(str(pair[0])+'\t'+str(pair[1])+'\n')
        
    file.close()


# http://www.caida.org/data/as-classification/
#http://matlab.cheme.cmu.edu/2011/09/26/some-basic-data-structures-in-matlab/
def plot_asn_cuttoff(dataset):
    total_tr  = {}
    total_con = {}
    total_ent = {}
    total = {}
    new_total = {}

    for asns in dataset.values():
        for asn in asns:
            export_asn_coverage(new_total,asn)
            for i in range(1,10001,1):
                i=i/100
                if asn['% of country (Users)']>=i:
                    total.setdefault(i,set())
                    total[i].add(asn['ASN'])
                    if asn['caida_class']=='transit/access':
                        total_tr.setdefault(i,set())
                        total_tr[i].add(asn['ASN'])
                    elif asn['caida_class']=='content':
                        total_con.setdefault(i,set())
                        total_con[i].add(asn['ASN'])
                    elif asn['caida_class']=='enterpise':
                        total_ent.setdefault(i,set())
                        total_ent[i].add(asn['ASN'])
                else:
                    break
    
#    export(total_tr,'asn_transit_vs_cutoffcoverage.txt')
#    export(total_con,'asn_content_vs_cutoffcoverage.txt')
#    export(total_ent,'asn_enterprise_vs_cutoffcoverage.txt')
#    export(total,'asn_vs_cutoffcoverage.txt')

    new_total = sorted(new_total.items(), key=operator.itemgetter(1))
    file = open('asn_coverage.txt','w')
    for pair in new_total:
        file.write(str(pair[0])+'\n')
    file.close()


def export_asn_coverage(new_total,asn):
    key = str(asn['% of country (Users)'])+' | '+asn['ASN'] +' | '+ asn['CC'] +' | '+ asn['AS Name']
    value = asn['% of country (Users)']
    new_total[key]=value


def plot_countries_cutoff(dataset):
    
    total = {}
    for country in dataset:
        for cc in dataset[country]:
            for i in range(1,10001,1):
                i=i/100
                if cc['% of country (Users)']>=i:
                    total.setdefault(i,set())
                    total[i].add(cc['CC'])
    export(total,'country_vs_cutoffcoverage.txt')


def main():

    apnic_file_asn = '../ripe_atlas/eyeball_classifier/data/apnic_final_dataset_ASN_enriched_31_3_2017.json'
    apnic_file_cc = '../ripe_atlas/eyeball_classifier/data/apnic_final_dataset_CC_31_3_2017.json'
    
    plot_cdf_AsnCC_coverage(import_data(apnic_file_asn))
#    plot_countries_cutoff(import_data(apnic_file_cc))
    plot_asn_cuttoff(import_data(apnic_file_asn))
    
if __name__ == "__main__":
    main()
