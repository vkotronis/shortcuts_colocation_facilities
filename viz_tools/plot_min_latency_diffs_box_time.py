#!/usr/bin/env python3

import matplotlib.pyplot as plt
import glob
import argparse
import ujson as json
import numpy as np
import re
import sys
from pprint import pprint as pp
import datetime as dt
#http://stackoverflow.com/questions/23343484/python-3-statsmodels
#http://matplotlib.org/examples/pylab_examples/boxplot_demo2.html
import matplotlib
matplotlib.rcParams['text.usetex'] = True

MIN_STEP = 1
MAX_STEP = 10
TYPES    = [
    'rae2cor',
    'rae2plr',
    'rae2rar_other',
    'rae2rar_eye'
]

colors      = ('k', 'g', 'b', 'r')
linestyles  = ('-','--','-.',':')
styles      = ['{}{}'.format(color,linestyles[i]) for i,color in enumerate(colors)]
style_index = 0
fontsize    = 20
#box_width   = 0.5
box_width   = 1.0


parser = argparse.ArgumentParser(description="plot % latency diffs boxplot over time")
parser.add_argument('-i', '--input', dest='input_dir', type=str, help='directory with all valid_ping_median jsons', required=True)
parser.add_argument('-t', '--time_file', dest='time_file', type=str, help='file with all msm temporal information', required=True)
parser.add_argument('-u', '--use_median', dest='use_median', type=bool, help='use median instead of min', default=False)
parser.add_argument('-n', '--n_rounds', dest='n_rounds', type=int, help='number of rounds to bucket in each box', default=6)
parser.add_argument('-o', '--output', dest='output_figure_file', type=str, help='png/ps figure file for output', required=True)
args = parser.parse_args()

# get times
with open(args.time_file, 'r') as f:
    msm_times = json.load(f)

# initialize results
results = {}
for type in TYPES:
    results[type] = {}

# initialize list of msm_ids
msm_ids = []

# gather results
for msm_res_json in glob.iglob('{}/msm_*_valid_ping_medians.json'.format(args.input_dir)):

    match = re.match('.*msm_(\d+)_valid_ping_medians.json', msm_res_json)
    msm_id = match.group(1)
    msm_ids.append(int(msm_id))

    for type in TYPES:
        results[type][msm_id] = []

    with open(msm_res_json, 'r') as f:
        this_res = json.load(f)

    for pair in this_res:
        for type in TYPES:
            if len(this_res[pair][type].values()) > 0:
                rae_node        = list(this_res[pair]['rae2rae'].keys())[0]
                rae2rae_median  = this_res[pair]['rae2rae'][rae_node]['median']
                #for rel in this_res[pair][type].keys():
                if not args.use_median:
                    rel_median = min([v['median'] for v in this_res[pair][type].values()])
                else:
                    rel_median = np.median([v['median'] for v in this_res[pair][type].values()])
                    #rel_median = this_res[pair][type][rel]['median']
                #diff            = (rae2rae_median - rel_median)/(1.0*rae2rae_median)
                #perc_diff       = diff * 100.0
                diff            = (rae2rae_median - rel_median)
                results[type][msm_id].append(diff)

fig, ax = plt.subplots()
plt.subplots_adjust(bottom=0.25)
plt.subplots_adjust(top=0.9)
all_msm_ids  = sorted(msm_ids)
every_n_msm_ids  = []
for i,j in enumerate(all_msm_ids):
    if i % args.n_rounds == 0:
        every_n_msm_ids.append(j)

# cumul_results every n rounds
cumul_results = {}
every_n_buckets = []
for type in TYPES:
    cumul_results[type] = {}

for i,j in enumerate(every_n_msm_ids):
    for type in TYPES:
        if i not in cumul_results[type]:
            cumul_results[type][i] = []
    k = j
    while k < every_n_msm_ids[min(i+1, len(every_n_msm_ids)-1)]:
        for type in TYPES:
            if str(k) in results[type]:
                cumul_results[type][i].extend(results[type][str(k)])
            else:
                print('K={} not in results for type {}, double-check!'.format(k, type))
        k += 1
    if i == len(every_n_msm_ids) - 1:
        while k <= all_msm_ids[-1]:
            for type in TYPES:
                if str(k) in results[type]:
                    cumul_results[type][i].extend(results[type][str(k)])
                else:
                    print('K={} not in results for type {}, double-check!'.format(k, type))
            k += 1

    bucket_start = j
    bucket_end = k-1
    if str(bucket_end) not in results['rae2cor']:
        bucket_end -= 1
    every_n_buckets.append((bucket_start, bucket_end))

print('{}-round buckets'.format(args.n_rounds))
pp(every_n_buckets)

for i,type in enumerate(TYPES):
    pos = []
    for j in every_n_msm_ids:
        if (i==0):
            #pos.append(j-0.9)
            pos.append(j-1.7)
        elif (i==1):
            #pos.append(j-0.3)
            pos.append(j-0.6)
        elif (i==2):
            #pos.append(j+0.3)
            pos.append(j+0.6)
        else:
            #pos.append(j+0.9)
            pos.append(j+1.7)

    list_of_vectors = []
    for j in sorted(cumul_results[type].keys()):
        list_of_vectors.append(cumul_results[type][j])
    bp = ax.boxplot(list_of_vectors, positions=pos, widths=box_width, notch=True, showfliers=False, manage_xticks=False)
    plt.setp(bp['boxes'], color=colors[i], linestyle=linestyles[i])
    plt.setp(bp['caps'], color=colors[i], linestyle=linestyles[i])
    plt.setp(bp['whiskers'], color=colors[i], linestyle=linestyles[i])
    plt.setp(bp['medians'], color=colors[i], linestyle=linestyles[i])

# draw temporary lines and use them to create a legend
temp_h = []
for i,type in enumerate(TYPES):
    h, = ax.plot([1,1], color=colors[i], linestyle=linestyles[i], label='{}'.format(type).replace('rae2', '').replace('_', '\_').upper())
    temp_h.append(h)

# set legend
#legend = ax.legend(loc='lower right', fontsize=18, frameon=False)
legend = ax.legend(bbox_to_anchor=(0., 1.02, 1., 1), loc=3, fontsize=16,
           ncol=4, mode="expand", borderaxespad=0., handlelength=1.0, handletextpad=0.5)

# hide temporary lines
for h in temp_h:
    h.set_visible(False)

# set xlabel
#plt.xlabel('Date of measurement round (UTC)', fontsize=fontsize)
plt.xlabel('Measurement data batch ID (6 rounds/batch)', fontsize=fontsize)

# set ylabel
plt.ylabel("Min latency over relay - direct (ms)", fontsize=fontsize)

# set xticks
xticks = []
for i,msm_id_bucket in enumerate(every_n_buckets):
    start_id = msm_id_bucket[0]
    end_id = msm_id_bucket[1]
    start_time = (msm_times[str(start_id)][str(MAX_STEP)]['end'] + msm_times[str(start_id)][str(MAX_STEP)]['start'])/2
    start_date = dt.datetime.fromtimestamp(start_time).strftime('%d/%m')
    end_time = (msm_times[str(end_id)][str(MAX_STEP)]['end'] + msm_times[str(end_id)][str(MAX_STEP)]['start'])/2
    end_date = dt.datetime.fromtimestamp(end_time).strftime('%d/%m')
    #xticks.append('{}-\n{}'.format(start_date, end_date))
    xticks.append(i+1)
#plt.subplots_adjust(bottom=0.1)
plt.xticks(every_n_msm_ids, xticks, fontsize=fontsize)

plt.yticks(fontsize=fontsize)

# set title
#plt.title("Boxplots of total % overlay latency differences vs time", fontsize=fontsize)

# activate grid
ax.grid(True, which='y')
ax.yaxis.grid(linestyle=':')

# misc
plt.tight_layout()
#plt.show()
plt.savefig(args.output_figure_file)
plt.close()



