#!/usr/bin/env python3

import matplotlib.pyplot as plt
import glob
import argparse
import ujson as json
import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True
from pprint import pprint as pp

MIN_STEP = 1
MAX_STEP = 10
TYPES    = [
    'rae2cor',
    'rae2plr',
    'rae2rar_other',
    'rae2rar_eye'
]

# http://stackoverflow.com/questions/7799156/can-i-cycle-through-line-styles-in-matplotlib
colors      = ('k', 'g', 'b', 'r')
linestyles  = ('-','--','-.',':')
styles      = ['{}{}'.format(color,linestyles[i]) for i,color in enumerate(colors)]
style_index = 0
fontsize    = 20

parser = argparse.ArgumentParser(description="plot relay gains vs num")
parser.add_argument('-i', '--input', dest='input_file', type=str, help='json dict file with times', required=True)
parser.add_argument('-o', '--output', dest='output_figure_file', type=str, help='png/ps figure file for output', required=True)
args = parser.parse_args()

with open(args.input_file) as f:
    input = json.load(f)

fig, ax = plt.subplots()
plt.subplots_adjust(bottom=0.15)
plt.subplots_adjust(top=0.9)
for type in TYPES:
    max_range = min(len(input[type]), 100)
    x = range(1, max_range+1)
    y = input[type][:max_range]
    ax.plot(x, y, styles[style_index], label='{}'.format(type).replace('rae2', '').replace('_', '\_').upper())
    style_index = (style_index + 1) % len(styles)

plt.xlim(1, max_range)

# set legend
#legend = ax.legend(loc='lower right', fontsize=fontsize, frameon=False)
legend = ax.legend(bbox_to_anchor=(0., 1.02, 1., 1), loc=3, fontsize=16,
           ncol=4, mode="expand", borderaxespad=0., handlelength=1.0, handletextpad=0.5)

# set ylable
plt.ylabel("\% of total cases improved vs. direct", fontsize=fontsize)

# set xlabel
plt.xlabel("Number of top-appearing relays", fontsize=fontsize)

#set xticks
xticks = []
xticks.append(1)
for i in x:
    if i % 10 == 0:
        xticks.append(i)
xticks.append(100)
plt.xticks(xticks, fontsize=fontsize)

plt.yticks(fontsize=fontsize)

# activate grid
ax.grid(True, which='both')
ax.xaxis.grid(linestyle=':')
ax.yaxis.grid(linestyle=':')

#plt.tight_layout()
#plt.show()
plt.savefig(args.output_figure_file)
plt.close()
