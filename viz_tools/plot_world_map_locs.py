#!/usr/bin/env python3

import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import argparse
import json
# install: https://github.com/matplotlib/basemap
# check: http://stackoverflow.com/questions/40374441/python-basemap-module-impossible-to-import
# check: http://stackoverflow.com/questions/18596410/importerror-no-module-named-mpl-toolkits-with-maptlotlib-1-3-0-and-py2exe
# use: http://matplotlib.org/basemap/api/basemap_api.html
from mpl_toolkits.basemap import Basemap
import matplotlib
matplotlib.rcParams['text.usetex'] = True

parser = argparse.ArgumentParser(description="plot locations of nodes on world map")
parser.add_argument('-i', '--input', dest='input_file', type=str, help='json dict file keyed by nodes ids', required=True)
parser.add_argument('-o', '--output', dest='output_figure_file', type=str, help='png/ps figure file for output', required=True)
args = parser.parse_args()

if not os.path.isfile(args.input_file):
    print("No input file %s, please make sure that the file exists" % (args.input_file))
    sys.exit(1)

# using the simplest projection, just display
# location data collections on the world map
# using latitude/longitude coordinates."""
#check: http://matplotlib.org/basemap/
#check: http://peak5390.wordpress.com/2012/12/08/matplotlib-basemap-tutorial-plotting-points-on-a-simple-map/

# initialize the figure
plt.figure()
m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
            llcrnrlon=-180,urcrnrlon=180,resolution='c')
m.drawcoastlines()
m.drawcountries()
m.fillcontinents(color='coral',lake_color='aqua')
m.drawparallels(np.arange(-90.,91.,30.))
m.drawmeridians(np.arange(-180.,181.,60.))
m.drawmapboundary(fill_color='aqua')

# gather data from file
ids      = []
lon_list = []
lat_list = []
with open(args.input_file, 'r') as f:
    info = json.load(f)
    for i in info:
        ids.append(i)
        lat_list.append(info[i]['latitude'])
        lon_list.append(info[i]['longitude'])

# plot location data
x,y = m(lon_list, lat_list)
m.plot(x, y, 'bo')
# no labels needed (too small)
#for i, xpt, ypt in zip(ids, x, y):
#    plt.text(xpt, ypt, i)
#plt.show()
plt.savefig(args.output_figure_file, bbox_inches = 'tight', pad_inches = 0)
