#!/usr/bin/env python3

import matplotlib.pyplot as plt
import glob
import argparse
import ujson as json
import numpy as np
import statsmodels.api as sm # recommended import according to the docs
#http://stackoverflow.com/questions/23343484/python-3-statsmodels
import matplotlib
matplotlib.rcParams['text.usetex'] = True
from pprint import pprint as pp

MIN_STEP = 1
MAX_STEP = 10
TYPES    = [
    'rae2cor',
    'rae2plr',
    'rae2rar_other',
    'rae2rar_eye'
]

# http://stackoverflow.com/questions/7799156/can-i-cycle-through-line-styles-in-matplotlib
colors      = ('k', 'g', 'b', 'r')
linestyles  = ('-','--')#,'-.')#,':')
markers     = ('o', 's', '+', '^')
styles      = ['{}{}{}'.format(color,linestyle,marker) for (color,marker) in zip(colors,markers) for linestyle in linestyles]
style_index = 0
fontsize    = 20

parser = argparse.ArgumentParser(description="plot top relay gains (in ms) for each of the top relays")
parser.add_argument('-i', '--input', dest='input_file', type=str, help='json dict file with times', required=True)
parser.add_argument('-n', '--number_relays', type=int, help='number of relays to consider', required=True)
#parser.add_argument('--min', dest='min', type=int, help="minimum value on x-axis", default=None)
#parser.add_argument('--max', dest='max', type=int, help="maximum value on x-axis", default=None)
parser.add_argument('-o', '--output', dest='output_figure_file', type=str, help='png/ps figure file for output', required=True)
args = parser.parse_args()

with open(args.input_file) as f:
    input = json.load(f)

fig, ax = plt.subplots()
plt.subplots_adjust(bottom=0.15)
plt.subplots_adjust(top=0.9)

# set cdfs
# ecdfs = {}
# for type in TYPES:
#    ecdfs[type] = {
#        '1-top' : sm.distributions.ECDF(input[type]['improvements'][0]),
#        'top-{}'.format(args.number_relays) : sm.distributions.ECDF(input[type]['improvements'][args.number_relays-1]),
#        'max' : sm.distributions.ECDF(input[type]['best_improvements'])
#    }

# set x axes
x = range(0, 101, 5)
# x = {}
# all_x_values = []
# for type in TYPES:
#    all_x_values.extend(input[type]['improvements'][0])
#    all_x_values.extend(input[type]['improvements'][args.number_relays-1])
#    all_x_values.extend(input[type]['best_improvements'])
# x_min = min(all_x_values)
# x_max = max(all_x_values)
# if args.min is not None:
#    x_min = args.min
# if args.max is not None:
#    x_max = args.max
# for type in TYPES:
#    x[type] = {
#        '1-top' : np.linspace(x_min, x_max, 1000),
#        'top-{}'.format(args.number_relays) : np.linspace(x_min, x_max, 1000),
#        'max' : np.linspace(x_min, x_max, 1000)
#    }

# set y axes
y = {}
for type in TYPES:
    y[type] = {
        #'1-top' : [],
        'top-{}'.format(args.number_relays) : [],
        'all' : []
    }
    for i in x:
        #y[type]['1-top'].append(100.0*len([l for l in input[type]['improvements'][0] if l >= i])/(1.0*input['all_instances']))
        y[type]['top-{}'.format(args.number_relays)].append(100.0*len([l for l in input[type]['improvements'][args.number_relays-1] if l >= i])/(1.0*input['all_instances']))
        y[type]['all'].append(100.0*len([l for l in input[type]['best_improvements'] if l >= i])/(1.0*input['all_instances']))

# y = {}
# for type in TYPES:
#     y[type] = {
#         '1-top' : ecdfs[type]['1-top'](x[type]['1-top']),
#         'top-{}'.format(args.number_relays) : ecdfs[type]['top-{}'.format(args.number_relays)](x[type]['top-{}'.format(args.number_relays)]),
#         'max' : ecdfs[type]['max'](x[type]['max'])
#     }

# plot figure
fig, ax = plt.subplots()
plt.subplots_adjust(bottom=0.15)
plt.subplots_adjust(top=0.9)
for type in TYPES:
    #ax.plot(x, y[type]['1-top'], styles[style_index], label='{}-1-top'.format(type).replace('rae2', '').replace('_', '\_').upper())
    #style_index = (style_index + 1) % len(styles)
    ax.plot(x, y[type]['top-{}'.format(args.number_relays)],
            styles[style_index], label='{}-top-{}'.format(type, args.number_relays).replace('rae2', '').replace('_', '\_').upper())
    style_index = (style_index + 1) % len(styles)
    ax.plot(x, y[type]['all'], styles[style_index], label='{}-all'.format(type).replace('rae2', '').replace('_', '\_').upper())
    style_index = (style_index + 1) % len(styles)

plt.xlim(0, 100)

# set xticks
# xticks = []
# cur_x = int(x_min)
# xticks.append(cur_x)
# while cur_x < x_max:
#     xticks.append(cur_x)
#     cur_x += 50 # x-step
# xticks.append(int(x_max))
# plt.xticks(xticks, fontsize = fontsize)
# plt.xlim(x_min, x_max)
xticks = range(0, 101, 10)
plt.xticks(xticks, fontsize = fontsize)

# set yticks
# yticks = []
# cur_y = 0.2
# yticks.append(cur_y)
# while cur_y < 1.0:
#     yticks.append(cur_y)
#     cur_y += 0.1 # y-step
# yticks.append(1.0)
# plt.yticks(yticks, fontsize = fontsize)
plt.yticks(fontsize = fontsize)

# set legend
legend = ax.legend(loc='upper right', fontsize=fontsize-6, frameon=True, edgecolor='k')
#legend = ax.legend(bbox_to_anchor=(0., 1.02, 1., 1), loc=3, fontsize=16,
#           ncol=4, mode="expand", borderaxespad=0., handlelength=1.0, handletextpad=0.5)

# set xlabel
plt.xlabel("Latency (ms) improvement vs. direct paths threshold", fontsize=fontsize)

# set ylabel
plt.ylabel("\% of total cases improved vs. direct", fontsize=fontsize)

# activate grid
ax.grid(True, which='both')
ax.xaxis.grid(linestyle=':')
ax.yaxis.grid(linestyle=':')

#plt.tight_layout()
#plt.show()
plt.savefig(args.output_figure_file)
plt.close()








