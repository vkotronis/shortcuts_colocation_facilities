#!/usr/bin/env python3

import matplotlib.pyplot as plt
import glob
import argparse
import ujson as json
import numpy as np
import statsmodels.api as sm # recommended import according to the docs
#http://stackoverflow.com/questions/23343484/python-3-statsmodels
import matplotlib
matplotlib.rcParams['text.usetex'] = True

MIN_STEP = 1
MAX_STEP = 10
TYPES    = [
    'rae2rae',
    'rae2cor',
    'rae2rar_eye',
    'rae2rar_other',
    'rae2plr'
]

# http://stackoverflow.com/questions/7799156/can-i-cycle-through-line-styles-in-matplotlib
colors      = ('r', 'g', 'b', 'k')
linestyles  = ('-','--','-.',':')
styles      = ['{}{}'.format(color,linestyle) for color in colors for linestyle in linestyles]
style_index = 0
fontsize    = 20

parser = argparse.ArgumentParser(description="plot % latency coefficient of variation cdf")
parser.add_argument('-i', '--input', dest='input_dir', type=str, help='directory with all valid_ping_median jsons', required=True)
parser.add_argument('--min', dest='min', type=int, help="minimum value on x-axis", default=None)
parser.add_argument('--max', dest='max', type=int, help="maximum value on x-axis", default=None)
parser.add_argument('-o', '--output', dest='output_figure_file', type=str, help='png/ps figure file for output', required=True)
args = parser.parse_args()

# initialize results
results = {}
for type in TYPES:
    results[type] = {}

# gather results
for msm_res_json in glob.iglob('{}/msm_*_valid_ping_medians.json'.format(args.input_dir)):
    with open(msm_res_json, 'r') as f:
        this_res = json.load(f)

    for pair in this_res:
        for type in TYPES:
            if len(this_res[pair][type].values()) > 0:
                pair_tuple = eval(pair)

                if type == 'rae2rae':
                    rae2rae_median  = this_res[pair]['rae2rae'][pair_tuple[1]]['median']

                    if pair_tuple not in results[type]:
                        results[type][pair_tuple] = []
                    results[type][pair_tuple].append(rae2rae_median)
                else:
                    for rel in this_res[pair][type].keys():
                        rel_pair_tuple = (pair_tuple[1], rel, pair_tuple[0])

                    if rel_pair_tuple not in results[type]:
                        results[type][rel_pair_tuple] = []
                    results[type][rel_pair_tuple].append(this_res[pair][type][rel]['median'])

# calculate cvs
cvs = {}
for type in TYPES:
    cvs[type] = []

    for k in results[type]:
        if len(results[type][k]) >= 3:
            cvs[type].append(100.0*np.std(results[type][k])/(1.0*np.average(results[type][k])))

# set cdfs
ecdfs = {}
for type in TYPES:
    ecdfs[type] = sm.distributions.ECDF(cvs[type])

# set x axes
x     = {}
x_min = min([min(cvs[type]) for type in TYPES])
x_max = max([max(cvs[type]) for type in TYPES])
if args.min is not None:
    x_min = args.min
if args.max is not None:
    x_max = args.max
for type in TYPES:
    x[type] = np.linspace(x_min, x_max, 1000)

# set y axes
y = {}
for type in TYPES:
    y[type] = ecdfs[type](x[type])

fig, ax = plt.subplots()
for type in TYPES:
    label = str(type)
    if type != 'rae2rae':
        label = type.replace('rae2', '').replace('_', '\_').upper()
    else:
        label = 'DIR'
    ax.plot(x[type], y[type], styles[style_index], label=label)
    style_index = (style_index + 1) % len(styles)

# set xticks
#xticks = []
#cur_x = x_min
#xticks.append(cur_x)
#while cur_x < x_max:
#    xticks.append(cur_x)
#    cur_x += 5 # x-step
#plt.xticks(xticks)
plt.xticks(fontsize=fontsize)

# set yticks
yticks = []
cur_y = 0.0
yticks.append(cur_y)
while cur_y < 1.0:
    yticks.append(cur_y)
    cur_y += 0.2 # y-step
yticks.append(1.0)
plt.yticks(yticks, fontsize=fontsize)

# set legend
legend = ax.legend(loc='lower right', fontsize=fontsize, frameon=True, edgecolor='k')

# set xlabel
plt.xlabel("\% CV per direct pair/overlay triplet instance", fontsize=fontsize)

# set title
#plt.title("CDF of Coefficient of Variation for all direct pair/overlay triplet instances", fontsize=fontsize)

# activate grid
ax.grid(True, which='both')
ax.xaxis.grid(linestyle=':')
ax.yaxis.grid(linestyle=':')

# misc
plt.tight_layout()
#plt.show()
plt.savefig(args.output_figure_file)
plt.close()
