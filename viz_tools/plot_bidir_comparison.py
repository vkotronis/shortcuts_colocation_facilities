#!/usr/bin/env python3

import matplotlib.pyplot as plt
import glob
import argparse
import ujson as json
import numpy as np
import statsmodels.api as sm # recommended import according to the docs
#http://stackoverflow.com/questions/23343484/python-3-statsmodels
import matplotlib
matplotlib.rcParams['text.usetex'] = True

MIN_STEP = 1
MAX_STEP = 10

# http://stackoverflow.com/questions/7799156/can-i-cycle-through-line-styles-in-matplotlib
colors      = ('r', 'g', 'b', 'k')
linestyles  = ('-','--','-.',':')
styles      = ['{}{}'.format(color,linestyles[i]) for i,color in enumerate(colors)]
style_index = 0
fontsize    = 20

parser = argparse.ArgumentParser(description="plot bidir latency diffs cdf")
parser.add_argument('-i', '--input', dest='input_dir', type=str, help='directory with all valid_ping_median jsons', required=True)
parser.add_argument('--min', dest='min', type=int, help="minimum value on x-axis", default=None)
parser.add_argument('--max', dest='max', type=int, help="maximum value on x-axis", default=None)
parser.add_argument('-o', '--output', dest='output_figure_file', type=str, help='png/ps figure file for output', required=True)
args = parser.parse_args()

# initialize results
results = {}

# gather results
for msm_res_json in glob.iglob('{}/msm_*_valid_ping_medians.json'.format(args.input_dir)):
    with open(msm_res_json, 'r') as f:
        this_res = json.load(f)

    for pair in this_res:
        pair_tuple      = eval(pair)
        src             = pair_tuple[1]
        dst             = pair_tuple[0]
        rae2rae_median = this_res[pair]['rae2rae'][pair_tuple[0]]['median']

        if src not in results:
            results[src] = {}

        if dst not in results[src]:
            results[src][dst] = []

        results[src][dst].append(rae2rae_median)

src_dst_found = 0
# process results
bidir_lat_diff = []
for src in results:
    for dst in results[src]:
        if dst in results and src in results[dst]:
            bidir_lat_diff.append(((np.median(results[src][dst])-np.median(results[dst][src]))/np.median(results[src][dst]))*100.0)
            src_dst_found += 1

print(src_dst_found/2.0)

# set cdf
ecdf = sm.distributions.ECDF(bidir_lat_diff)

# set x axes
x     = {}
x_min = min(bidir_lat_diff)
x_max = max(bidir_lat_diff)
if args.min is not None:
    x_min = args.min
if args.max is not None:
    x_max = args.max
x = np.linspace(x_min, x_max, 1000)

# set y axes
y = ecdf(x)

fig, ax = plt.subplots()
ax.plot(x, y, styles[style_index])

# set xticks
xticks = []
cur_x = x_min
xticks.append(cur_x)
while cur_x < x_max:
    xticks.append(cur_x)
    cur_x += 5 # x-step
xticks.append(x_max)
plt.xticks(xticks, fontsize=fontsize)
plt.xlim(x_min, x_max)

# set yticks
yticks = []
cur_y = 0.05
yticks.append(cur_y)
while cur_y < 1.0:
    yticks.append(cur_y)
    cur_y += 0.2 # y-step
yticks.append(0.95)
plt.yticks(yticks, fontsize=fontsize)

# set xlabel
plt.xlabel("\% RTT difference", fontsize=fontsize)

# set title
#plt.title("CDF of % directional differences in median rae2rae latencies", fontsize=fontsize)

# activate grid
ax.grid(True, which='both')
ax.xaxis.grid(linestyle=':')
ax.yaxis.grid(linestyle=':')

# misc
plt.tight_layout()
#plt.show()
plt.savefig(args.output_figure_file)
plt.close()
