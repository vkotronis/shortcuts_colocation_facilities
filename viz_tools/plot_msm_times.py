#!/usr/bin/env python3

import matplotlib.pyplot as plt
import matplotlib.dates as md
import datetime as dt
import argparse
import ujson as json

MIN_STEP = 1
MAX_STEP = 10

# http://stackoverflow.com/questions/7799156/can-i-cycle-through-line-styles-in-matplotlib
colors      = ('r', 'g', 'b', 'k')
linestyles  = ('-','--','-.',':')
styles      = ['{}{}'.format(color,linestyle) for color in colors for linestyle in linestyles]
style_index = 0
fontsize    = 12

#http://stackoverflow.com/questions/4090383/plotting-unix-timestamps-in-matplotlib

parser = argparse.ArgumentParser(description="plot msm times")
parser.add_argument('-i', '--input', dest='input_file', type=str, help='json dict file with times', required=True)
parser.add_argument('-o', '--output', dest='output_figure_file', type=str, help='png/ps figure file for output', required=True)
args = parser.parse_args()

with open(args.input_file) as f:
    input = json.load(f)

msm_ids = sorted([int(i) for i in input.keys()])
x       = []
y       = []
xticks  = []
yticks  = []

for msm_id in msm_ids:
    steps = sorted([int(i) for i in input[str(msm_id)].keys()])
    for step in steps:
        start_timestamp = input[str(msm_id)][str(step)]['start']
        end_timestamp   = input[str(msm_id)][str(step)]['end']
        start_date      = dt.datetime.fromtimestamp(start_timestamp)
        end_date        = dt.datetime.fromtimestamp(end_timestamp)
        start_datenum   = md.date2num(start_date)
        end_datenum     = md.date2num(end_date)

        x.append(start_datenum)
        x.append(end_datenum)

        # format: A.B -> msm A, step B
        y.append(msm_id + 0.1*step)
        y.append(msm_id + 0.1*step)

        # take into account only the start and end of the measurement
        # to avoid cluttered x-axis
        if step == MIN_STEP:
            xticks.append(start_datenum)

        if step == MAX_STEP:
            xticks.append(end_datenum)

    yticks.append(msm_id)

yticks.append(msm_id+1)

plt.subplots_adjust(bottom=0.1)
plt.xticks(xticks, rotation=90, size=8)
plt.yticks(yticks, size=10)
ax = plt.gca()
xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
ax.xaxis.set_major_formatter(xfmt)
plt.plot(x, y, linestyle='', marker='+')
plt.xlabel("Time", size=fontsize)
plt.ylabel("<Measurement>.<Step>", size=fontsize)
plt.title("Measurement times", size=fontsize)
plt.tight_layout()
#plt.show()
plt.savefig(args.output_figure_file)
plt.close()
