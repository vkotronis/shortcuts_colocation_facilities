#!/usr/bin/env python3

import ujson as json
import random
import os
import sys
import time
import numpy
from itertools import combinations
from itertools import product
from datetime import datetime, timedelta
from ripe.atlas.cousteau import (
    Ping,
    AtlasSource,
    AtlasCreateRequest,
    AtlasResultsRequest
)
import concurrent.futures
from pprint import pprint as pp


# RIPE Atlas rate limits
# No more than 100 simultaneous measurements
# No more than 1000 probes may be used per measurement
# No more than 100,000 results can be generated per day
# No more than 50 measurement results per second per measurement.
# This is calculated as the spread divided by the number of probes.
# No more than 1,000,000 credits may be used each day
# No more than 10 ongoing and 10 one-off measurements of the same type
# running against the same target at any time


# example error:
# {'error': {'code': 102,
#           'detail': {'non_field_errors': ['We do not allow more than 10 '
#                                           'concurrent measurements to the '
#                                           'same target: 172.217.19.196.']},
#           'status': 400,
#          'title': 'Bad Request'}}

# Source:
# https://atlas.ripe.net/docs/api/v2/manual/overview/error_messages.html

# error when measurement has not started:
# {"error":{"status":403,"code":104,"detail":"This measurement hasn't yet started or is no longer running","title":"Forbidden"}}


VALID_RTT_TYPES = [int, float]


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def get_median(rtt_dict_list):
    median_rtt = -1

    all_rtts = []
    for rtt_dict in rtt_dict_list:
        if 'avg' in rtt_dict and type(rtt_dict['avg']) in VALID_RTT_TYPES and rtt_dict['avg'] > 0:
            all_rtts.append(rtt_dict['avg'])

    if len(all_rtts) > 0:
        try:
            median_rtt = numpy.median(all_rtts)
        except:
            median_rtt = -1
    else:
        median_rtt = -1

    return median_rtt


def get_results(msm_info):
    wanted_keys = ['timestamp', 'prb_id', 'from',
                   'src_addr', 'dst_addr', 'min', 'max', 'avg']
    msm_id = int(msm_info[0])
    dst = msm_info[1]
    expected_src = msm_info[2]
    expected_pkt_per_src = msm_info[3]
    msm_results = {}
    kwargs = {
        'msm_id': msm_id,
    }
    output = 'fetched_msms/{}'.format(msm_id)
        
    start_time = int(time.time())
    retrieve_timeout = 10 * 60  # 10 minutes is ok since we have already slept and waited!
    expected_res = expected_src * expected_pkt_per_src

    while True:
        is_success, results = AtlasResultsRequest(**kwargs).create()
        now_time = int(time.time())

        # return only on success, either when you have all the needed results
        # or you are tired of waiting
        if is_success and (len(results) == expected_res or ((now_time - start_time) > retrieve_timeout)):
            for msm in results:
                msm_dict = {key: msm[key] for key in wanted_keys}
                if msm['prb_id'] not in msm_results:
                    msm_results[msm['prb_id']] = []

                msm_results[msm['prb_id']].append(msm_dict)

            fin_dict = {'msm_id': msm_id, 'dst': dst,
                        'msm_results': msm_results}
            with open(output, 'w') as f:
                json.dump(fin_dict, f, indent=2)

            return

        time.sleep(60)


class ProbeMsmIssuer():

    def __init__(self,
                 authkeys_file=None,
                 probe_filepath=None,
                 other_dest_filepath=None,
                 feasible_dest_filepath=None,
                 true_run=False,
                 pkt_per_ping=1,
                 credits_per_pkt=1,
                 interval=60):

        self.authkeys_file = authkeys_file

        with open(self.authkeys_file, 'r') as f:
            self.authkeys = json.load(f)

        self.current_authkey_id = 0

        self.probe_info = None
        if probe_filepath is not None and os.path.exists(probe_filepath):
            with open(probe_filepath, 'r') as f:
                self.probe_info = json.load(f)

        self.other_dest_info = None
        if other_dest_filepath is not None and os.path.exists(other_dest_filepath):
            with open(other_dest_filepath, 'r') as f:
                self.other_dest_info = json.load(f)

        self.feasible_dests = None
        if feasible_dest_filepath is not None and os.path.exists(feasible_dest_filepath):
            with open(feasible_dest_filepath, 'r') as f:
                self.feasible_dests = json.load(f)

        self.pkt_per_ping = pkt_per_ping
        self.interval = interval
        self.true_run = true_run
        self.credits_per_pkt = credits_per_pkt
        self.credits = 0

    def get_next_key(self):

        self.current_authkey_id = (
            self.current_authkey_id + 1) % len(self.authkeys)
        return self.authkeys[self.current_authkey_id]

    def get_credits(self):

        return self.credits

    def update_credits_src_to_single_dst(self, sources):
        for source in sources:
            self.credits += len(source) * self.pkt_per_ping * \
                self.credits_per_pkt

    def createMeasurementsRA2SELF(self):
        msm_infos = []
        measurements = {}

        all_probes = list(self.probe_info.keys())

        for comb in combinations(all_probes, 2):
            if comb[0] in measurements:
                measurements[comb[0]].append(comb[1])
            else:
                measurements[comb[0]] = [comb[1]]

        for dst, src in measurements.items():

            # set destination ping
            kwargs = {
                'af': 4,
                'target': self.probe_info[dst]['address_v4'],
                'description': 'Ping from {} to {}'.format(len(src), dst),
                'packets': 1,
                'interval': self.interval
            }

            ping = Ping(**kwargs)

            # set source groups

            source_chunks = list(chunks(src, 10))
            sources = []
            total_src = 0
            for source_chunk in source_chunks:
                sources.append(AtlasSource(
                    type='probes',
                    value=','.join([str(x) for x in source_chunk]),
                    requested=len(source_chunk),
                ))
                total_src += len(source_chunk)

            # count credits
            self.update_credits_src_to_single_dst(source_chunks)

            # set measurement from source groups to destination
            while self.true_run:
                time_now = int(time.time())
                start = time_now
                stop = time_now + self.pkt_per_ping * \
                    self.interval + 60  # 60 seconds for safeguard
                atlas_request = AtlasCreateRequest(
                    key=self.get_next_key(),
                    measurements=[ping],
                    sources=sources,
                    is_oneoff=False,
                    start_time=start,
                    stop_time=stop
                )

                (is_success, response) = atlas_request.create()
                if is_success:
                    for _id in response['measurements']:
                        msm_infos.append(
                            (_id, dst, total_src, self.pkt_per_ping))
                    break
                time.sleep(1)

        return msm_infos

    def createMeasurementsRA2ANY(self):
        msm_infos = []

        dst_rev_dict = {}  # aux dict for reverse mapping, e.g., IP to probe id

        all_probes = list(self.probe_info.keys())
        all_dsts = []
        for dst in self.other_dest_info:
            # if probe
            if 'address_v4' in self.other_dest_info[dst]:
                all_dsts.append(self.other_dest_info[dst]['address_v4'])
                dst_rev_dict[self.other_dest_info[dst]['address_v4']] = dst
            # if not probe
            else:
                all_dsts.append(dst)
                # if not probe, rev_dict points to itself
                dst_rev_dict[dst] = dst

        for dst in all_dsts:

            # set destination ping
            kwargs = {
                'af': 4,
                'target': dst,
                'description': 'Ping from {} to {}'.format(len(all_probes), dst_rev_dict[dst]),
                'packets': 1,
                'interval': self.interval
            }

            ping = Ping(**kwargs)

            # set source groups
            # probes to consider as sources for this destination
            probes_to_consider = all_probes
            if self.feasible_dests is not None:
                probes_to_consider = []
                for p in all_probes:
                    if p in self.feasible_dests and dst_rev_dict[dst] in self.feasible_dests[p]:
                        probes_to_consider.append(p)

            if len(probes_to_consider) == 0:
                continue

            source_chunks = list(chunks(probes_to_consider, 10))
            sources = []
            total_src = 0

            for source_chunk in source_chunks:
                sources.append(AtlasSource(
                    type='probes',
                    value=','.join([str(x) for x in source_chunk]),
                    requested=len(source_chunk),
                ))
                total_src += len(source_chunk)

            # count credits
            self.update_credits_src_to_single_dst(source_chunks)

            # set measurement from source groups to destination
            while self.true_run:
                time_now = int(time.time())
                start = time_now
                stop = time_now + self.pkt_per_ping * \
                    self.interval + 60  # 60 seconds for safeguard

                atlas_request = AtlasCreateRequest(
                    key=self.get_next_key(),
                    measurements=[ping],
                    sources=sources,
                    is_oneoff=False,
                    start_time=start,
                    stop_time=stop
                )

                (is_success, response) = atlas_request.create()
                if is_success:
                    for _id in response['measurements']:
                        msm_infos.append(
                            (_id, dst_rev_dict[dst], total_src, self.pkt_per_ping))
                    break
                time.sleep(1)

        return msm_infos
