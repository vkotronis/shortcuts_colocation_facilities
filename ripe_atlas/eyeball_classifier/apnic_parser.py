#!/usr/bin/env python3

import urllib.request
from lxml import html
import argparse,requests
import ujson
import pprint
from bs4 import BeautifulSoup
pp = pprint.PrettyPrinter(indent=2)


def export_data(data,filename):
    with open(filename, 'w') as file:
        ujson.dump(data, file, ensure_ascii=True)       


def fetch_html_code():
    apnic_url = 'https://stats.labs.apnic.net/v6pop'
    
    data = requests.get(apnic_url)
    print('Html Code:',data.status_code)
    
    # Export data to file
    file = open('raw_html_code.txt','w') 
    file.write(data.text)
    file.close()
    

def clean_country_code(cc):
    return BeautifulSoup(cc,"lxml").text


def extract_info(requested_key,filename):
    apnic_file = filename
    track = False
    first_time = True
    start_tag = 'google.visualization.arrayToDataTable'
    end_tag = ']);'
    keys = []
    dataset = {}
    
    with open(apnic_file, 'r') as file:
        
        # Parses the raw html file.
        for line in file.readlines():
            line = line.rstrip('\n,\,')
            
            if(start_tag in line):
                track = True
                
            elif(end_tag in line and track):
                track = False
                break           
           
            elif track:
                if first_time:
                    keys = eval(line)
                    first_time = False
                else:
                    values = eval(line)
                    entry =  dict(zip(keys, values))
                    entry['CC'] = clean_country_code(entry['CC'])
                    
                    dataset.setdefault(entry[requested_key],[])
                    dataset[entry[requested_key]].append(entry)

    export_data(dataset,'dataset.json')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=str,
                        help='file with APNIC raw html dataset', required=True)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-u','--update', action='store_true',help='Update dataset.')
    group.add_argument('-k','--key', nargs=1, action='store')
    
    keys = {
        '0' : 'Rank',
        '1' : 'ASN',
        '2' : 'AS Name',
        '3' : 'CC',
        '4' : 'Users (est.)',
        '5' : 'Value (GDP x Users)',
        '6' : '% of country (Users)',
        '7' : '% of Internet (Users)',
        '8' : 'V6 Users (est)',
        '9' : '% of AS',
        '10' : '% of country V6 total',
        '11' : '%of internet V6 total',
        '12' : 'Samples'
    }
    
    
    options=parser.parse_args()
    
    if options.update:
        fetch_html_code()
    elif options.key:
        print('Selected key:',keys[options.key[0]])
        extract_info(keys[options.key[0]],options.input)

if __name__ == "__main__":
    main()
