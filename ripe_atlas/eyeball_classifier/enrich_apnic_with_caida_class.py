#!/usr/bin/env python

import sys
import os
import argparse
import ujson

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', dest='input_file', type=str,
                        help='file with APNIC dataset keyed by ASN', required=True)
    parser.add_argument('-c', '--caida_class_file', dest='caida_class_file', type=str,
                        help='file with CAIDA classification of ASNs', required=True)
    args = parser.parse_args()

    apnic_file = args.input_file
    caida_file = args.caida_class_file

    as2type = {}
    with open(caida_file, 'r') as f:
        for line in list(f)[6:]:
            split_l                         = line.strip().split('|')
            as2type['AS' + str(split_l[0])] = split_l[2].lower()

    apnic_data = {}
    with open(apnic_file, 'r') as f:
        apnic_data = ujson.load(f)

    for asn in apnic_data:
        for entry in apnic_data[asn]:
            if asn in as2type:
                entry['caida_class'] = as2type[asn]
            else:
                entry['caida_class'] = 'unknown'

    with open(apnic_file, 'w') as f:
        ujson.dump(apnic_data, f)

if __name__ == '__main__':
    main()