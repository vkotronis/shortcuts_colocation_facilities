#!/usr/bin/env python3

import json
import os
import sys
import concurrent.futures
import arrow
from ripe.atlas.cousteau import ProbeRequest
from multiprocessing import cpu_count
import socket


class ProbeClassifier():

    def __init__(self,
                 apnic_asn_data_file=None,
                 asn_cc_threshold=None,
                 output_json_dir=None
                 ):

        self.apnic_asn_data   = {}
        self.asn_cc_threshold = asn_cc_threshold
        self.output_json_dir  = output_json_dir

        dir_path = os.path.dirname(os.path.realpath(__file__))

        with open('{}/data/{}'.format(dir_path,apnic_asn_data_file), 'r') as f:
            self.apnic_asn_data = json.load(f)

        self.timestamp = arrow.now().timestamp

        self.total_probes = {
            'eyeball': {},
            'unknown': {}
        }

    def run(self):
        params = {
            "is_public"   : "true",
            "status"      : "1",
            "tags"        : "system-ipv4-works,system-ipv4-stable-30d"
        }

        probes = ProbeRequest(**params)

        def analyze_probe_info(probe):
            # accept either anchors or v3 probes
            examine_probe = False
            if not probe['is_anchor']:
                for tag in probe['tags']:
                    if tag['slug'] == 'system-v3':
                        examine_probe = True
                        break
            else:
                examine_probe = True

            if probe['asn_v4'] is None:
                examine_probe = False

            if examine_probe and 'geometry' in probe:

                asn_v4   = 'AS' + str(probe['asn_v4'])
                probe_cc = str(probe['country_code'])

                # for the classification of probes, take into account the (ASN,country) coverage
                # threshold
                as_cc_type = 'unknown'
                if asn_v4 in self.apnic_asn_data:
                    for entry in self.apnic_asn_data[asn_v4]:
                        if (entry['CC'] == probe_cc) and (entry['% of country (Users)'] >= self.asn_cc_threshold):
                            as_cc_type = 'eyeball'
                            break

                self.total_probes[as_cc_type][probe['id']] = {
                    'asn_v4'    : probe['asn_v4'],
                    'prefix_v4' : probe['prefix_v4'],
                    'address_v4': probe['address_v4'],
                    'latitude'  : probe['geometry']['coordinates'][1],
                    'longitude' : probe['geometry']['coordinates'][0],
                    'cc'        : probe['country_code'],
                    'is_anchor' : probe['is_anchor']
                }

                (hostname, aliases, ipaddrs) = socket.gethostbyaddr(
                    probe['address_v4'])

                if probe['address_v4'] in ipaddrs:
                    self.total_probes[as_cc_type][
                        probe['id']]['dns'] = [hostname]
                else:
                    self.total_probes[as_cc_type][
                        probe['id']]['dns'] = []

        with concurrent.futures.ThreadPoolExecutor() as executor:
            executor.map(analyze_probe_info, probes)

        return_dict = {
            'timestamp'         : self.timestamp,
            'total'             : 0,
            'total_v3_probes'   : 0,
            'total_anchors'     : 0
        }

        for as_cc_type, probes in self.total_probes.items():
            if not os.path.isdir(self.output_json_dir):
                os.mkdir(self.output_json_dir)

            filename = self.output_json_dir + '/' + \
                as_cc_type.lower() + '_' + \
                str(self.timestamp) + '.json'

            with open(filename, 'w') as f:
                json.dump(probes, f, indent=1)

            # count anchors
            return_dict['%s_anchors' % (as_cc_type)]  = len([i for i in probes if probes[i]['is_anchor']])
            return_dict['total_anchors']             += return_dict['%s_anchors' % (as_cc_type)]

            # count v3 probes
            return_dict['%s_v3_probes' % (as_cc_type)]  = len([i for i in probes if not probes[i]['is_anchor']])
            return_dict['total_v3_probes']             += return_dict['%s_v3_probes' % (as_cc_type)]

            # count total
            return_dict['%s_total' % (as_cc_type)]  = return_dict['%s_anchors' % (as_cc_type)] + return_dict['%s_v3_probes' % (as_cc_type)]
            return_dict['total']                   += return_dict['%s_total' % (as_cc_type)]

        return return_dict