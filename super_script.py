#!/usr/bin/env python3

# to run with pypy3:
# virtualenv -p <PATH_TO_PYPY_FOLDER>/bin/pypy3 venv
# source venv/bin/activate
# pypy3 super_script.py
# deactivate


import sys
import os
import glob
import random
import time
import ujson as json
from ripe_atlas.eyeball_classifier.classify_probes import ProbeClassifier as ra_class
from geolocation import util as geo_util
from ripe_atlas.issue.msm_issue import ProbeMsmIssuer, get_results, get_median
import subprocess
from pprint import pprint as pp
from collections import defaultdict
import concurrent.futures
import threading

ALL_MSM_DATA_DIR = '/home/vkotronis/colos/all_msm_data'
ALL_MSM_SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
MSM_INTERVAL = 12 * 3600  # 12 hours
MIN_STEP = 1
MAX_STEP = 10
MAX_INTER_STEP_DURATION = 1 * 3600  # 1 h
EYEBALL_CUTOFF = 10.0
MAX_IPS_PER_FAC = 3
MAX_PLS_PER_SITE = 2
PING_INTERVAL = 5 * 60  # 5 minutes
PING_PKTS = 6
RUN_MSM = True
CHECK_NET = True
PY3_BIN = '/usr/bin/python3'
# ATTENTION TODO: HERE PUT THE FINAL TIMESTAMP AFTER WHICH NO MSMS SHOULD
# BE DONE!
END_DATE_TIMESTAMP = 1496275199  # Wed, 31 May 2017 23:59:59 GMT

THREAD_LOCK = threading.RLock()  # initialize locking
FNULL = open(os.devnull, 'w')  # initialize /dev/null


def check_network(check_net_flag=CHECK_NET):
    if check_net_flag:
        response = os.system('ping -c 1 www.google.com > /dev/null')
        if response != 0:
            print_progress(
                'WARNING! Ping to google failed. Killing script for now..')
            sys.exit(0)


def print_progress(text):
    print('[+] {}'.format(text))


def add_total_credits(credits):
    try:
        with open('metadata.json', 'r') as f:
            metadata = json.load(f)
    except:
        metadata = {}

    if 'total_credits' not in metadata:
        metadata['total_credits'] = 0

    metadata['total_credits'] += credits

    with open('metadata.json', 'w') as f:
        json.dump(metadata, f, indent=2)


def validate_measurement(msm_dir, valid=True):
    try:
        with open('{}/metadata.json'.format(msm_dir), 'r') as f:
            metadata = json.load(f)
    except:
        metadata = {}

    metadata['valid'] = valid

    with open('{}/metadata.json'.format(msm_dir), 'w') as f:
        json.dump(metadata, f, indent=2)


def get_valid(msm_dir=None):
    valid = True
    try:
        with open('{}/metadata.json'.format(msm_dir), 'r') as f:
            metadata = json.load(f)
            valid = metadata['valid']
    except:
        valid = True

    return valid


def store_step(step):
    try:
        with open('metadata.json', 'r') as f:
            metadata = json.load(f)
    except:
        metadata = {}

    metadata['step'] = step

    with open('metadata.json', 'w') as f:
        json.dump(metadata, f, indent=2)


def get_step(msm_dir=None):
    step = 0
    try:
        with open('{}/metadata.json'.format(msm_dir), 'r') as f:
            metadata = json.load(f)
            step = metadata['step']
    except:
        return 0

    return step


def get_all_start_time(msm_dir=None):
    try:
        with open('{}/metadata.json'.format(msm_dir), 'r') as f:
            metadata = json.load(f)
    except:
        return None

    return metadata['step_times']['{}'.format(MIN_STEP)]['start_time']


def get_all_end_time(msm_dir=None):
    try:
        with open('{}/metadata.json'.format(msm_dir), 'r') as f:
            metadata = json.load(f)
    except:
        return None

    return metadata['step_times']['{}'.format(MAX_STEP)]['end_time']


def get_step_stop(msm_dir=None, step=None):
    step_stop = 0
    try:
        with open('{}/metadata.json'.format(msm_dir), 'r') as f:
            metadata = json.load(f)
            step_stop = metadata['step_times']['{}'.format(step)]['end_time']
    except:
        try:
            step_stop = metadata['step_times']['{}'.format(
                step + 1)]['start_time']  # return start time of next step
        except:
            step_stop = time.time() + 1  # return future time since step is not over yet

    return step_stop


def store_step_start(step, start_time):
    step = str(step)
    try:
        with open('metadata.json', 'r') as f:
            metadata = json.load(f)
    except:
        metadata = {}

    if 'step_times' not in metadata:
        metadata['step_times'] = {}

    if step not in metadata['step_times']:
        metadata['step_times'][step] = {}

    # if start_time already exists do not change it. we want to know how much
    # time we lost in case of error
    if 'start_time' not in metadata['step_times'][step]:
        metadata['step_times'][step]['start_time'] = int(start_time)

        with open('metadata.json', 'w') as f:
            json.dump(metadata, f, indent=2)


def store_step_stop(step, end_time):
    step = str(step)
    try:
        with open('metadata.json', 'r') as f:
            metadata = json.load(f)
    except:
        metadata = {}

    # this is an extreme case because start_time should already exist
    if 'step_times' not in metadata:
        metadata['step_times'] = {}

    # this is an extreme case because start_time should already exist
    if step not in metadata['step_times']:
        metadata['step_times'][step] = {}
        metadata['step_times'][step]['start_time'] = int(end_time)

    metadata['step_times'][step]['end_time'] = int(end_time)
    metadata['step_times'][step]['duration'] = int(
        end_time) - metadata['step_times'][step]['start_time']

    with open('metadata.json', 'w') as f:
        json.dump(metadata, f, indent=2)


def get_ra_probes():
    # sample probe format:
    '''
    "18433": {
        "is_anchor": false,
        "latitude": 50.1685,
        "address_v4": "94.112.176.199",
        "cc": "CZ",
        "longitude": 12.6685,
        "prefix_v4": "94.112.0.0/16",
        "dns": [
            "ip-94-112-176-199.net.upcbroadband.cz"
        ],
        "asn_v4": 6830
    }
    '''

    eyeball_json_files = glob.glob('initial_ra_files/eyeball_*.json')
    eyeball_json_file = eyeball_json_files[0]
    unknown_json_files = glob.glob('initial_ra_files/unknown_*.json')
    unknown_json_file = unknown_json_files[0]

    with open(eyeball_json_file, 'r') as f1, open(unknown_json_file, 'r') as f2:
        eyeball_probes = json.load(f1)
        unknown_probes = json.load(f2)

    all_probes = {**eyeball_probes, **unknown_probes}

    return (eyeball_probes, unknown_probes, all_probes)


def sample_ra_probes(probes={}, avoid_probes={}):

    country_asn_to_probes = defaultdict(lambda: defaultdict(set))
    for probe, value in probes.items():
        if probe not in avoid_probes:
            country_asn_to_probes[value['cc']][value['asn_v4']].add(probe)

    sampled_probes = set()
    for cc in country_asn_to_probes:
        sampled_asn = random.sample(country_asn_to_probes[cc].keys(), 1)[0]
        sampled_probes.add(random.sample(
            country_asn_to_probes[cc][sampled_asn], 1)[0])

    sampled_probes = list(sampled_probes)

    return sampled_probes


def sample_colo_ips(all_colo_ips={}, max_ips_per_fac=MAX_IPS_PER_FAC):

    fac_to_ips = defaultdict(set)
    for ip, value in all_colo_ips.items():
        fac_to_ips[value['fac']].add(ip)

    sampled_ips = set()
    for fac in fac_to_ips:
        if len(fac_to_ips[fac]) <= max_ips_per_fac:
            sampled_ips.update(fac_to_ips[fac])
        else:
            sampled_ips.update(
                set(random.sample(fac_to_ips[fac], max_ips_per_fac)))

    sampled_ips = list(sampled_ips)

    return sampled_ips


def sample_pl_nodes(all_pl_nodes={}, max_pls_per_site=MAX_PLS_PER_SITE):

    site_to_pls = defaultdict(set)
    for n, value in all_pl_nodes.items():
        site_to_pls[value['site_id']].add(n)

    sampled_pls = set()
    for site_id in site_to_pls:
        if len(site_to_pls[site_id]) <= max_pls_per_site:
            sampled_pls.update(site_to_pls[site_id])
        else:
            sampled_pls.update(
                set(random.sample(site_to_pls[site_id], max_pls_per_site)))

    sampled_pls = list(sampled_pls)

    return sampled_pls


# expects tuple of: (dst_type, probe_filepath, other_dest_filepath,
# feasible_dest_filepaths)
def issue_rae_to_dst_type(params=None):

    dst_type = params[0]
    probe_filepath = params[1]
    other_dest_filepath = params[2]
    feasible_dest_filepath = params[3]
    first_wait = params[4]

    if not os.path.isfile('rae2{}_results/issue.json'.format(dst_type)):

        print_progress('Issuing rae2{} measurements'.format(dst_type))

        issuer = ProbeMsmIssuer(authkeys_file="{}/.atlas/auth.json".format(os.environ['HOME']),
                                probe_filepath=probe_filepath,
                                other_dest_filepath=other_dest_filepath,
                                feasible_dest_filepath=feasible_dest_filepath,
                                true_run=RUN_MSM,
                                pkt_per_ping=PING_PKTS,
                                interval=PING_INTERVAL
                                )

        if dst_type == 'rae':
            msm_infos = issuer.createMeasurementsRA2SELF()
        else:
            msm_infos = issuer.createMeasurementsRA2ANY()

        print_progress('Credits required for the rae2{} step = {}'.format(
            dst_type, issuer.get_credits()))
        add_total_credits(issuer.get_credits())

        with open('rae2{}_results/issue.json'.format(dst_type), 'w') as f:
            json.dump(msm_infos, f, indent=2)


# expects tuple of: (dst_type, probe_filepath, other_dest_filepath,
# feasible_dest_filepaths, wait)
def fetch_rae_to_dst_type(params=None):

    dst_type = params[0]
    probe_filepath = params[1]
    other_dest_filepath = params[2]
    feasible_dest_filepath = params[3]
    first_wait = params[4]

    if not os.path.exists('rae2{}_results/results.json'.format(dst_type)):

        if first_wait:
            print_progress('Sleeping for {} seconds to let msms run smoothly'.format(PING_PKTS * PING_INTERVAL))
            time.sleep(PING_PKTS * PING_INTERVAL)

        issued_msm_ids = set()
        with open('rae2{}_results/issue.json'.format(dst_type), 'r') as f:
            msm_infos = json.load(f)

        for msm_info in msm_infos:
            issued_msm_ids.add(str(msm_info[0]))

        if not os.path.isdir('fetched_msms'):
            print_progress('Creating fetched_msms')
            os.mkdir('fetched_msms')

        fetched_msms = [m.split('/')[1] for m in glob.iglob('fetched_msms/*')]

        print_progress('Retrieving rae2{} measurements'.format(dst_type))

        msm_infos_to_remove = []
        for info in msm_infos:
            if str(info[0]) in fetched_msms:
                msm_infos_to_remove.append(info)

        for info in msm_infos_to_remove:
            msm_infos.remove(info)

        with concurrent.futures.ThreadPoolExecutor() as executor:
            executor.map(get_results, msm_infos)

        with open('rae2{}_results/results.json'.format(dst_type), 'w') as f:
            fetched_msms = [m.split('/')[1] for m in glob.iglob('fetched_msms/*')]
            results = []
            for msm_file_name in fetched_msms:
                if msm_file_name not in issued_msm_ids:
                    continue

                with open('fetched_msms/{}'.format(msm_file_name), 'r') as msm_fd:
                    results.append(json.load(msm_fd))
            json.dump(results, f, indent=2)


def check_full_end():
    now_time = time.time()
    if now_time > END_DATE_TIMESTAMP:
        print_progress('END OF MEASUREMENTS. EXITING')
        sys.exit(0)


# first, check if all measurements are done
check_full_end()

# cd into the dir of the super_script.py
os.chdir(ALL_MSM_SCRIPT_DIR)

# create the all msm data folder if it doesn't already exist
if not os.path.exists(ALL_MSM_DATA_DIR):
    os.mkdir(ALL_MSM_DATA_DIR)
os.chdir(ALL_MSM_DATA_DIR)

# initialize current measurement information
cur_step = 0
running_id = 1
most_recent_msm_id = 0

# check the place of the current msm in the world
if len(list(glob.iglob('msm_*'))) > 0:
    most_recent_msm_dir = max(glob.iglob('msm_*'), key=os.path.getctime)
    most_recent_msm_id = int(
        most_recent_msm_dir.split('msm_')[1].replace('/', ''))

    while not os.path.isfile('{}/metadata.json'.format(most_recent_msm_dir)):
        time.sleep(60)

    most_recent_msm_step = get_step(most_recent_msm_dir)
    # the last msm has been completed successfully, wait for 12 hours and
    # start this one
    if most_recent_msm_step == MAX_STEP or (not get_valid(most_recent_msm_dir)):
        if get_valid(most_recent_msm_dir):
            most_recent_runtime = get_all_end_time(most_recent_msm_dir)
        else:
            most_recent_runtime = get_step_stop(
                most_recent_msm_dir, most_recent_msm_step)
        running_id = most_recent_msm_id + 1
        now_time = int(time.time())
        # if start time is still in the future wait, otherwise start immediately
        # after checking which id this msm should have
        # this takes care of applying the correct interval, even if the script
        # has crashed in the meanwhile
        if most_recent_runtime + MSM_INTERVAL > now_time:
            wait_time = most_recent_runtime + MSM_INTERVAL - now_time
            print_progress('Current measurement {} is in the future, waiting for {} seconds'.format(
                running_id, wait_time))
            time.sleep(wait_time)
        else:
            num_of_intervals_passed = int(
                (now_time - most_recent_runtime) / MSM_INTERVAL)
            running_id = most_recent_msm_id + num_of_intervals_passed
            print_progress('Current measurement {} will commence from scratch right now'.format(
                running_id))
    # the last msm is the current one, continue from where we left off
    else:
        cur_step = most_recent_msm_step
        running_id = most_recent_msm_id
        cur_step_stop = get_step_stop(most_recent_msm_dir, cur_step)
        now_time = time.time()
        if (now_time - cur_step_stop) > MAX_INTER_STEP_DURATION:
            validate_measurement(most_recent_msm_dir, False)
            print_progress(
                'Current measurement {} is not valid! Exiting...'.format(running_id))
            sys.exit(1)
        else:
            print_progress(
                'Current measurement {} will resume right now from step {}'.format(running_id, cur_step + 1))

print_progress('Current measurement {} is running'.format(running_id))

if not os.path.isdir('msm_{}'.format(running_id)):
    print_progress('Creating msm_{}'.format(running_id))
    os.mkdir('msm_{}'.format(running_id))

os.chdir('msm_{}'.format(running_id))
os.system('touch metadata.json')
validate_measurement('.', True)

# Step 1 - Debugged
# fetch all updated RIPE Atlas nodes belonging to eyeballs
if cur_step == 0:

    print_progress('Starting step {}'.format(cur_step + 1))
    time_started = time.time()
    store_step_start(cur_step + 1, time_started)
    check_network()

    print_progress('Classifying RA probes')
    ra_classifier = ra_class(
        apnic_asn_data_file='apnic_final_dataset_ASN_enriched_31_3_2017.json',
        asn_cc_threshold=EYEBALL_CUTOFF,
        output_json_dir='initial_ra_files')

    ra_classifier_dict = ra_classifier.run()
    with open('initial_ra_files/ra_classifier_ret_dict.json', 'w') as f:
        json.dump(ra_classifier_dict, f, indent=2)

    time_ended = time.time()
    store_step_stop(cur_step + 1, time_ended)
    store_step(cur_step + 1)
    cur_step = 1
    print_progress('Moving to next step {}'.format(cur_step + 1))

# Step 2 - Debugged
# fetch all updated pingable alive PlanetLab nodes
if cur_step == 1:

    print_progress('Starting step {}'.format(cur_step + 1))
    time_started = time.time()
    store_step_start(cur_step + 1, time_started)
    check_network()

    if not os.path.isdir('initial_pl_files'):
        print_progress('Creating initial_pl_files')
        os.mkdir('initial_pl_files')

    if not os.path.isfile('initial_pl_files/alive_planetlab_nodes.json'):
        print_progress('Finding alive planetlab nodes')
        retcode = subprocess.call(
            '{} {}/planetlab/check_alive.py -i {}/planetlab/planetlab_nodes -o initial_pl_files/alive_planetlab_nodes.json --key {}/rsa_keys/id_rsa'.format(
                PY3_BIN, ALL_MSM_SCRIPT_DIR, ALL_MSM_SCRIPT_DIR, ALL_MSM_SCRIPT_DIR).split(), stdout=FNULL, stderr=FNULL)

    if not os.path.isfile('initial_pl_files/pingable_alive_planetlab_nodes.json'):
        print_progress('Finding pingable planetlab nodes')
        retcode = subprocess.call(
            '{} {}/planetlab/check_pingable.py -i initial_pl_files/alive_planetlab_nodes.json -o initial_pl_files/pingable_alive_planetlab_nodes.json'.format(
                PY3_BIN, ALL_MSM_SCRIPT_DIR).split(), stdout=FNULL, stderr=FNULL)

    if not os.path.isfile('initial_pl_files/geolocated_pingable_alive_planetlab_nodes.json'):
        print_progress('Geolocating planetlab nodes')
        retcode = subprocess.call(
            '{} {}/geolocation/get_planetlab_coordinates.py -i initial_pl_files/pingable_alive_planetlab_nodes.json -c {}/geolocation/data/pl_cache.json -o initial_pl_files/geolocated_pingable_alive_planetlab_nodes.json'.format(
                PY3_BIN, ALL_MSM_SCRIPT_DIR, ALL_MSM_SCRIPT_DIR).split(), stdout=FNULL, stderr=FNULL)

    if not os.path.isfile('initial_pl_files/geolocated_pingable_alive_planetlab_nodes.json'):
        sys.exit(1)

    time_ended = time.time()
    store_step_stop(cur_step + 1, time_ended)
    store_step(cur_step + 1)
    cur_step = 2
    print_progress('Moving to next step {}'.format(cur_step + 1))

# Step 3 - Debugged
# fetch all verified colo IPs
if cur_step == 2:

    print_progress('Starting step {}'.format(cur_step + 1))
    time_started = time.time()
    store_step_start(cur_step + 1, time_started)
    check_network()

    if not os.path.isdir('initial_colo_files'):
        print_progress('Creating initial_colo_files')
        os.mkdir('initial_colo_files')

    if not os.path.isfile('initial_colo_files/pingable_checked_colo_ips.json'):
        print_progress('Finding pingable colos')
        retcode = subprocess.call(
            '{} {}/colo_tools/check_pingable.py -i {}/colo_tools/data/final_checked_colo_ips.json -o initial_colo_files/pingable_checked_colo_ips.json'.format(
                PY3_BIN, ALL_MSM_SCRIPT_DIR, ALL_MSM_SCRIPT_DIR).split(), stdout=FNULL, stderr=FNULL)

    if not os.path.isfile('initial_colo_files/geolocated_pingable_checked_colo_ips.json'):
        print_progress('Geolocating colos')
        retcode = subprocess.call(
            '{} {}/geolocation/get_colo_coordinates.py -i initial_colo_files/pingable_checked_colo_ips.json -f {}/colo_tools/data/pdb_facs.json -l {}/geolocation/data/city_geoloc.json -o initial_colo_files/geolocated_pingable_checked_colo_ips.json'.format(
                PY3_BIN, ALL_MSM_SCRIPT_DIR, ALL_MSM_SCRIPT_DIR, ALL_MSM_SCRIPT_DIR).split(), stdout=FNULL, stderr=FNULL)

    if not os.path.isfile('initial_colo_files/geolocated_pingable_checked_colo_ips.json'):
        sys.exit(1)

    time_ended = time.time()
    store_step_stop(cur_step + 1, time_ended)
    store_step(cur_step + 1)
    cur_step = 3
    print_progress('Moving to next step {}'.format(cur_step + 1))

# Step 4 - Debugged
# generate sample of RIPE Atlas nodes for RAE end-points
# based on (CC,ASN) (1 per country, only for eyeball probes)
if cur_step == 3:

    print_progress('Starting step {}'.format(cur_step + 1))
    time_started = time.time()
    store_step_start(cur_step + 1, time_started)

    (eyeball_probes, unknown_probes, all_probes) = get_ra_probes()

    if not os.path.isfile('sampled_rae.json'):
        print_progress('Sampling RAE probes')
        sampled_probes = sample_ra_probes(eyeball_probes)

        sampled_probe_info = {}
        for p in sampled_probes:
            sampled_probe_info[p] = eyeball_probes[p]

        with open('sampled_rae.json', 'w') as f:
            json.dump(sampled_probe_info, f, indent=2)

    if not os.path.isfile('sampled_rae.json'):
        sys.exit(1)

    time_ended = time.time()
    store_step_stop(cur_step + 1, time_ended)
    store_step(cur_step + 1)
    cur_step = 4
    print_progress('Moving to next step {}'.format(cur_step + 1))

# Step 5 - Debugged
# generate sample of RIPE Atlas nodes for RAR relays
# based on (CC,ASN) (1 per country, both for eyeball and all probes)
if cur_step == 4:

    print_progress('Starting step {}'.format(cur_step + 1))
    time_started = time.time()
    store_step_start(cur_step + 1, time_started)

    (eyeball_probes, unknown_probes, all_probes) = get_ra_probes()

    if not os.path.isfile('sampled_rar_eye.json'):
        print_progress('Sampling eyeball RAR probes')
        sampled_probes = sample_ra_probes(eyeball_probes)

        sampled_probe_info = {}
        for p in sampled_probes:
            sampled_probe_info[p] = eyeball_probes[p]

        with open('sampled_rar_eye.json', 'w') as f:
            json.dump(sampled_probe_info, f, indent=2)

    if not os.path.isfile('sampled_rar_eye.json'):
        sys.exit(1)

    if not os.path.isfile('sampled_rar_other.json'):
        print_progress('Sampling other RAR probes')
        sampled_probes = sample_ra_probes(unknown_probes)

        sampled_probe_info = {}
        for p in sampled_probes:
            sampled_probe_info[p] = all_probes[p]

        with open('sampled_rar_other.json', 'w') as f:
            json.dump(sampled_probe_info, f, indent=2)

    if not os.path.isfile('sampled_rar_other.json'):
        sys.exit(1)

    time_ended = time.time()
    store_step_stop(cur_step + 1, time_ended)
    store_step(cur_step + 1)
    cur_step = 5
    print_progress('Moving to next step {}'.format(cur_step + 1))

# Step 6 - Debugged
# generate sample of colo IPs for COR relays
# based on facility (1-3 IPs per facility)
if cur_step == 5:

    print_progress('Starting step {}'.format(cur_step + 1))
    time_started = time.time()
    store_step_start(cur_step + 1, time_started)

    if not os.path.isfile('sampled_cor.json'):
        print_progress('Sampling Colo relays')
        with open('initial_colo_files/geolocated_pingable_checked_colo_ips.json', 'r') as f:
            all_colo_ips = json.load(f)

        sampled_colo_ips = sample_colo_ips(all_colo_ips)

        sampled_colo_ip_info = {}
        for ip in sampled_colo_ips:
            sampled_colo_ip_info[ip] = all_colo_ips[ip]

        with open('sampled_cor.json', 'w') as f:
            json.dump(sampled_colo_ip_info, f, indent=2)

    if not os.path.isfile('sampled_cor.json'):
        sys.exit(1)

    time_ended = time.time()
    store_step_stop(cur_step + 1, time_ended)
    store_step(cur_step + 1)
    cur_step = 6
    print_progress('Moving to next step {}'.format(cur_step + 1))

# Step 7 - Debugged
# generate sample of PL nodes for PLR relays
# based on site (1-3 nodes per site)
if cur_step == 6:

    print_progress('Starting step {}'.format(cur_step + 1))
    time_started = time.time()
    store_step_start(cur_step + 1, time_started)

    if not os.path.isfile('sampled_plr.json'):
        print_progress('Sampling PL relays')
        with open('initial_pl_files/geolocated_pingable_alive_planetlab_nodes.json', 'r') as f:
            all_pl_nodes = json.load(f)

        sampled_pl_nodes = sample_pl_nodes(all_pl_nodes)

        sampled_pl_node_info = {}
        for n in sampled_pl_nodes:
            sampled_pl_node_info[n] = all_pl_nodes[n]

        with open('sampled_plr.json', 'w') as f:
            json.dump(sampled_pl_node_info, f, indent=2)

    if not os.path.isfile('sampled_plr.json'):
        sys.exit(1)

    time_ended = time.time()
    store_step_stop(cur_step + 1, time_ended)
    store_step(cur_step + 1)
    cur_step = 7
    print_progress('Moving to next step {}'.format(cur_step + 1))

# Step 8 - Debugged
# issue and fetch (RAE,RAE) measurements to retrieve direct RTTs
if cur_step == 7:

    print_progress('Starting step {}'.format(cur_step + 1))
    time_started = time.time()
    store_step_start(cur_step + 1, time_started)
    check_network()

    if not os.path.isdir('rae2rae_prepare'):
        print_progress('Creating rae2rae_prepare')
        os.mkdir('rae2rae_prepare')

    if not os.path.isfile('rae2rae_prepare/issue.json'):
        print_progress('Issuing direct rae2rae measurements')

        issuer = ProbeMsmIssuer(authkeys_file="{}/.atlas/auth.json".format(os.environ['HOME']),
                                probe_filepath='sampled_rae.json',
                                other_dest_filepath=None,
                                true_run=RUN_MSM,
                                pkt_per_ping=PING_PKTS,
                                interval=PING_INTERVAL
                                )

        msm_infos = issuer.createMeasurementsRA2SELF()
        print_progress('Credits required for this step = {}'.format(
            issuer.get_credits()))
        add_total_credits(issuer.get_credits())

        print_progress('Sleeping for {} seconds to let msms run smoothly'.format(PING_PKTS*PING_INTERVAL))
        time.sleep(PING_PKTS*PING_INTERVAL)

        with open('rae2rae_prepare/issue.json', 'w') as f:
            json.dump(msm_infos, f, indent=2)

    if not os.path.isfile('rae2rae_prepare/results.json'):
        print_progress('Retrieving direct rae2rae measurements')

        with open('rae2rae_prepare/issue.json', 'r') as f:
            msm_infos = json.load(f)

        if not os.path.isdir('fetched_msms'):
            print_progress('Creating fetched_msms')
            os.mkdir('fetched_msms')

        fetched_msms = [m.split('/')[1] for m in glob.iglob('fetched_msms/*')]

        msm_infos_to_remove = []
        for info in msm_infos:
            if str(info[0]) in fetched_msms:
                msm_infos_to_remove.append(info)

        for info in msm_infos_to_remove:
            msm_infos.remove(info)

        with concurrent.futures.ThreadPoolExecutor() as executor:
            executor.map(get_results, msm_infos)

        with open('rae2rae_prepare/results.json', 'w') as f:
            fetched_msms = [m.split('/')[1] for m in glob.iglob('fetched_msms/*')]
            results = []
            for msm_file_name in fetched_msms:
                with open('fetched_msms/{}'.format(msm_file_name), 'r') as msm_fd:
                    results.append(json.load(msm_fd))
            json.dump(results, f, indent=2)

    if not os.path.isfile('rae2rae_prepare/valid_direct_rtts.json'):
        print_progress('Calculating valid direct rae2rae RTTs')

        # load direct RTTs between endpoints
        with open('rae2rae_prepare/results.json', 'r') as f:
            results = json.load(f)

        direct_rtts = {}
        for res in results:
            dst = str(res['dst'])

            for prb_id in res['msm_results']:
                src = str(prb_id)

                med = get_median(res['msm_results'][prb_id])

                if med > 0:
                    if src not in direct_rtts:
                        direct_rtts[src] = {}

                    if dst not in direct_rtts[src]:
                        direct_rtts[src][dst] = med

        with open('rae2rae_prepare/valid_direct_rtts.json', 'w') as f:
            json.dump(direct_rtts, f)

    if not os.path.isfile('rae2rae_prepare/valid_direct_rtts.json'):
        sys.exit(1)

    time_ended = time.time()
    store_step_stop(cur_step + 1, time_ended)
    store_step(cur_step + 1)
    cur_step = 8
    print_progress('Moving to next step {}'.format(cur_step + 1))

# Step 9 - Debugged
# for each (RAE,RAE) pair calculate the feasible RAR, PLR and COR relays
# based on the direct RTT and the geo_latency
if cur_step == 8:

    print_progress('Starting step {}'.format(cur_step + 1))
    time_started = time.time()
    store_step_start(cur_step + 1, time_started)

    rel_types = ['rar_eye',
                 'rar_other',
                 'plr',
                 'cor'
                 ]

    and_condition = True
    for rel_type in rel_types:
        and_condition = and_condition and os.path.isfile(
            'feasible_{}.json'.format(rel_type))

    if not and_condition:

        # load endpoints
        with open('sampled_rae.json', 'r') as f:
            sampled_rae = json.load(f)

        # load valid direct RTTs between endpoints
        with open('rae2rae_prepare/valid_direct_rtts.json', 'r') as f:
            direct_rtts = json.load(f)

        sampled_rel = {}
        feasible_rel = {}

        for rel_type in rel_types:

            if not os.path.isfile('feasible_{}.json'.format(rel_type)):

                print_progress("Finding feasible {} relays".format(rel_type))

                # load sampled relays
                sampled_rel[rel_type] = {}
                with open('sampled_{}.json'.format(rel_type), 'r') as f:
                    sampled_rel[rel_type] = json.load(f)

                # calculate feasible relays
                feasible_rel[rel_type] = {}

                for src in direct_rtts:
                    for dst in direct_rtts[src]:

                        src = str(src)
                        dst = str(dst)

                        if src not in feasible_rel[rel_type]:
                            feasible_rel[rel_type][src] = set()

                        if dst not in feasible_rel[rel_type]:
                            feasible_rel[rel_type][dst] = set()

                        rae_src_coords = (sampled_rae[src]['latitude'], sampled_rae[
                                          src]['longitude'])
                        rae_dst_coords = (sampled_rae[dst]['latitude'], sampled_rae[
                                          dst]['longitude'])

                        for relay, value in sampled_rel[rel_type].items():

                            try:
                                relay_coords = (
                                    value['latitude'], value['longitude'])
                            except:
                                continue

                            rae2rae = direct_rtts[src][dst]

                            # takes care of wrong/unresponsive measurements
                            if rae2rae < 0:
                                continue

                            try:
                                rae2rel = geo_util.propagation_delay(
                                    geo_util.geopy_vincenty_distance(rae_src_coords, relay_coords))
                                rel2rae = geo_util.propagation_delay(
                                    geo_util.geopy_vincenty_distance(relay_coords, rae_dst_coords))
                            except:
                                continue

                            if 2 * (rae2rel + rel2rae) <= rae2rae:
                                feasible_rel[rel_type][src].add(relay)
                                feasible_rel[rel_type][dst].add(relay)

                with open('feasible_{}.json'.format(rel_type), 'w') as f:
                    json.dump(feasible_rel[rel_type], f, indent=2)

    and_condition = True
    for rel_type in rel_types:
        and_condition = and_condition and os.path.isfile(
            'feasible_{}.json'.format(rel_type))

    if not and_condition:
        sys.exit(1)

    time_ended = time.time()
    store_step_stop(cur_step + 1, time_ended)
    store_step(cur_step + 1)
    cur_step = 9
    print_progress('Moving to next step {}'.format(cur_step + 1))

# Step 10 - Debugged main functionality
# issue and fetch all (RAE,RAE), (RAE,RAR), (RAE,COR), (RAE,PLR) measurements
if cur_step == 9:

    print_progress('Starting step {}'.format(cur_step + 1))
    time_started = time.time()
    store_step_start(cur_step + 1, time_started)
    check_network()

    # dest types for which the msms should be initiated by rae!
    dst_types_from_rae = ['rae',
                          'rar_eye',
                          'rar_other',
                          'cor',
                          'plr'
                          ]

    and_condition = True
    for dst_type in dst_types_from_rae:
        if not os.path.isdir('rae2{}_results'.format(dst_type)):
            print_progress('Creating rae2{}_results'.format(dst_type))
            os.mkdir('rae2{}_results'.format(dst_type))
        and_condition = and_condition and os.path.isfile(
            'rae2{}_results/results.json'.format(dst_type))

    if not and_condition:
        # issue_rae_to_dst_type expects tuple of: (dst_type, probe_filepath,
        # other_dest_filepath, feasible_dest_filepath)
        issue_fetch_rae_to_dst_type_params = []
        issue_fetch_rae_to_dst_type_params.append(
            ('rae', 'sampled_rae.json', None, None, True))
        issue_fetch_rae_to_dst_type_params.append(
            ('rar_eye', 'sampled_rae.json', 'sampled_rar_eye.json', 'feasible_rar_eye.json', False))
        issue_fetch_rae_to_dst_type_params.append(
            ('rar_other', 'sampled_rae.json', 'sampled_rar_other.json', 'feasible_rar_other.json', False))
        issue_fetch_rae_to_dst_type_params.append(
            ('cor', 'sampled_rae.json', 'sampled_cor.json', 'feasible_cor.json', False))
        issue_fetch_rae_to_dst_type_params.append(
            ('plr', 'sampled_rae.json', 'sampled_plr.json', 'feasible_plr.json', False))

        #with concurrent.futures.ThreadPoolExecutor() as executor:
        #    executor.map(issue_fetch_rae_to_dst_type, issue_fetch_rae_to_dst_type_params)
        for p in issue_fetch_rae_to_dst_type_params:
            issue_rae_to_dst_type(p)

        for p in issue_fetch_rae_to_dst_type_params:
            fetch_rae_to_dst_type(p)

    and_condition = True
    for dst_type in dst_types_from_rae:
        and_condition = and_condition and os.path.isfile(
            'rae2{}_results/results.json'.format(dst_type))

    if not and_condition:
        sys.exit(1)

    time_ended = time.time()
    store_step_stop(cur_step + 1, time_ended)
    store_step(cur_step + 1)
    cur_step = 10

print_progress('All Steps Completed')
FNULL.close()
