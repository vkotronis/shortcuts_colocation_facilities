#!/usr/bin/env python3

from ripe.atlas.cousteau import MeasurementRequest
from ripe.atlas.cousteau import AtlasResultsRequest
from ripe.atlas.sagan import TracerouteResult
import sys
import csv
import signal
import datetime
from pprint import pprint as pp

EPOCHS_PER_DAY = 86400
csv_file_handler = None


def signal_handler(signal, frame):
    global csv_file_handler
    if csv_file_handler is not None:
        csv_file_handler.close()
    sys.exit(0)


def main():
    global csv_file_handler

    csv_file_handler = open(sys.argv[1] + '.csv', 'w')
    csv_writer       = csv.writer(csv_file_handler, delimiter=',')

    signal.signal(signal.SIGINT, signal_handler)

    filters = {
        'is_public': True,
        'status': 4,
        'type': 'traceroute',
        'target_asn': int(sys.argv[1]),
        'start': int(sys.argv[2]),
        'stop': int(sys.argv[3]),
        'af': 4,
	'is_oneoff': True
    }

    # break down the measurement duration in days
    current_start         = int(sys.argv[2])
    current_stop          = min(int(sys.argv[3]), current_start + EPOCHS_PER_DAY - 1)

    while True:
        filters['start'] = current_start
        filters['stop']  = current_stop

        measurements = MeasurementRequest(**filters)

        try:
            for msm_group in measurements:
                msm_id = msm_group['id']

                kwargs = {
                    'msm_id': int(msm_id),
                }

                is_success, results = AtlasResultsRequest(**kwargs).create()

                if is_success:
                    for msm in results:
                        try:
                            trace = TracerouteResult(
                                msm, on_error=TracerouteResult.ACTION_IGNORE)
                            if trace.is_success == True:
                                curr_trace = []
                                for hop in trace.ip_path:
                                    curr_trace.append(hop[0])
                                csv_writer.writerow(curr_trace)
                        except Exception as e:
                            print(str(e))
                            csv_file_handler.close()
                            sys.exit(0)
        except Exception as e:
            print(str(e))

        current_start += EPOCHS_PER_DAY
        current_stop   = min(int(sys.argv[3]), current_start + EPOCHS_PER_DAY - 1)

        if current_start >= current_stop:
            break

    csv_file_handler.close()

if __name__ == '__main__':
    main()
