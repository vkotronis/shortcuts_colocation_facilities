#!/usr/bin/env python3

import os
import sys
from urllib.request import urlretrieve, urlopen
import ujson
import csv
import requests
from bs4 import BeautifulSoup
from ipaddress import ip_network
import re
import subprocess
import time
from pprint import pprint as pp

PDB_FACS_FILE           = './data/pdb_facs.json'
GROUND_TRUTH_COLOS_FILE = './data/ip_facilities_public_2015_giotsas.txt'

def main():
    # fetch all pdb facilities
    print("Fetching all facilities from PeeringDB...")
    start_time = time.time()
    pdb_fac_list = []
    if not os.path.exists(PDB_FACS_FILE):
        pdb_fac_list = fetch_and_store_pdb_facs(PDB_FACS_FILE)
        print("Fetched all facilities from PeeringDB [%i sec]" % (int(time.time() - start_time)))
    else:
        print("Using cached PeeringDB facilities file...")
        with open(PDB_FACS_FILE, 'r') as f:
            pdb_fac_list = ujson.load(f)

    # index facilities based on their id
    print("Indexing pdb facilities based on id...")
    start_time = time.time()
    pdb_facs = {}
    for fac in pdb_fac_list:
        pdb_facs[fac['id']] = fac
    print("Indexed pdb facilities based on id [%i sec]" % (int(time.time() - start_time)))

    cand_colo_ips = {}
    with open(GROUND_TRUTH_COLOS_FILE ,'r') as f:
        old_dataset = ujson.load(f)

        for ip in old_dataset:
            if len(old_dataset[ip]['facilities_intersection']) == 1 and \
                old_dataset[ip]['facilities_intersection'][0] in pdb_facs:
                cand_colo_ips[ip] = old_dataset[ip]

    with open('./data/old_cand_colo_ips.json', 'w') as f:
        ujson.dump(cand_colo_ips, f)

if __name__ == '__main__':
    main()