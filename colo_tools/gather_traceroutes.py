import os
import sys
import json
import subprocess
import argparse
import time

PY3_BIN   = '/usr/bin/python3'
TRACE_BIN = 'find_traces_per_asn_csv.py'
MV_BIN    = '/bin/mv'

parser = argparse.ArgumentParser(description="gather traceroutes for specific asns for specific timestamps")
parser.add_argument('-i', '--input', dest='input_file', type=str, help='list of asns in json format (can also be dict with asn keys in file)', required=True)
parser.add_argument('-s', '--start_timestamp', dest='start_timestamp', type=int, help='start timestamp (integer)', required=True)
parser.add_argument('-t', '--stop_timestamp', dest='stop_timestamp', type=int, help='stop_timestamp (integer)', required=True)
parser.add_argument('-m', '--max_processes', dest='max_processes', type=int, help='max number of processes (integer)', default=4)
parser.add_argument('-o', '--out_dir', dest='out_dir', type=str, help='directory where the results are stored in csv format', required=True)
args = parser.parse_args()

if not os.path.isdir(args.out_dir):
    os.mkdir(args.out_dir)

TRACE_DIR = "%s/%s_%s_oneoff_traces" % (args.out_dir, str(args.start_timestamp), str(args.stop_timestamp))
if not os.path.isdir(TRACE_DIR):
    os.mkdir(TRACE_DIR)

start_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(args.start_timestamp))
stop_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(args.stop_timestamp))

asn_list = None
with open(args.input_file, 'r') as f:
    asn_list = json.load(f)

active_processes         = {}
dead_processes           = []
all_completed_asns       = set()
currently_completed_asns = set()
already_present_asns     = set()

for asn in asn_list:

    print("Collecting traceroute measurements for asn %s for period: %s (%i) - %s (%i)" % (asn,
                                                                                        start_time,
                                                                                        args.start_timestamp,
                                                                                        stop_time,
                                                                                        args.stop_timestamp))

    if os.path.exists('%s/%s.csv' % (TRACE_DIR, asn)):
        already_present_asns.add(asn)
        print("Traceroute measurements for asn %s already stored in: %s/%s.csv" % (str(asn), TRACE_DIR, str(asn)))
        continue

    cmd = [PY3_BIN, TRACE_BIN, str(asn), str(args.start_timestamp), str(args.stop_timestamp)]

    if len(active_processes) < args.max_processes:
        active_processes[asn] = subprocess.Popen(cmd)
        print("Collection process for asn %s started" % (str(asn)))
        print("Currently %i out of %i processes are running" %(len(active_processes), args.max_processes))
    else:
        print("Collection process for asn %s blocked until others complete" % (str(asn)))
        while True:
            for a,p in active_processes.items():
                if p.poll() is not None:
                    currently_completed_asns.add(a)

            if len(currently_completed_asns) > 0:
                for a in currently_completed_asns:
                    del active_processes[a]
                    print("Collection process for asn %s completed" % (str(a)))
                    subprocess.call([MV_BIN, '%s.csv' % (a), '%s/%s.csv' % (TRACE_DIR, a)])
                    print("Traceroute measurements for asn %s stored in: %s/%s.csv" % (str(a), TRACE_DIR, str(a)))
                    all_completed_asns.add(a)
                    print("Currently %i out of %i processes are running" % (len(active_processes), args.max_processes))

                currently_completed_asns = set()
                break

            time.sleep(1)

        active_processes[asn] = subprocess.Popen(cmd)
        print("Collection process for asn %s unblocked and started" % (str(asn)))
        print("Currently %i out of %i processes are running" % (len(active_processes), args.max_processes))

print("Waiting for all collection processes to complete")
currently_completed_asns = set()
while True:
    if len(active_processes) == 0:
        break

    for a,p in active_processes.items():
        if p.poll() is not None:
            currently_completed_asns.add(a)

    if len(currently_completed_asns) > 0:
        for a in currently_completed_asns:
            del active_processes[a]
            print("Collection process for asn %s completed" % (str(a)))
            subprocess.call([MV_BIN, '%s.csv' % (a), '%s/%s.csv' % (TRACE_DIR, a)])
            print("Traceroute measurements for asn %s stored in: %s/%s.csv" % (str(a), TRACE_DIR, str(a)))
            all_completed_asns.add(a)
            print("Currently %i out of %i processes are running" % (len(active_processes), args.max_processes))
        currently_completed_asns = set()

    time.sleep(1)

print("All collection processes completed")
print("Total number of asns examined = %i" % (len(asn_list)))
print("%i ASNs were freshly gathered" % (len(all_completed_asns)))
print("%i ASNs were already present" % (len(already_present_asns)))