#!/usr/bin/env python

import sys
import os
import requests
import ujson
import json
# Libraries required to calculate the HMAC signature
import hmac, hashlib, base64
import time
sys.path.append('../geolocation')
import util
import argparse
from pprint import pprint as pp

# periscope code samples:
# http://www.caida.org/tools/utilities/looking-glass-api/#samples

# create a folder to store the periscope data

API_URL = "http://104.197.64.148/api"

PERISCOPE_SECRET = None
with open('periscope_secret.txt', 'rb') as f:
    for l in f.readlines():
        PERISCOPE_SECRET = l.lstrip().rstrip()

def issue_measurements(target_ips, orig_ip_info, pdb_facs):

    created_msm_ids = {}

    # Get the available looking glass nodes
    response            = requests.get(API_URL + "/host/list")
    all_available_hosts = response.json()
    print("We have in total %i available looking glass nodes" % (len(all_available_hosts)))

    for target_ip in target_ips:
        #print("Checking IP %s with Periscope" % (target_ip))

        ip_fac         = orig_ip_info[target_ip]['facilities_intersection'][0]
        ip_fac_city    = pdb_facs[ip_fac]['city']
        ip_fac_country = pdb_facs[ip_fac]['country']

        #print("The candidate facility of this IP is located at %s, %s" % (ip_fac_city,
        #                                                                  ip_fac_country))

        periscope_hosts_in_city = []
        for h in all_available_hosts:
            host_city    = h['city']
            host_country = h['country']

            if (host_country == ip_fac_country) and util.city_strings_match(host_city, ip_fac_city):
                periscope_hosts_in_city.append(h)

                #print("fac: %s|%s, matches host: %s|%s" % (ip_fac_city,
                #                                           ip_fac_country,
                #                                           host_city,
                #                                           host_country))

        #print("We have %i available looking glass nodes at this location" % (len(periscope_hosts_in_city)))

        if len(periscope_hosts_in_city) == 0:
            continue

        print("Issuing msms for IP %s from the available looking glasses" % (target_ip))

        # Construct the measurement array
        measurement = dict()
        measurement["argument"] = target_ip
        measurement["command"]  = "traceroute" # this is the only command supported right now!
        measurement["name"]     = "validation_%s_at_%s_%s" % (target_ip, ip_fac_city, ip_fac_country)
        measurement["hosts"]    = periscope_hosts_in_city

        # When encoding the data array to JSON string it's important to make sure that the JSON string
        # will not have empty spaces after the separators.
        data = json.dumps(measurement, sort_keys=True, separators=(',', ':'))

        # Calculate the HMAC signature
        public_key    = "gchaviaras_forth_gr"
        secret_key    = PERISCOPE_SECRET
        signature     = base64.b64encode(hmac.new(secret_key, msg=data, digestmod=hashlib.sha256).digest())
        sanitized_sig = signature.replace('+', '-').replace('=', '_').replace('/', '~')

        # Set the authentication headers
        headers = {'X-Public': public_key, 'X-Hash': sanitized_sig}

        # Post the data and the authentication headers
        response = requests.post(API_URL + '/measurement', data=data, headers=headers)

        # Convert the response from JSON string to array
        decoded_response = None
        try:
            decoded_response = response.json()
        except:
            print("Msms for IP %s from the available looking glasses not parsable!" % (target_ip))
            print("Sleeping for 120 seconds before next msm issue")
            time.sleep(120)
            continue

        if decoded_response is not None and "id" in decoded_response:
            created_msm_ids[target_ip] = decoded_response["id"]
            print("Msms for IP %s from the available looking glasses issued with id %s" % (target_ip,
                                                                                           str(decoded_response["id"])))
        else:
            print("Msms for IP %s from the available looking glasses not issued!" % (target_ip))

        print("Sleeping for 60 seconds before next msm issue")
        time.sleep(60)

    return created_msm_ids

def extract_results(msm_ids=None, out_dir=None):

    pending_ips        = set([ip for ip in msm_ids.keys() if not os.path.exists('%s/periscope_results_%s.json' % (out_dir, ip))])
    pending_msms_count = len(pending_ips)

    while pending_msms_count > 0:

        for ip in msm_ids:

            if ip not in pending_ips:
                continue

            print("---IP=%s, msm id=%s---" % (ip, msm_ids[ip]))
            try:
                response = requests.get(API_URL + "/measurement/" + str(msm_ids[ip]) + "/result?format=json").json()
            except:
                print("Currently unable to retrieve response, moving on...")
                time.sleep(20)
                continue

            print("%i collectors completed, %i failed, %i active" % (response["status"]["completed"],
                                                                     response["status"]["failed"],
                                                                     response["status"]["active"]))
            if response["status"]["active"] == 0:

                result = {
                    'rtts'      : [],
                    'msm_id'    : msm_ids[ip],
                    'full_msm'  : {}
                }

                for outer_res in response["queries"]:
                    if outer_res['status'] == 'completed':
                        for inner_res in outer_res['result']:
                            if len(inner_res['result']) == 0:
                                continue

                            last_hop_res = inner_res['result'][-1]
                            for last_hop in last_hop_res['result']:
                                if last_hop['from'] == ip and last_hop['rtt'] != 'x':
                                    result['rtts'].append(last_hop['rtt'])

                pending_ips.remove(ip)

                result['full_msm'] = response
                pending_msms_count = len(pending_ips)

                dump_result     = {}
                dump_result[ip] = result
                with open('%s/periscope_results_%s.json' % (out_dir, ip), 'w') as f:
                    ujson.dump(dump_result, f)

            time.sleep(20)

        print("%i/%i IPs complete" % (len(msm_ids.keys()) - pending_msms_count, len(msm_ids.keys())))

        if pending_msms_count == 0:
            break

def main():
    parser = argparse.ArgumentParser(description="gather periscope RTTs within the facility cities")
    parser.add_argument('-i', '--ips_file', dest='ips_file', type=str,
                        help='txt file with ips to examine', required=True)
    parser.add_argument('-c', '--orig_colo_ips_json', dest='orig_colo_ips_json', type=str,
                        help='json file with original colo ip information', required=True)
    parser.add_argument('-p', '--pdb_facs_json', dest='pdb_facs_json', type=str,
                        help='json file with list of pdb facilities', required=True)
    parser.add_argument('-o', '--out_dir', dest='out_dir', type=str,
                        help='directory where the results are stored in json format', required=True)
    args = parser.parse_args()

    pdb_fac_list = []
    with open(args.pdb_facs_json, 'r') as f:
        pdb_fac_list = ujson.load(f)

    pdb_facs = {}
    for fac in pdb_fac_list:
        pdb_facs[fac['id']] = fac

    target_ips = None
    with open(args.ips_file, 'r') as f:
        target_ips = []
        for l in f.readlines():
            target_ips.append(l.rstrip().lstrip())

    orig_ip_info = None
    with open(args.orig_colo_ips_json, 'r') as f:
        orig_ip_info = ujson.load(f)

    if not os.path.exists('./data/periscope_created_msm_ids.json'):
        print("Generating Periscope measurements")
        created_msm_ids = issue_measurements(target_ips, orig_ip_info, pdb_facs)
        with open('./data/periscope_created_msm_ids.json', 'w') as f:
            ujson.dump(created_msm_ids, f)

    created_msm_ids = None
    with open('./data/periscope_created_msm_ids.json', 'r') as f:
        created_msm_ids = ujson.load(f)

    print("Extracting Periscope results")
    if not os.path.isdir(args.out_dir):
        os.mkdir(args.out_dir)
    extract_results(created_msm_ids, args.out_dir)

    print("Results extracted and stored")

if __name__=='__main__':
    main()
