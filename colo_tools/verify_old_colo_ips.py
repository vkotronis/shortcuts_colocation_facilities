#!/usr/bin/env python3

# example run: python3 verify_old_colo_ips.py -c data/old_cand_colo_ips.json
# -f data/pdb_facs.json
# -n data/pdb_nets.json
# -p data/periscope_results
# -o data


import os
import sys
import json
import ujson
import argparse
from pprint import pprint as pp
import glob
sys.path.append('..')
import caida.parse_utils
sys.path.append('../geolocation')
import util

global pingable_ips

def rule_1(ip, colo_ip_info, pingable_ips):
    '''
    the IP is still pingable (and thus usable)
    '''
    return ip in pingable_ips

def rule_2(ip, colo_ip_info, pref2as_pyt):
    '''
    the ASN of the IP is the same as today and the related prefix is not MOASed
    '''
    old_asn  = str(colo_ip_info['asn'])
    new_asns = pref2as_pyt[ip + '/32']
    if len(new_asns) == 1 and new_asns[0] == old_asn:
        return True
    return False

def rule_3(ip, colo_ip_info, pdb_net_to_facs):
    '''
    the ASN of the IP is present in the facility today
    '''
    colo_asn = str(colo_ip_info['asn'])
    colo_fac = colo_ip_info['facilities_intersection'][0]
    if colo_asn in pdb_net_to_facs and colo_fac in pdb_net_to_facs[colo_asn]:
        return True
    return False

def rule_4(ip, colo_ip_info, periscope_rtts, rtt_thres=1.0):
    '''
    minimum periscope latency from looking glasses in facility city should be very low
    '''
    if ip in periscope_rtts and periscope_rtts[ip] <= rtt_thres:
        return True

    return False

# rule should be deactivated since IP db information too bad
def rule_5(ip, colo_ip_info, pdb_facs, maxmind_locs, ipapi_locs):
    '''
    maxmind/ipapi location of IP should be at least at the same city as the facility
    '''
    # deactivating rule
    return True

    location_cache = './data/city_geoloc.json'

    ip_fac           = colo_ip_info['facilities_intersection'][0]
    ip_fac_city      = pdb_facs[ip_fac]['city']
    ip_fac_country   = pdb_facs[ip_fac]['country']
    (ip_lat, ip_lon) = util.city_to_coords('%s|%s' % (ip_fac_city, ip_fac_country), location_cache)

    if ip_lat is None or ip_lon is None:
        return False

    (maxmind_lat, maxmind_lon) = (None, None)
    if ip in maxmind_locs:
        maxmind_city_str = maxmind_locs[ip].strip()
        maxmind_country  = maxmind_city_str.split('|')[0]
        maxmind_city     = maxmind_city_str.split('|')[1]
        if maxmind_country != 'None' and maxmind_city != 'None':
            (maxmind_lat, maxmind_lon) = util.city_to_coords(maxmind_city_str, location_cache)

    (ipapi_lat, ipapi_lon) = (None, None)
    if ip in ipapi_locs:
        ipapi_city_str = ipapi_locs[ip].strip()
        ipapi_country  = ipapi_city_str.split('|')[0]
        ipapi_city     = ipapi_city_str.split('|')[1]
        if ipapi_country != 'None' and ipapi_city != 'None':
            (ipapi_lat, ipapi_lon) = util.city_to_coords(ipapi_city_str, location_cache)

    if maxmind_lat is not None and maxmind_lon is not None:
        maxmind_distance = util.geopy_vincenty_distance((maxmind_lat, maxmind_lon), (ip_lat, ip_lon))
        if maxmind_distance < 50:
            return True

    if ipapi_lat is not None and ipapi_lon is not None:
        ipapi_distance = util.geopy_vincenty_distance((ipapi_lat, ipapi_lon), (ip_lat, ip_lon))
        if ipapi_distance < 50:
            return True

    return False

def measure_facs_and_cities(ips, colo_ip_info, pdb_facs):

    fac_cities = set()
    fac_ids    = set()
    for ip in ips:
        fac_id = colo_ip_info[ip]['facilities_intersection'][0]
        fac_ids.add(fac_id)
        fac_cities.add('%s|%s' % (pdb_facs[fac_id]['city'], pdb_facs[fac_id]['country']))

    print("Current number of facilities: %i" % (len(fac_ids)))
    print("Current number of cities: %i" % (len(fac_cities)))

rule_fun_list = [
    rule_1,
    rule_2,
    rule_3,
    rule_4,
]

rule_msg = {
    'rule_1' : 'verified pingable from Internet via active ping',
    'rule_2' : 'verified, non-MOASed ASN membership via CAIDA',
    'rule_3' : 'verified presence of ASN in facility via PeeringDB',
    'rule_4' : 'minimum Periscope latency from facility location < 1 msec',
    'rule_5' : '[DEACTIVATED] verified location of IP in facility city via MaxMind/ipapi'
}

checked_ips_after = {}

parser = argparse.ArgumentParser(description="check candidate collocation IPs against certain rules")
parser.add_argument('-c', '--colo_ip_info_file', dest='colo_ip_info_file', type=str, help='json file with colo ips to check dict', required=True)
parser.add_argument('-f', '--pdb_facs_file', dest='pdb_facs_file', type=str, help='json file with PeeringDB facility info', required=True)
parser.add_argument('-n', '--pdb_nets_file', dest='pdb_nets_file', type=str, help='json file with PeeringDB net info', required=True)
parser.add_argument('-p', '--periscope_dir', dest='periscope_dir', type=str, help='directory with periscope results jsons', required=True)
parser.add_argument('-o', '--out_dir', dest='out_dir', type=str, help='directory where the results are stored in json format', required=True)
args = parser.parse_args()

print("Loading PDB facilities")
pdb_fac_list = []
with open(args.pdb_facs_file, 'r') as f:
    pdb_fac_list = ujson.load(f)

pdb_facs = {}
for fac in pdb_fac_list:
    pdb_facs[fac['id']] = fac

print("Loading PDB networks")
with open(args.pdb_nets_file, 'r') as f:
    pdb_net_list = ujson.load(f)

pdb_net_to_facs = {}
for net in pdb_net_list:
    for fac in net['netfac_set']:
        fac_id        = fac['fac_id']
        local_net_asn = str(fac['local_asn'])

        if local_net_asn not in pdb_net_to_facs:
            pdb_net_to_facs[local_net_asn] = set()
        pdb_net_to_facs[local_net_asn].add(fac_id)

print("Loading CAIDA prefix-to-AS information")
(as2pref, pref2as_pyt) = caida.parse_utils.get_as_prefs(input_file = "../caida/routeviews-rv2-20170312-1200.pfx2as")

print("Loading periscope measurement information")
periscope_rtts = {}
for per_file in glob.glob(args.periscope_dir + '/*.json'):
    with open(per_file, 'r') as f:
        this_per_res      = ujson.load(f)
        ip_key            = list(this_per_res.keys())[0]
        ip_value          = this_per_res[ip_key]
        ip_rtts           = [float(rtt) for rtt in ip_value['rtts'] if rtt != 'ms']
        if len(ip_rtts) > 0:
            periscope_rtts[ip_key] = min(ip_rtts)

'''
print("Loading maxmind and ipapi IP geoloc information")
maxmind_json = './data/maxmind_geo_pingable_old_cand_colo_ips.json'
ipapi_json   = './data/ipapi_geo_pingable_old_cand_colo_ips.json'
maxmind_locs = None
with open(maxmind_json, 'r') as f:
    maxmind_locs = ujson.load(f)
ipapi_locs = None
with open(ipapi_json, 'r') as f:
    ipapi_locs = ujson.load(f)
'''

print("Loading pingable old colo IPs to check")
pingable_ips = []
with open('./data/pingable_old_cand_colo_ips.json', 'r') as f:
    pingable_ips = json.load(f)

print("Loading old colo IPs to check")
colo_ip_info = None
with open(args.colo_ip_info_file, 'r') as f:
    colo_ip_info = ujson.load(f)

print("Initial IPs to be checked : %i" % (len(colo_ip_info)))
measure_facs_and_cities(colo_ip_info.keys(), colo_ip_info, pdb_facs)

for rid,rule in enumerate(rule_fun_list):
    if rid == 0:
        checked_ips_after['rule_' + str(rid+1)] = [ip for ip in sorted(colo_ip_info.keys())
                                                   if rule(ip, colo_ip_info[ip], pingable_ips)]
    elif rid == 1:
        checked_ips_after['rule_' + str(rid+1)] = [ip for ip in checked_ips_after['rule_' + str(rid)]
                                                   if rule(ip, colo_ip_info[ip], pref2as_pyt)]
    elif rid == 2:
        checked_ips_after['rule_' + str(rid+1)] = [ip for ip in checked_ips_after['rule_' + str(rid)]
                                                   if rule(ip, colo_ip_info[ip], pdb_net_to_facs)]
    elif rid == 3:
        checked_ips_after['rule_' + str(rid + 1)] = [ip for ip in checked_ips_after['rule_' + str(rid)] if
                                                     rule(ip, colo_ip_info[ip], periscope_rtts)]
    elif rid == 4:
        checked_ips_after['rule_' + str(rid+1)] = [ip for ip in checked_ips_after['rule_' + str(rid)]
                                                   if rule(ip, colo_ip_info[ip],  pdb_facs,
                                                           maxmind_locs, ipapi_locs)]
    else:
        checked_ips_after['rule_' + str(rid+1)] = [ip for ip in checked_ips_after['rule_' + str(rid)]
                                                   if rule(ip, colo_ip_info[ip])]
    print("Checked IPs after rule %i (%s) : %i" % (rid+1,
                                                   rule_msg['rule_' + str(rid+1)],
                                                   len(checked_ips_after['rule_' + str(rid+1)])))

    measure_facs_and_cities(checked_ips_after['rule_' + str(rid+1)], colo_ip_info, pdb_facs)

    with open('%s/checked_colo_ips_after_rule_%i.txt' % (args.out_dir, rid+1), 'w') as f:
        for ip in checked_ips_after['rule_' + str(rid+1)]:
            f.write(ip + '\n')

with open('%s/final_checked_colo_ips.json' % (args.out_dir), 'w') as f:
    final_ips = {}
    for ip in checked_ips_after['rule_4']:
        final_ips[ip] = colo_ip_info[ip]

    ujson.dump(final_ips, f, indent=1)